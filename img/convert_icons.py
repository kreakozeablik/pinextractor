from PIL import Image # pillow
import os
import glob

def cnvrt(src, dst, size, fn):
    im = Image.open(src)
    im.size = (size, size, 1)
    im.load()
    im.save(os.path.join(dst, fn + '.bmp'))

pwd = os.getcwd()
p16 = os.path.join(pwd, '16')
p32 = os.path.join(pwd, '32')
for x in glob.iglob(os.path.join(pwd, 'Downloads', '*.icns')):
    n = os.path.splitext(os.path.basename(x))[0]
    print(n)
    cnvrt(x, p32, 32, n)
    cnvrt(x, p16, 16, n)
