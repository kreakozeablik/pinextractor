
#  PinExtractor v1.0.0 (15.01.2021)

© 2021 kreakozeablik (kreakozeablik@yandex.ru)

![PinExtractor](./pinextractor.png)

Программа предназначена для получения файлов привязки цепей к пинам
FPGA из схем в формате Altium Designer.

[Скачать](https://bitbucket.org/kreakozeablik/pinextractor/downloads/)

---

## Используемые компоненты:

- [SynEdit](https://github.com/SynEdit/SynEdit)
- [Toolbar2000](https://github.com/plashenkov/Toolbar2000)
- [TBX](https://github.com/plashenkov/TBX)
- [Virtual-TreeView](https://github.com/JAM-Software/Virtual-TreeView)

---

## Иконки

- [IconArchive](https://iconarchive.com/)
