object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'PinExtractor'
  ClientHeight = 707
  ClientWidth = 828
  Color = clBtnFace
  Constraints.MinHeight = 265
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    000000000000330077000000000000000000000000003B077070000000000000
    000000000000BB807007000000000000000000000300B0007000700000000000
    00000000330070070700070000000000000000003B0700700070007000000000
    00000000BB800700000700070000000000000300B00070000000700070000000
    0000330070070000000007000700000000003B07007000000000007007000000
    0000BB800700000000000007070000000300B000700000000070000077000000
    330070070000000007000000803300003B070070000000000000000800330000
    BB8007000000000000000080BBBB0300B000700000000070000008000BB03300
    70070000000707000000803300003B070070000000707000000800330000BB80
    07000000070700000080BBBB0000B000700000000070000008000BB000007007
    0000000007000000803300000000707000007770000000080033000000008700
    0007070700000080BBBB00000000080000077777000008000BB0000000000080
    0007070700008033000000000000000800007770000800330000000000000000
    800000000080BBBB00000000000000000800000008000BB00000000000000000
    0080000080330000000000000000000000080008003300000000000000000000
    00008080BBBB00000000000000000000000008000BB00000000000000000FFFF
    33FFFFFF21FFFFFF00FFFFFB007FFFF3003FFFF2001FFFF0000FFFB00007FF30
    0003FF200003FF000003FB000003F3000000F2000000F0000010B00000393000
    000F2000000F0000010F0000039F000000FF000000FF000010FF800039FFC000
    0FFFE0000FFFF0010FFFF8039FFFFC00FFFFFE00FFFFFF10FFFFFFB9FFFF}
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 290
    Top = 59
    Height = 648
    ExplicitLeft = 262
    ExplicitTop = 412
    ExplicitHeight = 100
  end
  object Panel3: TPanel
    Left = 293
    Top = 59
    Width = 535
    Height = 648
    Align = alClient
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    object SynMemo1: TSynMemo
      Left = 0
      Top = 22
      Width = 535
      Height = 626
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentShowHint = False
      PopupMenu = pmMemo
      ShowHint = True
      TabOrder = 0
      CodeFolding.GutterShapeSize = 11
      CodeFolding.CollapsedLineColor = clGrayText
      CodeFolding.FolderBarLinesColor = clGrayText
      CodeFolding.IndentGuidesColor = clGray
      CodeFolding.IndentGuides = True
      CodeFolding.ShowCollapsedLine = False
      CodeFolding.ShowHintMark = True
      UseCodeFolding = False
      BookMarkOptions.DrawBookmarksFirst = False
      BookMarkOptions.EnableKeys = False
      BookMarkOptions.GlyphsVisible = False
      Gutter.Font.Charset = DEFAULT_CHARSET
      Gutter.Font.Color = clWindowText
      Gutter.Font.Height = -11
      Gutter.Font.Name = 'Courier New'
      Gutter.Font.Style = []
      Gutter.Visible = False
      Gutter.Width = 0
      HintMode = shmToken
      Options = [eoNoCaret, eoRightMouseMovesCursor, eoShowScrollHint]
      ReadOnly = True
      RightEdge = 500
      RightEdgeColor = clWindow
      SearchEngine = SynEditSearch
      WordWrapGlyph.Visible = False
      OnTokenHint = SynMemo1TokenHint
      FontSmoothing = fsmNone
    end
    object TBXToolbar3: TTBXToolbar
      Left = 0
      Top = 0
      Width = 535
      Height = 22
      Align = alTop
      Caption = 'TBXToolbar3'
      Images = dmCommonData.Images16
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object TBXSeparatorItem1: TTBXSeparatorItem
        Blank = True
        Size = 3
      end
      object TBXLabelItem2: TTBXLabelItem
        Caption = '       '#1053#1072#1081#1090#1080':  '
      end
      object TBControlItem1: TTBControlItem
        Control = edtFind
      end
      object TBXItem7: TTBXItem
        Action = ActionFindBackward
        DisplayMode = nbdmTextOnlyInMenus
      end
      object TBXItem8: TTBXItem
        Action = ActionFindForward
        DisplayMode = nbdmTextOnlyInMenus
      end
      object TBXSeparatorItem2: TTBXSeparatorItem
        Blank = True
      end
      object TBControlItem2: TTBControlItem
        Control = cbSeachWholeWord
      end
      object edtFind: TComboBox
        Left = 65
        Top = 0
        Width = 189
        Height = 21
        TabOrder = 0
        OnKeyPress = edtFindKeyPress
      end
      object cbSeachWholeWord: TCheckBox
        Left = 306
        Top = 2
        Width = 97
        Height = 17
        Caption = #1057#1083#1086#1074#1086' '#1094#1077#1083#1080#1082#1086#1084
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 290
    Height = 648
    Hint = #1055#1077#1088#1077#1093#1086#1076' '#1082' '#1075#1088#1091#1087#1087#1077': Cntrl + Click'
    Align = alLeft
    BevelOuter = bvNone
    ParentShowHint = False
    ShowCaption = False
    ShowHint = False
    TabOrder = 1
    OnResize = Panel2Resize
    object pnlFiltered: TPanel
      Left = 0
      Top = 603
      Width = 290
      Height = 23
      Hint = #1055#1077#1088#1077#1093#1086#1076' '#1082' '#1075#1088#1091#1087#1087#1077': Cntrl + Click'
      Align = alBottom
      Alignment = taLeftJustify
      Caption = #1054#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1085#1086':'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = pnlFilteredClick
    end
    object TBXToolbar2: TTBXToolbar
      Left = 0
      Top = 0
      Width = 290
      Height = 22
      Align = alTop
      Caption = 'TBXToolbar2'
      Images = dmCommonData.Images16
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object TBXItem6: TTBXItem
        Action = ActionGroupAdd
        DisplayMode = nbdmTextOnlyInMenus
      end
      object TBXItem5: TTBXItem
        Action = ActionGroupRemove
        DisplayMode = nbdmTextOnlyInMenus
      end
      object TBSeparatorItem5: TTBSeparatorItem
        Blank = True
      end
      object TBXItem4: TTBXItem
        Action = ActionGroupClean
        DisplayMode = nbdmTextOnlyInMenus
      end
    end
    object pnlAllCnt: TPanel
      Left = 0
      Top = 626
      Width = 290
      Height = 22
      Hint = #1055#1077#1088#1077#1093#1086#1076' '#1082' '#1075#1088#1091#1087#1087#1077': Cntrl + Click'
      Align = alBottom
      Alignment = taLeftJustify
      Caption = #1042#1089#1077#1075#1086': '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object pnlNoGrouped: TPanel
      Left = 0
      Top = 581
      Width = 290
      Height = 22
      Hint = #1055#1077#1088#1077#1093#1086#1076' '#1082' '#1075#1088#1091#1087#1087#1077': Cntrl + Click'
      Align = alBottom
      Alignment = taLeftJustify
      Caption = #1053#1077' '#1089#1075#1088#1091#1087#1087#1080#1088#1086#1074#1072#1085#1086':'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = pnlNoGroupedClick
    end
  end
  object TBXToolbar1: TTBXToolbar
    Left = 0
    Top = 0
    Width = 828
    Height = 53
    Align = alTop
    BorderStyle = bsNone
    Caption = 'TBXToolbar1'
    Images = dmCommonData.Images32
    Options = [tboSameWidth]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    UseLastDock = False
    object smRecentFiles: TTBXSubmenuItem
      Action = actOpenSch
      DisplayMode = nbdmImageAndText
      DropdownCombo = True
      InheritOptions = False
      Layout = tbxlGlyphTop
      MinHeight = 53
      MinWidth = 50
      OnPopup = smRecentFilesPopup
    end
    object TBXItem9: TTBXItem
      Action = actEditMezanins
      DisplayMode = nbdmImageAndText
      Layout = tbxlGlyphTop
      MinHeight = 53
    end
    object TBSeparatorItem4: TTBSeparatorItem
      Blank = True
    end
    object TBXItem1: TTBXItem
      Action = FileSaveAs
      DisplayMode = nbdmImageAndText
      Layout = tbxlGlyphTop
      MinHeight = 53
    end
    object smFormat: TTBXSubmenuItem
      Caption = #1060#1086#1088#1084#1072#1090
      DisplayMode = nbdmImageAndText
      Images = dmCommonData.ImagesFormat
      InheritOptions = False
      Layout = tbxlGlyphTop
      MinHeight = 53
      MinWidth = 160
      Options = [tboDropdownArrow]
    end
    object TBXItem2: TTBXItem
      Action = actSetings
      DisplayMode = nbdmImageAndText
      Layout = tbxlGlyphTop
      MinHeight = 53
      MinWidth = 44
    end
    object TBSeparatorItem3: TTBSeparatorItem
      Blank = True
    end
    object TBXItem3: TTBXItem
      Action = actMakeMeHappy
      DisplayMode = nbdmImageAndText
      Layout = tbxlGlyphTop
      MinHeight = 53
      MinWidth = 44
    end
    object TBSeparatorItem1: TTBSeparatorItem
      Blank = True
    end
    object TBXItem10: TTBXItem
      Action = actAbout
      DisplayMode = nbdmImageAndText
      Layout = tbxlGlyphTop
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 53
    Width = 828
    Height = 6
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 3
  end
  object ActionList: TActionList
    Images = dmCommonData.Images32
    OnUpdate = ActionListUpdate
    Left = 316
    Top = 108
    object actOpenSch: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100'...'
      Hint = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 27
      OnExecute = actOpenSchExecute
    end
    object actEditMezanins: TAction
      Caption = #1052#1077#1079#1072#1085#1080#1085#1099'...'
      ImageIndex = 21
      OnExecute = actEditMezaninsExecute
    end
    object ActionFindForward: TAction
      Caption = #1053#1072#1081#1090#1080
      ImageIndex = 11
      ShortCut = 114
      OnExecute = ActionFindForwardExecute
    end
    object ActionFindBackward: TAction
      Caption = #1048#1089#1082#1072#1090#1100' '#1085#1072#1079#1072#1076
      ImageIndex = 12
      ShortCut = 8306
      OnExecute = ActionFindBackwardExecute
    end
    object FileSaveAs: TFileSaveAs
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100'...'
      Dialog.Filter = 
        'Text CSV (*.csv)|*.csv|Xilinx ISE UCF (*.ucf)|*.ucf|Xilinx Vivad' +
        'o XDC (*.xdc)|*.xdc|Lattice Diamond LPF (*.lpf)|*.lpf|All files(' +
        '*.*)|*.*'
      Dialog.FilterIndex = 0
      Dialog.OnCanClose = FileSaveAsSaveDialogCanClose
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 25
      ShortCut = 16467
      BeforeExecute = FileSaveAsBeforeExecute
      OnAccept = FileSaveAsAccept
      OnUpdate = FileSaveAsUpdate
    end
    object actMakeMeHappy: TAction
      Caption = #1040#1074#1090#1086#1075#1088#1091#1087#1087#1080#1088#1086#1074#1082#1072
      Hint = #1040#1074#1090#1086#1075#1088#1091#1087#1087#1080#1088#1086#1074#1082#1072
      ImageIndex = 7
      OnExecute = actMakeMeHappyExecute
    end
    object actSetings: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080'...'
      Hint = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      ImageIndex = 5
      OnExecute = actSetingsExecute
    end
    object actMemoEditGroup: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1075#1088#1091#1087#1087#1091'...'
      ImageIndex = 6
      OnExecute = actMemoEditGroupExecute
    end
    object EditCopy2: TEditCopy
      Category = 'Edit'
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      Hint = 'Copy|Copies the selection and puts it on the Clipboard'
      ImageIndex = 28
      ShortCut = 16451
    end
    object EditSelectAll2: TEditSelectAll
      Category = 'Edit'
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      Hint = 'Select All|Selects the entire document'
      ShortCut = 16449
    end
    object actAbout: TAction
      Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
      ImageIndex = 30
      OnExecute = actAboutExecute
    end
  end
  object SynEditSearch: TSynEditSearch
    Left = 314
    Top = 640
  end
  object alGroups: TActionList
    Images = dmCommonData.Images16
    OnUpdate = alGroupsUpdate
    Left = 40
    Top = 88
    object ActionGroupAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091'...'
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 16
      OnExecute = ActionGroupAddExecute
    end
    object ActionGroupRemove: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 17
      OnExecute = ActionGroupRemoveExecute
    end
    object ActionGroupClean: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1075#1088#1091#1087#1087#1099
      Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1075#1088#1091#1087#1087#1099
      ImageIndex = 18
      OnExecute = ActionGroupCleanExecute
    end
    object actHideGroup: TAction
      Caption = #1057#1082#1088#1099#1090#1100
      ImageIndex = 14
      OnExecute = actHideGroupExecute
    end
    object actShowGroup: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100
      ImageIndex = 15
      OnExecute = actShowGroupExecute
    end
    object actGroupEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100'...'
      ImageIndex = 6
      OnExecute = sgGroupsDblClick
    end
    object actGroupMoveTo: TAction
      Caption = #1055#1077#1088#1077#1081#1090#1080
      ImageIndex = 13
      OnExecute = actGroupMoveToExecute
    end
  end
  object pmGroups: TPopupMenu
    Images = dmCommonData.Images16
    Left = 40
    Top = 134
    object N7: TMenuItem
      Action = actGroupEdit
    end
    object N5: TMenuItem
      Action = actHideGroup
    end
    object N4: TMenuItem
      Action = actShowGroup
    end
    object N10: TMenuItem
      Action = actGroupMoveTo
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = ActionGroupAdd
    end
    object N2: TMenuItem
      Action = ActionGroupRemove
    end
  end
  object pmMemo: TPopupMenu
    Images = dmCommonData.Images16
    OnPopup = pmMemoPopup
    Left = 312
    Top = 162
    object SelectAll1: TMenuItem
      Action = EditSelectAll2
    end
    object Copy1: TMenuItem
      Action = EditCopy2
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N8: TMenuItem
      Action = actMemoEditGroup
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object miAddGrp1: TMenuItem
      Caption = 'miAddGrp1'
      OnClick = miAddGrp1Click
    end
    object miAddGrp2: TMenuItem
      Caption = 'miAddGrp2'
      OnClick = miAddGrp2Click
    end
    object miAddGrp3: TMenuItem
      Caption = 'miAddGrp3'
      OnClick = miAddGrp3Click
    end
  end
end
