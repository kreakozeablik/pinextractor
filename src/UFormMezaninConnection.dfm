object FormMezaninConnection: TFormMezaninConnection
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077
  ClientHeight = 524
  ClientWidth = 1066
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 485
    Width = 1066
    Height = 39
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      1066
      39)
    object btnCancel: TButton
      Left = 981
      Top = 6
      Width = 78
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object Button2: TButton
      Left = 900
      Top = 6
      Width = 78
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #1055#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
      Default = True
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 665
    Height = 485
    Align = alLeft
    TabOrder = 1
    object GroupBox2: TGroupBox
      Left = 1
      Top = 228
      Width = 663
      Height = 256
      Align = alClient
      Caption = #1052#1077#1079#1072#1085#1080#1085
      Padding.Left = 5
      Padding.Top = 5
      Padding.Right = 5
      Padding.Bottom = 5
      TabOrder = 0
      inline fscMezaninBoard: TFrameSelectComp
        Left = 7
        Top = 20
        Width = 649
        Height = 229
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 7
        ExplicitTop = 20
        ExplicitWidth = 649
        ExplicitHeight = 229
        inherited Panel2: TPanel
          Width = 649
          ExplicitWidth = 649
        end
        inherited sgSelComp: TStringGrid
          Width = 649
          Height = 174
          ExplicitWidth = 649
          ExplicitHeight = 174
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 663
      Height = 227
      Align = alTop
      Caption = #1054#1089#1085#1086#1074#1085#1072#1103' '#1087#1083#1072#1090#1072
      Padding.Left = 5
      Padding.Top = 5
      Padding.Right = 5
      Padding.Bottom = 5
      TabOrder = 1
      inline fscMainBoard: TFrameSelectComp
        Left = 7
        Top = 20
        Width = 649
        Height = 200
        Align = alClient
        TabOrder = 0
        ExplicitLeft = 7
        ExplicitTop = 20
        ExplicitWidth = 649
        ExplicitHeight = 200
        inherited Panel2: TPanel
          Width = 649
          ExplicitWidth = 649
        end
        inherited sgSelComp: TStringGrid
          Width = 649
          Height = 145
          ExplicitWidth = 649
          ExplicitHeight = 145
        end
      end
    end
  end
  object grdPins: TStringGrid
    Left = 665
    Top = 0
    Width = 401
    Height = 485
    Align = alClient
    ColCount = 4
    FixedCols = 0
    RowCount = 3
    FixedRows = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
    ScrollBars = ssVertical
    TabOrder = 2
  end
end
