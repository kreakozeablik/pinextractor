unit strutil;

interface

uses Classes;

//  �������� �� ������������ ����������� ���������
//  ���������� ��������� ������� ��:
//    c - ������������ ������� �������
//    * - ����� ������ ���� � ����� ���
//    ? - ����� ������ ���� ���
//    # - ����� (���� ��� ����� ����)
//    \d - ���� �����
//    \n - ���� ��� ����� ����
//    \c - ���� �����
//    \S - ���� ��� ����� ����
//    [�����] - ����� ������ �� ������ ���� � ����� ���
//    {�����} - ����� ������ �� ������ ���� ���
//      ����� ����� ��������� ��������:
//        ������_������-���������_������
//    \\ - "\"
//    \{ - "{"
//    \[ - "["
function reg_exp(in_text, in_template: String): Boolean;

function StringSortCompare(str1, str2: string): Integer;

function StrPadR(s: string; newlen: Integer): string;

function fltrstr(istr: string): string;

function TranslitRus2Lat(const Str: string): string;

function AddComboLines(sl: TStrings; text: string; maxi: Integer = 10): string;

implementation

uses SysUtils;

function fltrstr(istr: string): string;
var
  c: Char;
  s: string;
  ln: Integer;
  p: Integer;
begin
  s := '';
  for c in istr do
    if CharInSet(c, ['A'..'Z', 'a'..'z', '0'..'9', '_']) then
      s := s + c;

  ln := Length(s);
  if (ln > 0) and (s[ln] = '_') then
    delete(s, ln, 1);

  p := pos('__', s);
  while (p > 0) do
  begin
    delete(s, p, 1);
    p := pos('__', s, p);
  end;

  Result := s;
end;

function reg_exp(in_text, in_template: String): Boolean;
var
  txt, tmpl: String;

  function match(ptxt, ptmpl: Integer): Boolean;
  var
    i: Integer;
    s: string;
  begin
    result := false;
    repeat
      case tmpl[ptmpl] of
        #0: Exit(txt[ptxt] = #0);

        '?': ;

        '*': begin
            for i := ptxt to length(txt) do
              if(match(i, ptmpl + 1))then
                Exit(True);
            Exit(False);
          end;

          '{': begin
            s := '';
            Inc(ptmpl);
            while (tmpl[ptmpl] <> '}') do
            begin
              s := s + tmpl[ptmpl];
              Inc(ptmpl);
            end;
            if(pos(txt[ptxt], s) = 0)then
              Exit(false);
          end;

          '[': begin
            s := '';
            Inc(ptmpl);
            while (tmpl[ptmpl] <> ']') do
            begin
              s := s + tmpl[ptmpl];
              Inc(ptmpl);
            end;

            for i := ptxt to Length(txt) do
            begin
              if(match(i, ptmpl + 1))then
                Exit(True);

              if(pos(txt[i], s) = 0)then
                Exit(false);
            end;
          end;

          '#': begin
            if(pos(txt[ptxt], '0123456789') = 0)then
                  Exit(false);

            for i := ptxt + 1 to Length(txt) do
            begin
              if(match(i, ptmpl + 1))then
                Exit(True);

              if(pos(txt[i], '0123456789') = 0)then
                Exit(false);
            end;
          end;

          '\': begin
            Inc(ptmpl);
            case tmpl[ptmpl] of

              'd', 'D': begin
                if(pos(txt[ptxt], '0123456789') = 0)then
                  Exit(false);
              end;

              'n', 'N': begin
                for i := ptxt to Length(txt) do
                begin
                  if(match(i, ptmpl + 1))then
                    Exit(True);

                  if(pos(txt[i], '0123456789') = 0)then
                    Exit(false);
                end;
              end;

              'c', 'C': begin
                if(pos(txt[ptxt], '0123456789') > 0)then
                  Exit(false);
              end;

              's', 'S': begin
                for i := ptxt to Length(txt) do
                begin
                  if(match(i, ptmpl + 1))then
                    Exit(True);

                  if(pos(txt[i], '0123456789') > 0)then
                    Exit(false);
                end;
              end;

              else
                if(txt[ptxt] <> tmpl[ptmpl])then
                  Exit(false);
            end;
          end;
      else
        if(txt[ptxt] <> tmpl[ptmpl])then
          Exit(false);
      end;
      Inc(ptxt);
      Inc(ptmpl);
    until ((ptxt > Length(txt)) or (ptmpl > Length(tmpl)));
  end;

begin
  txt := UpperCase(in_text) + #0;
  tmpl := UpperCase(in_template) + #0;

  Result := match(1, 1)
end;

function StringSortCompare(str1, str2: string): Integer;
var
    s1, s2: String;
    i: Integer;
    l, l1, l2: Integer;
    t: Integer;
    a1, a2: Integer;
begin
    s1 := str1;
    l1 := length(s1);

    s2 := str2;
    l2 := length(s2);

    result := l1 - l2;

    if(result > 0)then
        l := l2
    else
        l := l1;

    a1 := 0;
    a2 := 0;

    t := 0;

    for i := 1 to l do
    begin
      if(CharInSet(s1[i], ['0'..'9']) and CharInSet(s2[i], ['0'..'9']))then
      begin
        a1 := a1 * 10 + Byte(s1[i]) - 48;
        a2 := a2 * 10 + Byte(s2[i]) - 48;
      end
      else if(CharInSet(s1[i], ['0'..'9']))then
        t := 1
      else if(CharInSet(s2[i], ['0'..'9']))then
        t := -1
      else if(a1 = a2)then
        t := Byte(s1[i]) - Byte(s2[i])
      else
        t := a1 - a2;

      if(t <> 0)then
      begin
        result := t;
        exit;
      end;
    end;


    if(result = 0)then
      result := a1 - a2;
end;

function StrPadR(s: string; newlen: Integer): string;
var
  len: Integer;
begin
  len := Length(s);
  Result := s;
  if len < newlen then
    Result := Result + StringOfChar(' ', newlen - len);
end;


/////////////////////
// ������������ � https://delphisources.ru/pages/faq/base/cyr_to_lat.html

{ **** UBPFD *********** by delphibase.endimus.com ****
>> �������������� ��������� � ������

�������������� ������ ���� '��������������' � 'Transliteratsiya' �� �������� �����

�����������: System
�����:       �������� ������, sergeante@mail.ru, ICQ:93240449, ��������
Copyright:   �������� �.�., 2002
����:        18 ������� 2002 �.
***************************************************** }

function TranslitRus2Lat(const Str: string): string;
const
  RArrayL = '��������������������������������';
  RArrayU = '�����Ũ��������������������������';
  colChar = 33;
  arr: array[1..2, 1..ColChar] of string =
  (('a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'y',
    'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f',
    'kh', 'ts', 'ch', 'sh', 'shch', '''', 'y', '''', 'e', 'yu', 'ya'),
    ('A', 'B', 'V', 'G', 'D', 'E', 'Yo', 'Zh', 'Z', 'I', 'Y',
    'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F',
    'Kh', 'Ts', 'Ch', 'Sh', 'Shch', '''', 'Y', '''', 'E', 'Yu', 'Ya'));
var
  i: Integer;
  LenS: Integer;
  p: integer;
  d: byte;
begin
  result := '';
  LenS := length(str);
  for i := 1 to lenS do
  begin
    d := 1;
    p := pos(str[i], RArrayL);
    if p = 0 then
    begin
      p := pos(str[i], RArrayU);
      d := 2
    end;
    if p <> 0 then
      result := result + arr[d, p]
    else
      result := result + str[i]; //���� �� ������� �����, �� ����� ��������
  end;
end;

function AddComboLines(sl: TStrings; text: string; maxi: Integer = 10): string;
var
    ind: Integer;
begin
  Result := '';

  ind := sl.IndexOf(text);

  if(ind >= 0)then
    sl.Move(ind, 0)
  else if(ind < 0)then
  begin
    sl.Insert(0, Text);

    if(sl.Count > maxi)then
    begin
      Result := sl[maxi];
      sl.Delete(maxi);
    end;
  end;
end;

end.
