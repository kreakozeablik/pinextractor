unit UDlgGroupForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, PinDoc,
  VirtualTrees, TB2Dock, TB2Toolbar, TBX, System.Actions, Vcl.ActnList,
  Vcl.Menus, TB2Item, UdmCommonData, ActiveX;

const
  // Helper message to decouple node change handling from edit handling.
  WM_STARTEDITING = WM_USER + 778;

type
  TDlgGroupForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    edtFiltr: TEdit;
    cbVisible: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    trBuses: TVirtualStringTree;
    Label2: TLabel;
    TBXToolbar1: TTBXToolbar;
    PopupMenu: TTBXPopupMenu;
    ActionList: TActionList;
    actBusAdd: TAction;
    actDelBus: TAction;
    TBXItem1: TTBXItem;
    TBXItem2: TTBXItem;
    TBXSeparatorItem1: TTBXSeparatorItem;
    TBXItem3: TTBXItem;
    TBXSeparatorItem2: TTBXSeparatorItem;
    TBXItem4: TTBXItem;
    actBusClear: TAction;
    TBXItem5: TTBXItem;
    procedure FormCreate(Sender: TObject);
    procedure trBusesGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure trBusesNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; NewText: string);
    procedure trBusesCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure trBusesColumnClick(Sender: TBaseVirtualTree; Column: TColumnIndex;
      Shift: TShiftState);
    procedure actBusAddExecute(Sender: TObject);
    procedure actDelBusUpdate(Sender: TObject);
    procedure actDelBusExecute(Sender: TObject);
    procedure actBusClearUpdate(Sender: TObject);
    procedure actBusClearExecute(Sender: TObject);
    procedure trBusesDragAllowed(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; var Allowed: Boolean);
    procedure trBusesDragDrop(Sender: TBaseVirtualTree; Source: TObject;
      DataObject: IDataObject; Formats: TFormatArray; Shift: TShiftState;
      Pt: TPoint; var Effect: Integer; Mode: TDropMode);
    procedure trBusesDragOver(Sender: TBaseVirtualTree; Source: TObject;
      Shift: TShiftState; State: TDragState; Pt: TPoint; Mode: TDropMode;
      var Effect: Integer; var Accept: Boolean);
    procedure trBusesEditing(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; var Allowed: Boolean);
    procedure trBusesExit(Sender: TObject);
  private
    FBuses: TBusList;
    FAfterDrop: Boolean;

    procedure WMStartEditing(var Message: TMessage); message WM_STARTEDITING;
  public
    function Edit(grp: TGroup): Boolean;
  end;

var
  DlgGroupForm: TDlgGroupForm;

implementation

uses VirtualTreeViewEditors, CoolEditor, System.UITypes;

{$R *.dfm}

{ TDlgGroupForm }

procedure TDlgGroupForm.actBusAddExecute(Sender: TObject);
var
  Node: PVirtualNode;
begin
  FBuses.Add(TBus.Create());
  Node := trBuses.AddChild(trBuses.RootNode);
  trBuses.Invalidate;

  PostMessage(Self.Handle, WM_STARTEDITING, WPARAM(Node), 0);
end;

procedure TDlgGroupForm.actBusClearExecute(Sender: TObject);
begin
  if (MessageDlg('������� ��� ����?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    trBuses.CancelEditNode();
    FBuses.Clear;
    trBuses.RootNodeCount := 0;
  end;
end;

procedure TDlgGroupForm.actBusClearUpdate(Sender: TObject);
begin
  actBusClear.Enabled := FBuses.Count > 0;
end;

procedure TDlgGroupForm.actDelBusExecute(Sender: TObject);
var
  Node: PVirtualNode;
begin
  trBuses.CancelEditNode;
  Node := trBuses.GetFirstSelected();
  if Assigned(Node) then
  begin
    trBuses.CancelEditNode();
    FBuses.Delete(Node.Index);
    trBuses.DeleteNode(Node);
  end;
end;

procedure TDlgGroupForm.actDelBusUpdate(Sender: TObject);
begin
  actDelBus.Enabled := Assigned(trBuses.GetFirstSelected());
end;

function TDlgGroupForm.Edit(grp: TGroup): Boolean;
begin
  edtFiltr.Text := grp.Filtr;
  cbVisible.Checked := grp.Visible;
  CopyBuses(grp.Buses, FBuses);
  trBuses.RootNodeCount := FBuses.Count;

  Result := False;
  if ShowModal = mrOk then
  begin
    grp.Filtr := edtFiltr.Text;
    grp.Visible := cbVisible.Checked;
    CopyBuses(FBuses, grp.Buses);
    Result := True;
  end;
end;

procedure TDlgGroupForm.FormCreate(Sender: TObject);
begin
  FBuses := TBusList.Create;
  FAfterDrop := False;

  edtFiltr.Hint := '���������� ��������� ������� ��:'#13+
'c - ������������ ������� �������'#13 +
'* - ����� ������ ���� � ����� ���'#13 +
'? - ����� ������ ���� ���'#13+
'# - ����� (���� ��� ����� ����)'#13+
'\d - ���� �����'#13 +
'\n - ���� ��� ����� ����'#13  +
'\c - ���� �����'#13  +
'\S - ���� ��� ����� ����'#13 +
'[�����] - ����� ������ �� ������ ���� � ����� ���'#13 +
'{�����} - ����� ������ �� ������ ���� ���'#13+
'     ����� ����� ��������� ��������:'#13+
'       ������_������-���������_������'#13+
'\\ - "\"'#13+
'\{ - "{"'#13+
'\[ - "["';
end;

procedure TDlgGroupForm.trBusesColumnClick(Sender: TBaseVirtualTree; Column: TColumnIndex; Shift: TShiftState);
var
  Node: PVirtualNode;
begin
  if FAfterDrop then
  begin
    FAfterDrop := False;
    exit;
  end;

  Node := trBuses.GetFirstSelected();
  if Assigned(Node) then
    PostMessage(Self.Handle, WM_STARTEDITING, WPARAM(Node), Column);
end;

procedure TDlgGroupForm.trBusesCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
var
  ne: TVTNumberCellEditor;
  fe: TVTComboCellEditor;
begin
  case Column of
  0: EditLink := nil;
  1, 2: begin
      ne := TVTNumberCellEditor.Create(Sender);
      ne.Editor.Base := cebUnsigned;
      EditLink := ne;
    end;
  3: begin
      fe := TVTComboCellEditor.Create(Sender);
      fe.Editor.Style := csDropDownList;
      fe.Editor.Items.Text := '<'#13'>';
      EditLink := fe;
    end;
  end;
end;

procedure TDlgGroupForm.trBusesDragAllowed(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := (Sender = trBuses);
end;

procedure TDlgGroupForm.trBusesDragDrop(Sender: TBaseVirtualTree;
  Source: TObject; DataObject: IDataObject; Formats: TFormatArray;
  Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
var
  Src: PVirtualNode;
  Dst: PVirtualNode;
begin
  Src := trBuses.FocusedNode;
  Dst := trBuses.DropTargetNode;

  if Assigned(Src) and Assigned(Dst) then
  begin
    FBuses.Move(Src.Index, Dst.Index);
    trBuses.Invalidate();
  end;
  FAfterDrop := True;
end;

procedure TDlgGroupForm.trBusesDragOver(Sender: TBaseVirtualTree;
  Source: TObject; Shift: TShiftState; State: TDragState; Pt: TPoint;
  Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
begin
  Accept := Sender = Source;
end;

procedure TDlgGroupForm.trBusesEditing(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := (Integer(Node.Index) < FBuses.Count);
end;

procedure TDlgGroupForm.trBusesExit(Sender: TObject);
begin
  trBuses.EndEditNode;
end;

procedure TDlgGroupForm.trBusesGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
  function FrmatInt(v: Integer): string;
  begin
    if v < 0 then Result := ''
    else Result := IntToStr(v);
  end;
  function FrmatReverce(v: Boolean): string;
  begin
    if v then Result := '>'
    else Result := '<';
  end;
begin
  if (Integer(Node.Index) < FBuses.Count) and Assigned(Node.Parent) then
    case Column of
    0: CellText := FBuses[Node.Index].Name;
    1: CellText := FrmatInt(FBuses[Node.Index].Count);
    2: CellText := FrmatInt(FBuses[Node.Index].StartInd);
    3: CellText := FrmatReverce(FBuses[Node.Index].Reverce);
    end;
end;

procedure TDlgGroupForm.trBusesNewText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; NewText: string);
  function FrmatReverce(): Integer;
  begin
    if NewText.IsEmpty then Result := -1
    else Result := StrToIntDef(NewText, 0);
  end;
begin
  if (Integer(Node.Index) < FBuses.Count) then
    case Column of
    0: FBuses[Node.Index].Name := NewText;
    1: FBuses[Node.Index].Count := FrmatReverce();
    2: FBuses[Node.Index].StartInd := FrmatReverce();
    3: FBuses[Node.Index].Reverce := (NewText = '>');
    end;
end;

procedure TDlgGroupForm.WMStartEditing(var Message: TMessage);
begin
  trBuses.EditNode(Pointer(Message.WParam), Message.LParam);
end;

end.
