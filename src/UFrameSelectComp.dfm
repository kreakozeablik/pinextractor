object FrameSelectComp: TFrameSelectComp
  Left = 0
  Top = 0
  Width = 643
  Height = 234
  TabOrder = 0
  OnResize = FrameResize
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 643
    Height = 55
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 47
      Top = 11
      Width = 42
      Height = 13
      Caption = #1060#1080#1083#1100#1090#1088':'
    end
    object Label3: TLabel
      Left = 22
      Top = 35
      Width = 67
      Height = 13
      Caption = #1050#1086#1084#1087#1086#1085#1077#1085#1090#1099':'
    end
    object edtFiltr: TComboBox
      Left = 95
      Top = 8
      Width = 292
      Height = 21
      TabOrder = 0
      OnChange = edtFiltrChange
      OnKeyPress = edtFiltrKeyPress
    end
    object cbViewAll: TCheckBox
      Left = 546
      Top = 10
      Width = 97
      Height = 17
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1089#1077
      TabOrder = 1
      OnClick = edtFiltrChange
    end
  end
  object sgSelComp: TStringGrid
    Left = 0
    Top = 55
    Width = 643
    Height = 179
    Align = alClient
    ColCount = 3
    DefaultColWidth = 30
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goHorzLine, goDrawFocusSelected, goColSizing, goRowSelect]
    TabOrder = 1
    OnDblClick = sgSelCompDblClick
    OnKeyPress = sgSelCompKeyPress
    OnSelectCell = sgSelCompSelectCell
    ColWidths = (
      30
      30
      30)
    RowHeights = (
      20)
  end
end
