unit UFormSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, PinDoc,
  Vcl.Grids, Vcl.ComCtrls, Vcl.ToolWin, Vcl.Menus, System.Actions, Vcl.ActnList,
  System.ImageList, Vcl.ImgList, Vcl.ActnMan, TB2Item, TBX, TB2Dock, TB2Toolbar, UdmCommonData;

type
  TFormSettings = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    mmFiltrList: TMemo;
    GroupBox2: TGroupBox;
    Panel2: TPanel;
    cbCommentGroups: TCheckBox;
    cbCommentNet: TCheckBox;
    cbCommentSheet: TCheckBox;
    cbCommentComponent: TCheckBox;
    cbCommentPin: TCheckBox;
    cbVisibleFiltered: TCheckBox;
    GroupBox3: TGroupBox;
    sgReplace: TStringGrid;
    alReplace: TActionList;
    pmReplace: TPopupMenu;
    actReplaceAdd: TAction;
    actReplaceDel: TAction;
    actClean: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    TBXToolbar1: TTBXToolbar;
    TBXItem1: TTBXItem;
    TBXItem2: TTBXItem;
    TBXItem3: TTBXItem;
    TBXSeparatorItem1: TTBXSeparatorItem;
    btnSetAsDefault: TButton;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure actReplaceAddExecute(Sender: TObject);
    procedure actCleanExecute(Sender: TObject);
    procedure actReplaceDelExecute(Sender: TObject);
    procedure sgReplaceContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure alReplaceUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure btnSetAsDefaultClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetToOptions(opt: TPinDocOptions);
  end;

function FormSettingsDlgExecute(Doc: TPinDoc): Boolean;

implementation

uses UITypes, AppConfig;

{$R *.dfm}

function FormSettingsDlgExecute(Doc: TPinDoc): Boolean;
var
  rd: TReplaceSubStrData;
  i: Integer;
begin
  with TFormSettings.Create(Application) do
  begin
    mmFiltrList.Lines.Assign(Doc.Options.PinNameFilters);

    i := Doc.Options.ReplaceSubStrDatas.Count;
    if i > 0 then
      Inc(i)
    else
      i := 2;
    sgReplace.RowCount := i;

    i := 1;
    for rd in Doc.Options.ReplaceSubStrDatas do
    begin
      sgReplace.Cells[0, i] := rd.Condition;
      sgReplace.Cells[1, i] := rd.SubStr;
      sgReplace.Cells[2, i] := rd.Repl;
      Inc(i);
    end;

    cbCommentGroups.Checked    := fcGroup     in Doc.Options.FormatComment;
    cbCommentNet.Checked       := fcNet       in Doc.Options.FormatComment;
    cbCommentSheet.Checked     := fcSheet     in Doc.Options.FormatComment;
    cbCommentComponent.Checked := fcComponent in Doc.Options.FormatComment;
    cbCommentPin.Checked       := fcPinName   in Doc.Options.FormatComment;

    cbVisibleFiltered.Checked  := Doc.Options.VisibleFiltered;

    Result := (ShowModal() = mrOk);

    if Result then
    begin
      SetToOptions(Doc.Options);
      Doc.UpdateGroupsPins;
    end;

    Free;
  end;
end;

procedure TFormSettings.actCleanExecute(Sender: TObject);
begin
  if (MessageDlg('������� ��� ������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    sgReplace.RowCount := 2;
    sgReplace.Cells[0, 1] := '';
    sgReplace.Cells[1, 1] := '';
    sgReplace.Cells[2, 1] := '';
  end;
end;

procedure TFormSettings.actReplaceAddExecute(Sender: TObject);
begin
  sgReplace.RowCount := sgReplace.RowCount + 1;
  sgReplace.Row := sgReplace.RowCount - 1;
end;

procedure TFormSettings.actReplaceDelExecute(Sender: TObject);
var
  i: Integer;
begin
  if(sgReplace.RowCount = 2)then
  begin
    sgReplace.Cells[0, 1] := '';
    sgReplace.Cells[1, 1] := '';
    sgReplace.Cells[2, 1] := '';
  end else
  begin
    for i := sgReplace.Row to sgReplace.RowCount - 2 do
      sgReplace.Rows[i] := sgReplace.Rows[i + 1];

    sgReplace.RowCount := sgReplace.RowCount - 1;
  end;
end;

procedure TFormSettings.alReplaceUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
  v: Boolean;
begin
  v := (sgReplace.RowCount > 2) or
    (sgReplace.Cells[0, 1] <> '') or
    (sgReplace.Cells[1, 1] <> '') or
    (sgReplace.Cells[2, 1] <> '');
  actReplaceDel.Enabled := v and (sgReplace.Row > -1);
  actClean.Enabled := v;
end;

procedure TFormSettings.btnSetAsDefaultClick(Sender: TObject);
var
  dval: TPinDocOptions;
begin
  if (MessageDlg('������������ ��������� �� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    dval := TPinDocOptions.Create;
    SetToOptions(dval);
    Config.WriteJSONObj(CONFIG_OPTIONS, dval.Save());
    dval.Free;
  end;
end;

procedure TFormSettings.FormCreate(Sender: TObject);
begin
  sgReplace.Cells[0, 0] := '�������';
  sgReplace.Cells[1, 0] := '������';
  sgReplace.Cells[2, 0] := '�������� ��';

  mmFiltrList.Hint := '�� ������ ������� �� ����������� ���������.'#13+
'! � ������ ��������� - �������������. '#13+
'���������� ��������� ������� ��:'#13+
'c - ������������ ������� �������'#13 +
'* - ����� ������ ���� � ����� ���'#13 +
'? - ����� ������ ���� ���'#13+
'# - ����� (���� ��� ����� ����)'#13+
'\d - ���� �����'#13 +
'\n - ���� ��� ����� ����'#13  +
'\c - ���� �����'#13  +
'\S - ���� ��� ����� ����'#13 +
'[�����] - ����� ������ �� ������ ���� � ����� ���'#13 +
'{�����} - ����� ������ �� ������ ���� ���'#13+
'     ����� ����� ��������� ��������:'#13+
'       ������_������-���������_������'#13+
'\\ - "\"'#13+
'\{ - "{"'#13+
'\[ - "["';
end;

procedure TFormSettings.FormResize(Sender: TObject);
begin
  sgReplace.DefaultColWidth := sgReplace.ClientWidth mod 3;
end;

procedure TFormSettings.SetToOptions(opt: TPinDocOptions);
  procedure FiltrEmptyLine(src: TStrings; dst: TStrings);
  var
    s: string;
  begin
    dst.Clear;
    for s in src do
      if not s.IsEmpty then
        dst.Add(s);
  end;
var
  fc: TFormatCommentSet;
  rd: TReplaceSubStrData;
  i: Integer;
begin
  FiltrEmptyLine(mmFiltrList.Lines, opt.PinNameFilters);

  opt.ReplaceSubStrDatas.Clear();
  for i := 1 to sgReplace.RowCount - 1 do
    if not sgReplace.Cells[1, i].IsEmpty then
    begin
      rd := TReplaceSubStrData.Create();
      rd.Condition := sgReplace.Cells[0, i];
      rd.SubStr := sgReplace.Cells[1, i];
      rd.Repl := sgReplace.Cells[2, i];
      opt.ReplaceSubStrDatas.Add(rd);
    end;

  fc := [];
  if cbCommentGroups.Checked then
    fc := fc + [fcGroup];
  if cbCommentNet.Checked then
    fc := fc + [fcNet];
  if cbCommentSheet.Checked then
    fc := fc + [fcSheet];
  if cbCommentComponent.Checked then
    fc := fc + [fcComponent];
  if cbCommentPin.Checked then
    fc := fc + [fcPinName];

  opt.FormatComment := fc;

  opt.VisibleFiltered :=  cbVisibleFiltered.Checked;
end;

procedure TFormSettings.sgReplaceContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var
  c: Integer;
  r: Integer;
begin
  sgReplace.MouseToCell(MousePos.X, MousePos.Y, c, r);
  if r > 0 then
  begin
    sgReplace.Row := r;
    sgReplace.Col := c;
  end else
    Handled := True;
end;

end.
