unit UFormMezaninConnection;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrameSelectComp, Vcl.StdCtrls,
  Vcl.ExtCtrls, PinDoc, Shema, Vcl.Grids;

type
  TOnValidata = function (c: TComp): Boolean of object;

  TFormMezaninConnection = class(TForm)
    Panel2: TPanel;
    btnCancel: TButton;
    Button2: TButton;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    fscMainBoard: TFrameSelectComp;
    fscMezaninBoard: TFrameSelectComp;
    Panel1: TPanel;
    grdPins: TStringGrid;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    FConnection: TMezaninConnection;
    FMainBoard: TShema;
    FMezaninBoard: TShema;
    FOnValidateMezanin: TOnValidata;
    FOnValidateMain: TOnValidata;

    procedure OnFilteredComps();
    procedure OnSelectComp(Comp: TComp);
    procedure OnDbClkSelectComp(Comp: TComp);
  public
    property OnValidateMain: TOnValidata read FOnValidateMain write FOnValidateMain;
    property OnValidateMezanin: TOnValidata read FOnValidateMezanin write FOnValidateMezanin;

    procedure Init(MainBoard: TShema; MezaninBoard: TShema); overload;
    procedure Init(MainBoard: TShema; MezaninBoard: TShema; Connection: TMezaninConnection); overload;

    property Connection: TMezaninConnection read FConnection;
  end;

var
  FormMezaninConnection: TFormMezaninConnection;

implementation

uses System.UITypes, System.Generics.Collections, Math, System.Generics.Defaults, strutil;

{$R *.dfm}

{ TFormMezaninConnection }

procedure TFormMezaninConnection.Init(MainBoard, MezaninBoard: TShema;
  Connection: TMezaninConnection);
begin
  Init(MainBoard, MezaninBoard);

  FConnection.Assign(Connection);
  fscMainBoard.SelectComp(FConnection.CompMainBoardName);
  fscMezaninBoard.SelectComp(FConnection.CompMezaninBoardName);

  OnSelectComp(nil);
end;

procedure TFormMezaninConnection.OnDbClkSelectComp(Comp: TComp);
begin
  if Assigned(fscMainBoard.GetComp()) and Assigned(fscMezaninBoard.GetComp()) then
    Button2Click(nil);
end;

procedure TFormMezaninConnection.OnFilteredComps;
begin
  Button2.Enabled := fscMainBoard.Selected and fscMezaninBoard.Selected;
end;

type
  TConPinData = record
    net: string;
    pin: string;
  end;

  TConData = record
    main: TConPinData;
    mez: TConPinData;
  end;

  TConDataList = TList<TConData>;

procedure CreateConDataList(cmain, cmez: TComp; ConDataList: TConDataList);
var
  i, j: Integer;
  cd: TConData;
  p: TPin;
  mainp: string;
  mezp: string;
begin
  if Assigned(cmain) and not Assigned(cmez) then
  begin
    for p in cmain.Pins do
    begin
      cd.main.pin := p.Designator;
      cd.main.net := p.Net.Name;
      cd.mez.pin := '';
      cd.mez.net := '';
      ConDataList.Add(cd);
    end;
  end
  else if not Assigned(cmain) and Assigned(cmez) then
  begin
    for p in cmez.Pins do
    begin
      cd.main.pin := '';
      cd.main.net := '';
      cd.mez.pin := p.Designator;
      cd.mez.net := p.Net.Name;
      ConDataList.Add(cd);
    end;
  end
  else if Assigned(cmain) and Assigned(cmez) then
  begin
    i := 0;
    j := 0;
    while True do
    begin
      if i >= cmain.Pins.Count then
      begin
        while j < cmez.Pins.Count do
        begin
          cd.main.pin := '';
          cd.main.net := '';
          cd.mez.pin := cmez.Pins[j].Designator;
          cd.mez.net := cmez.Pins[j].Net.Name;
          ConDataList.Add(cd);
          Inc(j);
        end;
        exit;
      end;

      if j >= cmez.Pins.Count then
      begin
        while i < cmain.Pins.Count do
        begin
          cd.main.pin := cmain.Pins[i].Designator;
          cd.main.net := cmain.Pins[i].Net.Name;
          cd.mez.pin := '';
          cd.mez.net := '';
          ConDataList.Add(cd);
          Inc(i);
        end;
        exit;
      end;

      mainp := cmain.Pins[i].Designator;
      mezp := cmez.Pins[j].Designator;

      if mainp = mezp then
      begin
        cd.main.pin := cmain.Pins[i].Designator;
        cd.main.net := cmain.Pins[i].Net.Name;
        cd.mez.pin := cmez.Pins[j].Designator;
        cd.mez.net := cmez.Pins[j].Net.Name;
        ConDataList.Add(cd);
        Inc(i);
        Inc(j);
      end
      else if mainp > mezp then
      begin
        cd.main.pin := '';
        cd.main.net := '';
        cd.mez.pin := cmez.Pins[j].Designator;
        cd.mez.net := cmez.Pins[j].Net.Name;
        ConDataList.Add(cd);
        Inc(j);
      end
      else if mainp < mezp then
      begin
        cd.main.pin := cmain.Pins[i].Designator;
        cd.main.net := cmain.Pins[i].Net.Name;
        cd.mez.pin := '';
        cd.mez.net := '';
        ConDataList.Add(cd);
        Inc(i);
      end;
    end;
  end;
end;

procedure TFormMezaninConnection.OnSelectComp(Comp: TComp);
var
  cmain: TComp;
  cmez: TComp;
  ConDataList: TConDataList;
  i: Integer;
begin
  cmain := fscMainBoard.GetComp;
  if Assigned(cmain) then
    grdPins.Cells[1, 0] := cmain.Name
  else
    grdPins.Cells[1, 0] := '-';

  cmez := fscMezaninBoard.GetComp;
  if Assigned(cmez) then
    grdPins.Cells[2, 0] := cmez.Name
  else
    grdPins.Cells[2, 0] := '-';

  grdPins.RowCount := 3;
  grdPins.Cells[0, 3] := '';
  grdPins.Cells[1, 3] := '';
  grdPins.Cells[2, 3] := '';
  grdPins.Cells[3, 3] := '';

  ConDataList := TConDataList.Create;

  if Assigned(cmain) then
      cmain.Pins.Sort(TComparer<TPin>.Construct(
      function (const p1: TPin; const p2: TPin): Integer
      begin
        Result := StringSortCompare(p1.Designator, p2.Designator);
      end
    ));

  if Assigned(cmez) then
      cmez.Pins.Sort(TComparer<TPin>.Construct(
      function (const p1: TPin; const p2: TPin): Integer
      begin
        Result := StringSortCompare(p1.Designator, p2.Designator);
      end
    ));

  CreateConDataList(cmain, cmez, ConDataList);
  try
    grdPins.RowCount := max(grdPins.FixedRows + ConDataList.Count, 3);
    for i := 0 to ConDataList.Count - 1 do
    begin
      grdPins.Cells[0, i + 2] := ConDataList[i].main.net;
      grdPins.Cells[1, i + 2] := ConDataList[i].main.pin;
      grdPins.Cells[2, i + 2] := ConDataList[i].mez.pin;
      grdPins.Cells[3, i + 2] := ConDataList[i].mez.net;
    end;
  finally
    ConDataList.Free;
  end;
end;

procedure TFormMezaninConnection.Init(MainBoard, MezaninBoard: TShema);
begin
  FMainBoard := MainBoard;
  fscMainBoard.CompPrefix := 'X';
  fscMainBoard.Init(OnDbClkSelectComp, OnFilteredComps, OnSelectComp);
  fscMainBoard.SetCompList(FMainBoard);

  FMezaninBoard := MezaninBoard;
  fscMezaninBoard.CompPrefix := 'X';
  fscMezaninBoard.Init(OnDbClkSelectComp, OnFilteredComps, OnSelectComp);
  fscMezaninBoard.SetCompList(FMezaninBoard);
end;

procedure TFormMezaninConnection.Button2Click(Sender: TObject);
begin
  if not Assigned(fscMainBoard.GetComp()) then
  begin
    MessageDlg('�������� ���� �������� �����!', mtWarning, [mbOK], 0);
    exit;
  end;

  if not Assigned(fscMezaninBoard.GetComp()) then
  begin
    MessageDlg('�������� ���� ��������!', mtWarning, [mbOK], 0);
    exit;
  end;

  if Assigned(FOnValidateMain) and
    (fscMainBoard.GetComp <> FConnection.CompMainBoard) and
    (not FOnValidateMain(fscMainBoard.GetComp)) then
  begin
    MessageDlg('���� '+fscMainBoard.GetComp.Name+' �������� ����� ��� ���������!', mtWarning, [mbOK], 0);
    exit;
  end;

  if Assigned(FOnValidateMezanin) and
    (fscMezaninBoard.GetComp <> FConnection.CompMezaninBoard) and
    (not FOnValidateMezanin(fscMezaninBoard.GetComp)) then
  begin
    MessageDlg('���� '+fscMezaninBoard.GetComp.Name+' �������� ��� ���������!', mtWarning, [mbOK], 0);
    exit;
  end;

  if (Assigned(fscMainBoard.GetComp()) and Assigned(fscMezaninBoard.GetComp())) then
  begin
    FConnection.CompMainBoard := fscMainBoard.GetComp;
    FConnection.CompMainBoardName := FConnection.CompMainBoard.Name;
    FConnection.CompMezaninBoard := fscMezaninBoard.GetComp;
    FConnection.CompMezaninBoardName := FConnection.CompMezaninBoard.Name;

    ModalResult := mrOk;
  end;
end;

procedure TFormMezaninConnection.FormCreate(Sender: TObject);
begin
  FConnection := TMezaninConnection.Create;

  grdPins.Cells[0, 0] := '�������� �����';
  grdPins.Cells[1, 0] := '-';
  grdPins.Cells[2, 0] := '-';
  grdPins.Cells[3, 0] := '�������';

  grdPins.Cells[0, 1] := 'Net';
  grdPins.Cells[1, 1] := 'Pin';
  grdPins.Cells[2, 1] := 'Pin';
  grdPins.Cells[3, 1] := 'Net';
end;

procedure TFormMezaninConnection.FormDestroy(Sender: TObject);
begin
  FConnection.Free;
end;

procedure TFormMezaninConnection.FormResize(Sender: TObject);
var
  w: Integer;
  sbw: Integer;
const
  ColPinWidth = 50;
begin
  if ((GetWindowLong(grdPins.Handle, GWL_STYLE) and WS_VSCROLL) <> 0) then
    sbw := GetSystemMetrics(SM_CXVSCROLL)
  else
    sbw := 0;

  grdPins.ColWidths[1] := ColPinWidth;
  grdPins.ColWidths[2] := ColPinWidth;

  w := ((grdPins.ClientWidth - sbw) div 2) - ColPinWidth;

  grdPins.ColWidths[0] := w;
  grdPins.ColWidths[3] := w;
end;

end.
