unit TAppConfig;

interface

uses Classes;

type
  TAppConfig = class(TObject)
  private
    FSelectCompForm_Filtr: TStringList;
  published
    constructor Create();
    destructor Destroy(); override;
    property SelectCompForm_Filtr: TStringList read FSelectCompForm_Filtr;
  end;

implementation

{ AppConfig }

constructor TAppConfig.Create();
begin
  FSelectCompForm_Filtr := TStringList.Create;
end;

destructor TAppConfig.Destroy;
begin
  FSelectCompForm_Filtr.Free;
  inherited;
end;

end.
