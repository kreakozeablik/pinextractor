unit UConstraintsFormatter;

interface

uses Classes, SynEditHighlighter, PinDoc, Shema, System.Generics.Collections;

type
  TBaseDocFormatter = class
  public
    procedure Format(doc: TPinDoc; res: TStrings); virtual; abstract;
  end;

  TBaseFormat = class
  public
    function GetName: string; virtual; abstract;
    function GetFileExt: string; virtual; abstract;
    function GetHighlighter: TSynCustomHighlighter; virtual;
    function GetFormatter(): TBaseDocFormatter; virtual; abstract;
    function GetIconResurceName(): string; virtual;
  end;

  {Formatter helpers}

  TBaseGroupFormatter = class
  public
    procedure Format(g: TGroup; gname: string; res: TStrings; opt: TFormatCommentSet); virtual; abstract;
  end;

  TCommonDocFormatter = class(TBaseDocFormatter)
  private
    FGroupFormatter: TBaseGroupFormatter;
  public
    constructor Create(GroupFormatter: TBaseGroupFormatter);
    destructor Destroy(); override;
    procedure Format(doc: TPinDoc; res: TStrings); override;
  end;

  { Common line formatter }

  TBasePinFormatter = class
  public
    function Format(pin: string; net: string; ind: Integer): string; virtual; abstract;
  end;

  TCommonLineGroupFormatter = class(TBaseGroupFormatter)
  private
    FPinFormatter: TBasePinFormatter;
    FLineComment: string;
    FLineComment2: string;
  public
    constructor Create(LineComment: string; LineComment2: string; PinFormatter: TBasePinFormatter);
    destructor Destroy(); override;
    procedure Format(g: TGroup; gname: string; res: TStrings; opt: TFormatCommentSet); override;
  end;

  { CSV }

  TCSVFormat = class(TBaseFormat)
  public
    function GetName: string; override;
    function GetFileExt: string; override;
    function GetIconResurceName: string; override;
    function GetFormatter(): TBaseDocFormatter; override;
    function GetHighlighter: TSynCustomHighlighter; override;
  end;

  TCSVDocFormatter = class(TBaseDocFormatter)
  public
    procedure Format(doc: TPinDoc; res: TStrings); override;
  end;

  { UCF }

  TUCFPinFormatter = class(TBasePinFormatter)
  public
    function Format(pin: string; net: string; ind: Integer): string; override;
  end;

  TUCFFormat = class(TBaseFormat)
  public
    function GetName: string; override;
    function GetFileExt: string; override;
    function GetIconResurceName(): string; override;
    function GetHighlighter: TSynCustomHighlighter; override;
    function GetFormatter(): TBaseDocFormatter; override;
  end;

  { XDC }

  TXDCPinFormatter = class(TBasePinFormatter)
  public
    function Format(pin: string; net: string; ind: Integer): string; override;
  end;

  TXDCFormat = class(TBaseFormat)
  public
    function GetName: string; override;
    function GetFileExt: string; override;
    function GetIconResurceName: string; override;
    function GetHighlighter: TSynCustomHighlighter; override;
    function GetFormatter(): TBaseDocFormatter; override;
  end;

  { LPF }

  TLPFPinFormatter = class(TBasePinFormatter)
  public
    function Format(pin: string; net: string; ind: Integer): string; override;
  end;

  TLPFFormat = class(TBaseFormat)
  public
    function GetName: string; override;
    function GetFileExt: string; override;
    function GetIconResurceName: string; override;
    function GetHighlighter: TSynCustomHighlighter; override;
    function GetFormatter(): TBaseDocFormatter; override;
  end;

  { QSF }

  TQSFPinFormatter = class(TBasePinFormatter)
  public
    function Format(pin: string; net: string; ind: Integer): string; override;
  end;

  TQSFFormat = class(TBaseFormat)
  public
    function GetName: string; override;
    function GetFileExt: string; override;
    function GetIconResurceName: string; override;
    function GetHighlighter: TSynCustomHighlighter; override;
    function GetFormatter(): TBaseDocFormatter; override;
  end;

  { HDLDocFormatter }

  THDLHelperDocFormatter = class
  public
    procedure Header(doc: TPinDoc; res: TStrings); virtual; abstract;
    function FormPort(name: string; last: Boolean): string; virtual; abstract;
    function FormBus(name: string; h: Integer; l: Integer; last: Boolean): string; virtual; abstract;
    procedure Fother(doc: TPinDoc; res: TStrings); virtual; abstract;
  end;

  THDLDocFormatter = class(TBaseDocFormatter)
  protected
    FComment: string;
    FRowFormatter: THDLHelperDocFormatter;
  public
    constructor Create(Comment: string; RowFormatter: THDLHelperDocFormatter);
    destructor Destroy(); override;
    procedure Format(doc: TPinDoc; res: TStrings); override;
  end;

  { Verilog }

  TVerilogFormat = class(TBaseFormat)
  public
    function GetName: string; override;
    function GetFileExt: string; override;
    function GetIconResurceName: string; override;
    function GetFormatter(): TBaseDocFormatter; override;
    function GetHighlighter: TSynCustomHighlighter; override;
  end;

  TVerilogHelperDocFormatter = class(THDLHelperDocFormatter)
  public
    procedure Header(doc: TPinDoc; res: TStrings); override;
    function FormPort(name: string; last: Boolean): string; override;
    function FormBus(name: string; h: Integer; l: Integer; last: Boolean): string; override;
    procedure Fother(doc: TPinDoc; res: TStrings); override;
  end;

  { VHDL }

  TVHDLFormat = class(TBaseFormat)
  public
    function GetName: string; override;
    function GetFileExt: string; override;
    function GetIconResurceName: string; override;
    function GetFormatter(): TBaseDocFormatter; override;
    function GetHighlighter: TSynCustomHighlighter; override;
  end;

  TVHDLHelperDocFormatter = class(THDLHelperDocFormatter)
  protected
    FEntityName: string;
  public
    procedure Header(doc: TPinDoc; res: TStrings); override;
    function FormPort(name: string; last: Boolean): string; override;
    function FormBus(name: string; h: Integer; l: Integer; last: Boolean): string; override;
    procedure Fother(doc: TPinDoc; res: TStrings); override;
  end;

  { GroupTabulator }

  TGroupTabulator = class
  private
    sl: TStringList;
    alen: array of Integer;
    FSeparator: string;
  public
    procedure SetSeparator(Separator: string);
    constructor Create(len: Integer = 10);
    destructor Destroy(); override;
    procedure Add(s: string; obj: TObject = nil);
    procedure MoveTo(dst: TStrings);
    procedure Clean();
  end;

  { TFormatReestr }

  TFormatReestr = TList<TBaseFormat>;

var
  FormatReestr: TFormatReestr;

implementation

uses Forms, SynHinhlightUCF, SysUtils, IOUtils, StrUtils, strutil;

function FormatComment(p: TPinData; options: TFormatCommentSet): string;
begin
  Result := '';
  if fcNet in options then
  begin
    Result := p.NetNameOriginal;

    if(p.MezaninData.Count > 0) then
      Result := Result +
                ' -> ' +
                p.MezaninData[0].MezaninConnection.Owner.Shema.PrjName +
                ' : ' +
                p.MezaninData[0].MezaninPin.NetName;
  end;

  if fcSheet in options then
  begin
    if Result <> '' then
      Result := Result + '; '#9;
    Result := Result + p.Sheet;
  end;

  if fcComponent in options then
  begin
    if Result <> '' then
      Result := Result + '; '#9;
    Result := Result + p.Part;
  end;

  if fcPinName in options then
  begin
    if Result <> '' then
      Result := Result + '; '#9;
    Result := Result + p.Name;
  end;
end;

function FormatPinInd(i: Integer; op: Char = '['; cls: Char = ']'): string;
begin
  if i >= 0 then
    Result := op + IntToStr(i) + cls
  else
    Result := '';
end;

{ TBaseFormatter }

function TBaseFormat.GetHighlighter: TSynCustomHighlighter;
begin
  result := nil;
end;

function TBaseFormat.GetIconResurceName: string;
begin
  result := '';
end;

{ TCSVFormatter }

function TCSVFormat.GetFileExt: string;
begin
  result := 'csv';
end;

function TCSVFormat.GetFormatter: TBaseDocFormatter;
begin
  result := TCSVDocFormatter.Create();
end;

function TCSVFormat.GetHighlighter: TSynCustomHighlighter;
var
  h: TSynHinhlightUCF;
begin
  h := TSynHinhlightUCF.Create(nil);
  h.CommentLine := #0;
  result := h;
end;

function TCSVFormat.GetIconResurceName: string;
begin
    result := 'XCSVICO';
end;

function TCSVFormat.GetName: string;
begin
  result := 'Text CSV';
end;

{ TCSVDocFormatter }

procedure TCSVDocFormatter.Format(doc: TPinDoc; res: TStrings);
var
  tab: TGroupTabulator;

  procedure PinFormat(g: TGroup; gname: string; res: TStrings;
    opt: TFormatCommentSet);
  var
    line: string;
    s: string;
    enumerator: TPinEnumerator;
  begin
    if not g.Visible then
      exit;

    if g.Pins.Count = 0 then
      exit;

    enumerator := g.GetPinEnumerator;

    while enumerator.Next() do
    begin
      if enumerator.Index < 0 then
        s := ''
      else
        s := '[' + IntToStr(enumerator.Index) + ']';

      line := enumerator.Net + s + '; '#9 + enumerator.Pin.Designator;

      if (fcGroup in opt) then
        line := line + '; '#9 + gname;

      if (opt - [fcGroup]) <> [] then
        line := line + '; '#9 + FormatComment(enumerator.Pin, opt);

      tab.Add(line, enumerator.Pin);
    end;

    enumerator.Free();
  end;

var
  group: TGroup;
begin
  tab := TGroupTabulator.Create();

  for group in doc.Groups do
    PinFormat(group, group.Name, res, doc.Options.FormatComment);

  PinFormat(doc.NoGroup, 'No grouped', res, doc.Options.FormatComment);

  if doc.Options.VisibleFiltered then
    PinFormat(doc.FilteredGroup, 'Filtered', res, doc.Options.FormatComment);

  tab.MoveTo(res);
  tab.Free;
end;

{ TCommonDocFormatter }

constructor TCommonDocFormatter.Create(
  GroupFormatter: TBaseGroupFormatter);
begin
  FGroupFormatter := GroupFormatter;
end;

destructor TCommonDocFormatter.Destroy;
begin
  FGroupFormatter.Free;
  inherited;
end;

procedure TCommonDocFormatter.Format(doc: TPinDoc; res: TStrings);
var
  group: TGroup;
begin
  for group in doc.Groups do
    FGroupFormatter.Format(group, group.Name, res, doc.Options.FormatComment);

  FGroupFormatter.Format(doc.NoGroup, 'No grouped', res, doc.Options.FormatComment);

  if doc.Options.VisibleFiltered then
    FGroupFormatter.Format(doc.FilteredGroup, 'Filtered', res, doc.Options.FormatComment);
end;

{ TCommonLineGroupFormatter }

constructor TCommonLineGroupFormatter.Create(LineComment: string; LineComment2: string; PinFormatter: TBasePinFormatter);
begin
  FPinFormatter := PinFormatter;
  FLineComment := LineComment;
  FLineComment2 := LineComment2;
end;

destructor TCommonLineGroupFormatter.Destroy;
begin
  FPinFormatter.Free;
  inherited;
end;

procedure TCommonLineGroupFormatter.Format(g: TGroup; gname: string;
  res: TStrings; opt: TFormatCommentSet);
  var
    line: string;
    tab: TGroupTabulator;
    enumerator: TPinEnumerator;
  begin
    if not g.Visible then
      exit;

    if g.Pins.Count = 0 then
      exit;

    tab := TGroupTabulator.Create();

    tab.Add('');
    if fcGroup in opt then
    begin
      tab.Add(FLineComment + ' ' + gname, g.PinDataPlug);
      tab.Add('');
    end;

    enumerator := g.GetPinEnumerator();

    while enumerator.Next() do
    begin
      line := FPinFormatter.Format(enumerator.Pin.Designator, enumerator.Net, enumerator.Index);

      if (opt - [fcGroup]) <> [] then
        line := line + FLineComment2 + ' ' + FormatComment(enumerator.Pin, opt);

      tab.Add(line, enumerator.Pin);
    end;

    enumerator.Free();

    tab.MoveTo(res);
    tab.Free;
  end;

{ TUCFFormatter }

function TUCFFormat.GetFileExt: string;
begin
  result := 'ucf';
end;

function TUCFFormat.GetFormatter: TBaseDocFormatter;
begin
  result := TCommonDocFormatter.Create(
      TCommonLineGroupFormatter.Create(
        '#', ' '#9'#',
        TUCFPinFormatter.Create()));
end;

function TUCFFormat.GetHighlighter: TSynCustomHighlighter;
var
  h: TSynHinhlightUCF;
begin
  h := TSynHinhlightUCF.Create(nil);
  h.SetKeywords('LOC,NET');
  h.CommentLine := '#';
  result := h;
end;

function TUCFFormat.GetIconResurceName: string;
begin
  result := 'UCFICO';
end;

function TUCFFormat.GetName: string;
begin
  result := 'Xilinx ISE UCF';
end;

{ TUCFPinFormatter }

function TUCFPinFormatter.Format(pin: string; net: string; ind: Integer): string;
begin
  Result := 'NET ' + net + FormatPinInd(ind) + #9' LOC = ' + pin + ';';
end;

{ TXDCPinFormatter }

function TXDCPinFormatter.Format(pin, net: string; ind: Integer): string;
begin
  Result := 'set_property PACKAGE_PIN ' + pin + #9' [get_ports {' + net + FormatPinInd(ind) + '}]';
end;

{ TXDCFormat }

function TXDCFormat.GetFileExt: string;
begin
    result := 'xdc';
end;

function TXDCFormat.GetIconResurceName: string;
begin
  result := 'XDCICO';
end;

function TXDCFormat.GetFormatter: TBaseDocFormatter;
begin
  result := TCommonDocFormatter.Create(
      TCommonLineGroupFormatter.Create(
        '#', '; '#9'#',
        TXDCPinFormatter.Create()));
end;

function TXDCFormat.GetHighlighter: TSynCustomHighlighter;
var
  h: TSynHinhlightUCF;
begin
  h := TSynHinhlightUCF.Create(nil);
  h.SetKeywords('set_property,PACKAGE_PIN,get_ports');
  h.CommentLine := '#';
  result := h;
end;

function TXDCFormat.GetName: string;
begin
  result := 'Xilinx Vivado XDC';
end;

{ TLPFPinFormatter }

function TLPFPinFormatter.Format(pin, net: string; ind: Integer): string;
begin
  Result := 'LOCATE COMP "' + net + FormatPinInd(ind) + '"' + #9' SITE "' + pin + '"';
end;

{ TLPFFormat }

function TLPFFormat.GetFileExt: string;
begin
  Result := 'lpf';
end;

function TLPFFormat.GetFormatter: TBaseDocFormatter;
begin
  result := TCommonDocFormatter.Create(
      TCommonLineGroupFormatter.Create(
        '#', '; '#9'#',
        TLPFPinFormatter.Create()));
end;

function TLPFFormat.GetHighlighter: TSynCustomHighlighter;
var
  h: TSynHinhlightUCF;
begin
  h := TSynHinhlightUCF.Create(nil);
  h.SetKeywords('LOCATE,COMP,SITE');
  h.CommentLine := '#';
  result := h;
end;

function TLPFFormat.GetIconResurceName: string;
begin
  result := 'XLPFICO';
end;

function TLPFFormat.GetName: string;
begin
  result := 'Lattice Diamond LPF';
end;

{ TQSFPinFormatter }

function TQSFPinFormatter.Format(pin, net: string; ind: Integer): string;
begin
  Result := 'set_location_assignment PIN_' + pin + #9' -to ' + net + FormatPinInd(ind);
end;

{ TQSFFormat }

function TQSFFormat.GetFileExt: string;
begin
  Result := 'qsf';
end;

function TQSFFormat.GetFormatter: TBaseDocFormatter;
begin
  result := TCommonDocFormatter.Create(
      TCommonLineGroupFormatter.Create(
        '#', '; '#9'#',
        TQSFPinFormatter.Create()));
end;

function TQSFFormat.GetHighlighter: TSynCustomHighlighter;
var
  h: TSynHinhlightUCF;
begin
  h := TSynHinhlightUCF.Create(nil);
  h.SetKeywords('set_location_assignment,to');
  h.CommentLine := '#';
  result := h;
end;

function TQSFFormat.GetIconResurceName: string;
begin
  result := 'QSFICO';
end;

function TQSFFormat.GetName: string;
begin
  result := 'Intel Quartus QSF';
end;

{ HDLDocFormatter }

constructor THDLDocFormatter.Create(Comment: string; RowFormatter: THDLHelperDocFormatter);
begin
  FComment := Comment;
  FRowFormatter := RowFormatter;
end;

destructor THDLDocFormatter.Destroy;
begin
  FRowFormatter.Free;
  inherited;
end;

procedure THDLDocFormatter.Format(doc: TPinDoc; res: TStrings);
  procedure FormatGroup(g: TGroup; gname: string; res: TStrings; opt: TFormatCommentSet; last: Boolean);
  var
    cntpin: Integer;
    tab: TGroupTabulator;
    enumerator: TPortEnumeratorAdapter;
  begin
    if not g.Visible then
      exit;

    cntpin := g.Pins.Count;

    if cntpin = 0 then
      exit;

    tab := TGroupTabulator.Create();

    if fcGroup in opt then
    begin
      tab.Add(FComment + g.Name, g.PinDataPlug);
    end;

    enumerator := g.GetPortEnumerator();

    while enumerator.Next do
    begin
      if enumerator.HIndex = -1 then
        tab.Add(FRowFormatter.FormPort(enumerator.Net, (last and (not enumerator.NotLast))), g.PinDataPlug)
      else
        tab.Add(FRowFormatter.FormBus(enumerator.Net, enumerator.HIndex, enumerator.LIndex, (last and (not enumerator.NotLast))), g.PinDataPlug);
    end;

    enumerator.Free;

    tab.MoveTo(res);
    tab.Free;
  end;
var
  group: TGroup;
  c: Integer;
  i: Integer;
  gl: TGroupList;
begin
  if Assigned(doc.CurrentComp) then
  begin

    FRowFormatter.Header(doc, res);

    gl := TGroupList.Create(False);
    for group in doc.Groups do
      if group.Visible then
        gl.Add(group);

    if doc.NoGroup.Pins.Count > 0 then
      gl.Add(doc.NoGroup);

    if doc.Options.VisibleFiltered then
      gl.Add(doc.FilteredGroup);

    i := 1;
    c := gl.Count;
    for group in gl do
    begin
      FormatGroup(group, group.Name, res, doc.Options.FormatComment, (i = c));
      Inc(i);
    end;

    FRowFormatter.Fother(doc, res);
    res.Add('');
  end;
end;

{ TVerilogFormat }

function TVerilogFormat.GetFileExt: string;
begin
  Result := 'v';
end;

function TVerilogFormat.GetFormatter: TBaseDocFormatter;
begin
  Result := THDLDocFormatter.Create('    //', TVerilogHelperDocFormatter.Create());
end;

function TVerilogFormat.GetHighlighter: TSynCustomHighlighter;
var
  h: TSynHinhlightUCF;
begin
  h := TSynHinhlightUCF.Create(nil);
  h.SetKeywords('module,endmodule,input,output,inout');
  h.CommentLine := '//';
  h.StrBegin := '[';
  h.StrEnd := ']';
  result := h;
end;

function TVerilogFormat.GetIconResurceName: string;
begin
    Result := 'VICO';
end;

function TVerilogFormat.GetName: string;
begin
    Result := 'Verilog';
end;

{ TVerilogHelperDocFormatter }

function TVerilogHelperDocFormatter.FormBus(name: string; h: Integer; l: Integer; last: Boolean): string;
var
  s: string;
begin
  if last then
    s := ''
  else
    s := ',';

  result := '    inout [' + IntToStr(h) + ': ' + IntToStr(l) + '] ' + name + s;
end;

function TVerilogHelperDocFormatter.FormPort(name: string;
  last: Boolean): string;
var
  s: string;
begin
  if last then
    s := ''
  else
    s := ',';

  result := '    inout ' + name + s;
end;

procedure TVerilogHelperDocFormatter.Fother(doc: TPinDoc; res: TStrings);
begin
    res.Add(');');
    res.Add('');
    res.Add('');
    res.Add('endmodule');
end;

procedure TVerilogHelperDocFormatter.Header(doc: TPinDoc; res: TStrings);
begin
    res.Add( 'module ' + fltrstr(doc.GetFileComp()) + '(');
    res.Add('');
end;

{ TVHDLFormat }

function TVHDLFormat.GetFileExt: string;
begin
  Result := 'vhd';
end;

function TVHDLFormat.GetFormatter: TBaseDocFormatter;
begin
    Result := THDLDocFormatter.Create('        --', TVHDLHelperDocFormatter.Create());
end;

function TVHDLFormat.GetHighlighter: TSynCustomHighlighter;
var
  h: TSynHinhlightUCF;
begin
  h := TSynHinhlightUCF.Create(nil);
  h.SetKeywords('entity,end,in,out,inout,std_logic_vector,std_logic,port,is');
  h.CommentLine := '--';
  h.StrBegin := '(';
  h.StrEnd := ')';
  result := h;
end;

function TVHDLFormat.GetIconResurceName: string;
begin
  Result := 'VHDICO';
end;

function TVHDLFormat.GetName: string;
begin
  Result := 'VHDL';
end;

{ TVHDLHelperDocFormatter }

function TVHDLHelperDocFormatter.FormBus(name: string; h: Integer; l: Integer; last: Boolean): string;
var
  s: string;

  a: Integer;
  b: Integer;
begin
  if last then
    s := ''
  else
    s := ';';

  if h > l then
  begin
    a := h;
    b := l;
  end else begin
    a := l;
    b := h;
  end;

  result := '        ' + name + ' : inout std_logic_vector(' + IntToStr(a) + ' downto ' + IntToStr(b) + ')' + s;
end;

function TVHDLHelperDocFormatter.FormPort(name: string; last: Boolean): string;
var
  s: string;
begin
  if last then
    s := ''
  else
    s := ';';
  result := '        ' + name + #9' : inout std_logic' + s;
end;

procedure TVHDLHelperDocFormatter.Fother(doc: TPinDoc; res: TStrings);
begin
  res.Add('    );');
  res.Add('end ' + FEntityName + ';');
end;

procedure TVHDLHelperDocFormatter.Header(doc: TPinDoc; res: TStrings);
begin
  FEntityName := fltrstr(fltrstr(doc.GetFileComp()));

  res.Add( 'entity ' + FEntityName + ' is');
  res.Add('    port(');
end;

{ TGroupTabulator }

procedure TGroupTabulator.Add(s: string; obj: TObject);
var
  p1: Integer;
  p2: Integer;
  ln: Integer;
  ilen: Integer;
begin
  sl.AddObject(s, obj);

  ilen := 0;
  p1 := 1;
  p2 := 1;
  repeat
    p1 := PosEx(#9, s, p1);

    if p1 = 0 then
      break;

    Inc(p1);
    ln := p1 - p2;

    if alen[ilen] < ln then
      alen[ilen] := ln;

    p2 := p1;
    Inc(ilen);
  until False;
end;

procedure TGroupTabulator.Clean;
var
  i: Integer;
begin
  sl.Clear;
  for i := 0 to Length(alen) - 1 do
    alen[i] := 0;
end;

constructor TGroupTabulator.Create(len: Integer);
var
  i: Integer;
begin
    sl := TStringList.Create;
    SetLength(alen, len);
    for i := 0 to len do
      alen[i] := 0;
end;

destructor TGroupTabulator.Destroy;
begin
  sl.Free;
  inherited;
end;

procedure TGroupTabulator.MoveTo(dst: TStrings);
var
  i: Integer;
  ilen: Integer;
  s: string;
  sb: TStringBuilder;
  p1: Integer;
  p2: Integer;
  ln: Integer;
begin
  sb := TStringBuilder.Create(Length(alen)*2+1);
  for i := 0 to sl.Count - 1 do
  begin
    s := sl[i];
    ilen := 0;
    p2 := 1;
    repeat
      p1 := PosEx(#9, s, p2);

      if p1 = 0 then
        break;

      ln := p1 - p2;

      sb.Append(s, p2 - 1, ln);
      sb.Append(StringOfChar(' ', alen[ilen] - ln - 1));

      p2 := p1 + 1;
      Inc(ilen);
    until False;

    sb.Append(s, p2 - 1, Length(s) - p2 + 1);


    dst.AddObject(sb.ToString, sl.Objects[i]);
    sb.Clear;
  end;
  sb.Free;
end;

procedure TGroupTabulator.SetSeparator(Separator: string);
begin
  FSeparator := Separator;
end;

initialization
  FormatReestr := TFormatReestr.Create();
  FormatReestr.Add(TCSVFormat.Create());
  FormatReestr.Add(TUCFFormat.Create());
  FormatReestr.Add(TXDCFormat.Create());
  FormatReestr.Add(TLPFFormat.Create());
  FormatReestr.Add(TQSFFormat.Create());
  FormatReestr.Add(TVerilogFormat.Create());
  FormatReestr.Add(TVHDLFormat.Create());

finalization
  FormatReestr.Free;

end.
