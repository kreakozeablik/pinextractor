unit UdmCommonData;

interface

uses
  System.SysUtils, System.Classes, System.ImageList, Vcl.ImgList, Vcl.Controls,
  Vcl.Dialogs;

type
  TdmCommonData = class(TDataModule)
    Images16: TImageList;
    Images32: TImageList;
    OpenDialog: TOpenDialog;
    ImagesFormat: TImageList;
  private
    { Private declarations }
  public

  end;

var
  dmCommonData: TdmCommonData;

implementation

uses TBXOfficeXPTheme;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
