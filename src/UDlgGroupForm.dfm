object DlgGroupForm: TDlgGroupForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1075#1088#1091#1087#1087#1099
  ClientHeight = 319
  ClientWidth = 387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 278
    Width = 387
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      387
      41)
    object Button1: TButton
      Left = 305
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object Button2: TButton
      Left = 227
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 387
    Height = 93
    Align = alTop
    TabOrder = 0
    DesignSize = (
      387
      93)
    object Label1: TLabel
      Left = 106
      Top = 20
      Width = 42
      Height = 13
      Caption = #1060#1080#1083#1100#1090#1088':'
      FocusControl = edtFiltr
    end
    object Label2: TLabel
      Left = 14
      Top = 72
      Width = 34
      Height = 13
      Caption = #1064#1080#1085#1099':'
    end
    object edtFiltr: TEdit
      Left = 154
      Top = 17
      Width = 148
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object cbVisible: TCheckBox
      Left = 100
      Top = 44
      Width = 67
      Height = 17
      Alignment = taLeftJustify
      Caption = #1042#1080#1076#1080#1084#1099#1081
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object trBuses: TVirtualStringTree
    Left = 0
    Top = 115
    Width = 387
    Height = 163
    Align = alClient
    Colors.BorderColor = 15987699
    Colors.DisabledColor = clGray
    Colors.DropMarkColor = 15385233
    Colors.DropTargetColor = 15385233
    Colors.DropTargetBorderColor = 15385233
    Colors.FocusedSelectionColor = 15385233
    Colors.FocusedSelectionBorderColor = 15385233
    Colors.GridLineColor = clSilver
    Colors.HeaderHotColor = clBlack
    Colors.HotColor = clBlack
    Colors.SelectionRectangleBlendColor = 15385233
    Colors.SelectionRectangleBorderColor = 15385233
    Colors.SelectionTextColor = clBlack
    Colors.TreeLineColor = 9471874
    Colors.UnfocusedColor = clGray
    Colors.UnfocusedSelectionColor = 13421772
    Colors.UnfocusedSelectionBorderColor = 13421772
    DragOperations = [doMove]
    DragType = dtVCL
    DrawSelectionMode = smBlendedRectangle
    Header.AutoSizeIndex = 0
    Header.Options = [hoAutoResize, hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible]
    PopupMenu = PopupMenu
    TabOrder = 2
    TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScrollOnExpand, toAutoSort, toAutoTristateTracking, toAutoChangeScale]
    TreeOptions.MiscOptions = [toEditable, toGridExtensions, toFullRowDrag, toEditOnClick, toEditOnDblClick]
    TreeOptions.PaintOptions = [toShowButtons, toShowHorzGridLines, toShowVertGridLines]
    TreeOptions.SelectionOptions = [toExtendedFocus, toFullRowSelect, toRightClickSelect]
    TreeOptions.StringOptions = [toAutoAcceptEditChange]
    OnColumnClick = trBusesColumnClick
    OnCreateEditor = trBusesCreateEditor
    OnDragAllowed = trBusesDragAllowed
    OnDragOver = trBusesDragOver
    OnDragDrop = trBusesDragDrop
    OnEditing = trBusesEditing
    OnExit = trBusesExit
    OnGetText = trBusesGetText
    OnNewText = trBusesNewText
    Columns = <
      item
        Position = 0
        Text = #1053#1072#1079#1074#1072#1085#1080#1077
        Width = 139
      end
      item
        Alignment = taRightJustify
        Position = 1
        Text = #1044#1083#1080#1085#1085#1072
      end
      item
        Alignment = taRightJustify
        Position = 2
        Text = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1080#1085#1076#1077#1082#1089
        Width = 111
      end
      item
        Position = 3
        Text = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077
        Width = 83
      end>
  end
  object TBXToolbar1: TTBXToolbar
    Left = 0
    Top = 93
    Width = 387
    Height = 22
    Align = alTop
    Caption = 'TBXToolbar1'
    Images = dmCommonData.Images16
    TabOrder = 3
    object TBXItem2: TTBXItem
      Action = actBusAdd
    end
    object TBXSeparatorItem1: TTBXSeparatorItem
    end
    object TBXItem1: TTBXItem
      Action = actDelBus
    end
    object TBXItem5: TTBXItem
      Action = actBusClear
    end
  end
  object PopupMenu: TTBXPopupMenu
    Images = dmCommonData.Images16
    Left = 46
    Top = 148
    object TBXItem4: TTBXItem
      Action = actBusAdd
    end
    object TBXSeparatorItem2: TTBXSeparatorItem
    end
    object TBXItem3: TTBXItem
      Action = actDelBus
    end
  end
  object ActionList: TActionList
    Images = dmCommonData.Images16
    Left = 118
    Top = 148
    object actBusAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1096#1080#1085#1091
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1096#1080#1085#1091
      ImageIndex = 16
      OnExecute = actBusAddExecute
    end
    object actDelBus: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1096#1080#1085#1091
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1096#1080#1085#1091
      ImageIndex = 17
      OnExecute = actDelBusExecute
      OnUpdate = actDelBusUpdate
    end
    object actBusClear: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077' '#1096#1080#1085#1099
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1077' '#1096#1080#1085#1099
      ImageIndex = 18
      OnExecute = actBusClearExecute
      OnUpdate = actBusClearUpdate
    end
  end
end
