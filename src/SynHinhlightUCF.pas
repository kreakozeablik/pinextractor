unit SynHinhlightUCF;

{$I SynEdit.inc}

interface

uses
  Graphics, SynEditTypes, SynEditHighlighter, SynUnicode, SysUtils, Classes;

type
  TtkuTokenKind = (tkuComment, tkuIdentifier, tkuKey, tkuNull, tkuSpace, tkuString, tkuUnknown);

type
  TSynHinhlightUCF = class(TSynCustomHighlighter)
  private
    fKeywords: TStringList;
    fCommentLine: string;
    fCommentLineLen: Integer;
    fStrBegin: Char;
    fStrEnd: Char;
    fTokenID: TtkuTokenKind;
    fCommentAttri: TSynHighlighterAttributes;
    fIdentifierAttri: TSynHighlighterAttributes;
    fKeyAttri: TSynHighlighterAttributes;
    fSpaceAttri: TSynHighlighterAttributes;
    fStringAttri: TSynHighlighterAttributes;
    procedure SetCommentLine(const Value: string);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
    function GetDefaultAttribute(Index: Integer): TSynHighlighterAttributes; override;
    function GetEol: Boolean; override;
    function GetTokenAttribute: TSynHighlighterAttributes; override;
    function GetTokenKind: Integer; override;
    procedure Next; override;

    procedure SetKeywords(kwrds: string);
  published
    property Keywords: TStringList read fKeywords;
    property CommentLine: string read fCommentLine write SetCommentLine;
    property StrBegin: Char read fStrBegin write fStrBegin;
    property StrEnd: Char read fStrEnd write fStrEnd;
    property CommentAttri: TSynHighlighterAttributes read fCommentAttri write fCommentAttri;
    property IdentifierAttri: TSynHighlighterAttributes read fIdentifierAttri write fIdentifierAttri;
    property KeyAttri: TSynHighlighterAttributes read fKeyAttri write fKeyAttri;
    property SpaceAttri: TSynHighlighterAttributes read fSpaceAttri write fSpaceAttri;
    property StringAttri: TSynHighlighterAttributes read fStringAttri write fStringAttri;
  end;

implementation

uses
  SynEditStrConst;

destructor TSynHinhlightUCF.Destroy;
begin
  fKeywords.Free;
  inherited;
end;

constructor TSynHinhlightUCF.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fCaseSensitive := False;

  fCommentAttri := TSynHighLighterAttributes.Create(SYNS_AttrComment, SYNS_FriendlyAttrComment);
//  fCommentAttri.Style := [fsUnderline];
  fCommentAttri.Foreground := clGreen;
  AddAttribute(fCommentAttri);

  fIdentifierAttri := TSynHighLighterAttributes.Create(SYNS_AttrCharacter, SYNS_FriendlyAttrCharacter);
  fIdentifierAttri.Style := [fsBold];
  fIdentifierAttri.Foreground := clBlue;
  AddAttribute(fIdentifierAttri);

  fKeyAttri := TSynHighLighterAttributes.Create(SYNS_AttrReservedWord, SYNS_FriendlyAttrReservedWord);
  fKeyAttri.Style := [fsItalic];
//  fKeyAttri.Foreground := clSkyBlue;
  AddAttribute(fKeyAttri);

  fSpaceAttri := TSynHighLighterAttributes.Create(SYNS_AttrSpace, SYNS_FriendlyAttrSpace);
  AddAttribute(fSpaceAttri);

  fStringAttri := TSynHighLighterAttributes.Create(SYNS_AttrString, SYNS_FriendlyAttrString);
  fStringAttri.Style := [fsBold];
  fStringAttri.Foreground := clBlue;
  AddAttribute(fStringAttri);

  SetAttributesOnChange(DefHighlightChange);

  fKeywords := TStringList.Create();
  CommentLine:= '#';
  fStrBegin := '"';
  fStrEnd := '"';
end;

procedure TSynHinhlightUCF.Next;

  function IsComment(): Boolean;
  var
    i: Integer;
    j: Integer;
  begin
    result := True;
    i := 1;
    j := Run;
    while Result and (i <= fCommentLineLen) and (not IsLineEnd(j)) do
    begin
      Result := (fLine[j] = fCommentLine[i]);
      Inc(i);
      Inc(j);
    end;
  end;
var
  c: Char;
  ident: string;
  scnt: Integer;
begin
  fTokenPos := Run;
  c := fLine[Run];

  if c = #0 then
  begin
    inc(Run);
    fTokenID := tkuNull;
  end
  else if CharInSet(c, [#1..#32]) then
  begin
    inc(Run);
    fTokenID := tkuSpace;
    while ((fLine[Run] <= #32) and not IsLineEnd(Run)) do
      inc(Run);
  end
  else if IsComment() then
  begin
    fTokenID := tkuComment;
    while not IsLineEnd(Run) do
      Inc(Run);
  end
  else if c = StrBegin then
  begin
    inc(Run);
    fTokenID := tkuString;
    while ((not IsLineEnd(Run)) and (fLine[Run] <> StrEnd)) do
      inc(Run);

    if not IsLineEnd(Run) then
      inc(Run);
  end
  else if CharInSet(c, ['A'..'Z', 'a'..'z', '_']) then
  begin
    inc(Run);
    scnt := 0;

    while ((CharInSet(fLine[Run], ['_', '0'..'9', 'a'..'z', 'A'..'Z', '[']) or (scnt > 0)) and not IsLineEnd(Run)) do
    begin
      if (fLine[Run] = '[') then
        Inc(scnt)
      else if (fLine[Run] = ']') then
        Dec(scnt);

      inc(Run);
    end;

    ident := Copy(fLine, fTokenPos + 1, Run - fTokenPos);
    if fKeywords.IndexOf(UpperCase(ident)) >= 0 then
      fTokenID := tkuKey
    else
      fTokenID := tkuIdentifier;
  end
  else begin
    inc(Run);
    fTokenID := tkuUnknown;
  end;

  inherited;
end;

function TSynHinhlightUCF.GetDefaultAttribute(Index: Integer): TSynHighLighterAttributes;
begin
  Result := nil;
end;

function TSynHinhlightUCF.GetEol: Boolean;
begin
  Result := Run = fLineLen + 1;
end;

function TSynHinhlightUCF.GetTokenAttribute: TSynHighLighterAttributes;
begin
  case fTokenId of
    tkuComment: Result := fCommentAttri;
    tkuIdentifier: Result := fIdentifierAttri;
    tkuKey: Result := fKeyAttri;
    tkuSpace: Result := fSpaceAttri;
    tkuString: Result := fStringAttri;
    tkuUnknown: Result := fKeyAttri;
  else
    Result := nil;
  end;
end;

function TSynHinhlightUCF.GetTokenKind: Integer;
begin
  Result := Ord(fTokenId);
end;

procedure TSynHinhlightUCF.SetCommentLine(const Value: string);
begin
  fCommentLine := Value;
  fCommentLineLen := Length(Value);
end;

procedure TSynHinhlightUCF.SetKeywords(kwrds: string);
begin
  fKeywords.DelimitedText := UpperCase(kwrds);
end;

end.
