unit UMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UMoveList;

type
  TFormTestMoveList = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    mlst: TMoveList;
  end;

var
  FormTestMoveList: TFormTestMoveList;

implementation

{$R *.dfm}

procedure TFormTestMoveList.FormCreate(Sender: TObject);
var
  a: char;
begin
  mlst := TMoveList.Create(self);
  mlst.Parent := self;
  mlst.Align := alClient;

  for a:= 'A' to 'Z' do
  begin
    mlst.items.add(StringofChar(a, 8));
  end;
end;

end.
