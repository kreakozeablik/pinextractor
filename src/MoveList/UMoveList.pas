unit UMoveList;

interface

uses StdCtrls, ExtCtrls, Controls, Classes, Messages, Types;

type
  TOnMove = procedure (Src, Dst: Integer) of object;

  TMoveList = class(TListBox)
  private
    FDraggedIndex: Integer;
    FDropIndex: Integer;
    FPrevIndex: Integer;
    FLastItemIncrement: Integer;
    FDragFlag: Boolean;
    FScrollAmt: Integer;
    FStartTopIndex: Integer;
    FOnMove: TOnMove;
    FTimerEnable: Boolean;
    FDblCliced: Boolean;
    procedure StartTimer(Interval: Integer);
    procedure StopTimer();
    procedure WmTimer(var Msg: TMessage); message WM_TIMER;
    procedure UpdateLastItemIncrement();

    procedure DragPanelSetPos(X, Y: Integer);
    procedure DragPanelHide();
  protected
    procedure DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState); override;
    procedure RedrawItems;

    procedure CancelDrag;

    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DblClick; override;

    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property OnMove: TOnMove read FOnMove write FOnMove;
  end;

implementation

uses SysUtils, Windows, Graphics, Math, Forms;

type
  TDragForm = class(TForm)
  private
    str: string;
    FParent: TMoveList;
  protected
    procedure Paint; override;
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer  = 0); override;
    procedure SetPos(p: TPoint);
    procedure DragShow(Parent: TMoveList; p: TPoint; s: string);
  end;

var
  DragForm: TDragForm;

{ TMoveList }

procedure TMoveList.CancelDrag;
begin
  if FDragFlag then
  begin
    StopTimer();
    DragPanelHide;
    ReleaseCapture();
    FDragFlag:=false;
    TopIndex := FStartTopIndex;
    RedrawItems;
  end;
end;

constructor TMoveList.Create(AOwner: TComponent);
begin
  inherited;
  FDblCliced := False;
  DoubleBuffered:=true;
  FTimerEnable := False;
  Style := lbOwnerDrawVariable;
  StyleElements := [];

  if not Assigned(DragForm) then
    DragForm := TDragForm.CreateNew(Application);
end;

procedure TMoveList.DblClick;
begin
  inherited;
  FDblCliced := True;
end;

procedure TMoveList.DragPanelHide;
begin
  DragForm.Hide;
end;

procedure TMoveList.DragPanelSetPos(X, Y: Integer);
begin
  DragForm.SetPos(ClientToScreen(Point(X, Y)));
end;

procedure TMoveList.DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  inherited;

  if FDragFlag and (Index = FDropIndex) and
      (FDraggedIndex <> FDropIndex) and not FTimerEnable then
  begin
    Canvas.Pen.Width := 1;
    Canvas.Pen.Color := clBlack;

    if Index > FDraggedIndex then
    begin
      Canvas.MoveTo(Rect.Left, Rect.Bottom - 1);
      Canvas.LineTo(Rect.Right, Rect.Bottom - 1);
    end else begin
      Canvas.moveto(Rect.Left, Rect.Top + 1);
      Canvas.lineto(Rect.Right, Rect.Top + 1);
    end;
  end;
end;

procedure TMoveList.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then
    CancelDrag;
  inherited;
end;

procedure TMoveList.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;

  if mbLeft = Button then
  begin
    if FDblCliced then
      FDblCliced := False
    else
    begin
      FDraggedIndex := ItemAtPos(Point(0, Y), True);
      if (FDraggedIndex > -1) then
      begin
        FStartTopIndex := TopIndex;
        ItemIndex := -1;
        FDropIndex := -1;
        FDragFlag := true;
        SetCaptureControl(self);
        StopTimer();
      end;
    end;
  end;
end;

procedure TMoveList.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited;

  if FDragFlag then
  begin
    if (Y >= 0) and (Y <= Height) and FTimerEnable then
      StopTimer();

    if not FTimerEnable then
    begin

      if (Y <= 0) or (Y >= Height) then
      begin 
        if (Y > Height) and (FDropIndex < Items.Count-1) then
        begin
          FScrollAmt := 1;
          StartTimer(300);
        end
        else
        if (Y < 0) and (TopIndex > 0)then
        begin
          FScrollAmt := -1;
          StartTimer(300);
        end;
      end

      else
      begin
        FDropIndex := ItemAtPos(Point(0, Y), True);
        UpdateLastItemIncrement;
        if FDropIndex < 0 then
          FDropIndex := TopIndex + FLastItemIncrement;
        RedrawItems;
      end;
    end;

    if ((not DragForm.Visible) and (FDraggedIndex <> FDropIndex)) then
      DragForm.DragShow(self, ClientToScreen(Point(X, Y)), Items[FDraggedIndex])
    else
      DragPanelSetPos(X, Y);
  end;
end;

procedure TMoveList.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Itemindex := ItemAtPos(Point(0, Y), False);;

  if FDragFlag then
  begin
    StopTimer;
    DragPanelHide;

    if (FDropIndex >= 0) then
    begin
      FDropIndex := Min(FDropIndex, Items.Count - 1);
      if (FDraggedIndex >= 0) and (FDraggedIndex <> FDropIndex)  then
      begin
        if Assigned(FOnMove) then
          FOnMove(FDraggedIndex, FDropIndex)
        else
          Items.move(FDraggedIndex, FDropIndex);
      end;
      Itemindex := FDropIndex;
      Invalidate();
    end;

    ReleaseCapture(); 
    FDragFlag:=false;
    FDropIndex := -1;
    FDraggedIndex := -1;

    RedrawItems;
  end;

  inherited;
end;

procedure TMoveList.RedrawItems;
begin
    DrawItem(FPrevIndex, ItemRect(FPrevIndex), []);
    if (FDropIndex <> FPrevIndex) then
    begin
      DrawItem(FDropIndex, ItemRect(FDropIndex), []);
      FPrevIndex := FDropIndex;
    end;
end;

procedure TMoveList.StartTimer(Interval: Integer);
begin
  KillTimer(Handle, 1);
  if SetTimer(Handle, 1, Interval, nil) = 0 then
    raise EOutOfResources.Create('Can''t create timer.');
  FTimerEnable := True;
end;

procedure TMoveList.StopTimer;
begin
  KillTimer(Handle, 1);
  FTimerEnable := False;
end;

procedure TMoveList.UpdateLastItemIncrement;
begin
  FLastItemIncrement := Height div ItemHeight - 1;
end;

procedure TMoveList.WMKillFocus(var Message: TWMKillFocus);
begin
  inherited;
  CancelDrag;
end;

procedure TMoveList.WmTimer(var Msg: TMessage);
var
  NewIndex: Integer;
begin
  NewIndex := TopIndex + FScrollAmt;

  if (NewIndex >= 0) and (NewIndex < Items.count)
  then
  begin
    FPrevIndex := FDropIndex;

    if (FDropIndex = 0) or (FDropIndex = Items.Count-1) or
        (ItemAtPos(ScreenToClient(Mouse.CursorPos), true) >= 0)
    then
      StopTimer()
    else
    begin
      UpdateLastItemIncrement;
      if FScrollAmt > 0 then
        FDropIndex := NewIndex + FLastItemIncrement
      else
        FDropIndex := NewIndex;
    end;

    TopIndex := NewIndex;

    RedrawItems();
  end;

  StartTimer(150);
end;

{ TDragForm }

constructor TDragForm.CreateNew(AOwner: TComponent; Dummy: Integer  = 0);
begin
  inherited;
  BorderStyle := bsNone;
  FormStyle := fsStayOnTop;
end;

procedure TDragForm.DragShow(Parent: TMoveList; p: TPoint; s: string);
begin
  FParent := Parent;
  str := s;

  SetWindowPos(Handle, HWND_TOP, p.X+15, p.Y+10, Canvas.TextWidth(s)+6, Canvas.TextHeight(s)+6, SWP_SHOWWINDOW or SWP_NOACTIVATE);
  Visible := True;
end;

procedure TDragForm.Paint;
begin
  inherited;
  Canvas.Font := Font;
  Canvas.Pen.Color := clBlack;
  Canvas.Rectangle(0, 0, Width, Height);
  Canvas.TextOut(3, 3, str);
end;

procedure TDragForm.SetPos(p: TPoint);
begin
  SetWindowPos(Handle, HWND_TOP, p.X+15, p.Y+10, 0, 0, SWP_NOACTIVATE or SWP_NOSIZE);
end;

end.
