unit PinDoc;

interface

uses System.Generics.Collections, Classes, Shema, System.SysUtils, JSON, JSONHelpers;

type
  TFormatComment = (fcNet, fcSheet, fcComponent, fcPinName, fcGroup);
  TFormatCommentSet = set of TFormatComment;

  TGroup = class;
  TMezanin = class;

  TMezaninConnection = class(TJSONSerializeble)
  private
    function GetValid: Boolean;

  public
    Owner: TMezanin;
    CompMainBoardName: string;
    CompMainBoard: TComp;
    CompMezaninBoardName: string;
    CompMezaninBoard: TComp;
    property Valid: Boolean read GetValid;

    procedure Assign(src: TMezaninConnection);

    constructor Create();

    function Save(): TJSONObject; override;
    procedure Load(data: TJSONObject); override;
  end;
  TMezaninConnectionList = class(System.Generics.Collections.TObjectList<TMezaninConnection>);

  TMezanin = class(TJSONSerializeble)
  private
    FNetPrefix: string;
  protected
    FConnections: TMezaninConnectionList;
    FShema: TShema;
    FFileName: string;
    FValid: Boolean;

    procedure UpdateShema();
  public
    constructor Create(); overload;
    constructor Create(src: TMezanin); overload;
    destructor Destroy(); override;

    procedure OpenShema(fname: string);

    procedure AssignShema(src: TShema);
    procedure SwapShema(src: TShema);

    function Save(): TJSONObject; override;
    procedure Load(data: TJSONObject); override;
    procedure SetMainShema(shm: TShema);

    property FileName: string read FFileName;
    property Shema: TShema read FShema;
    property Connections: TMezaninConnectionList read FConnections;
    property NetPrefix: string read FNetPrefix write FNetPrefix;
    property Valid: Boolean read FValid;
  end;
  TMezaninList = class(System.Generics.Collections.TObjectList<TMezanin>);

  TMezaninData = class(TObject)
  private
    FMezaninConnection: TMezaninConnection;
    FMezaninPin: TPin;
  public
    property MezaninConnection: TMezaninConnection read FMezaninConnection;
    property MezaninPin: TPin read FMezaninPin;
    constructor Create(xpin: TPin; mcon: TMezaninConnection);
  end;
  TMezaninDataList = class(System.Generics.Collections.TObjectList<TMezaninData>);

  TPinData = class(TObject)
  private
    function GetDesignator: string;
    function GetName: string;
    function GetNetNameOriginal: string;
    function GetPart: string;
    function GetSheet: string;
  protected
    FPin: TPin;
    FMezaninData: TMezaninDataList;

  public
    NetName: string;
    Group: TGroup;

    property MezaninData: TMezaninDataList read FMezaninData;
    property Pin: TPin read FPin;
    property NetNameOriginal: string read GetNetNameOriginal;
    property Name: string read GetName;
    property Designator: string read GetDesignator;
    property Sheet: string read GetSheet;
    property Part: string read GetPart;

    constructor Create(vPin: TPin);
    destructor Destroy(); override;
  end;

  TPinDataList = class(System.Generics.Collections.TObjectList<TPinData>);

  TBus = class(TJSONSerializeble)
  private
    FName: string;
    FStartInd: Integer;
    FReverce: Boolean;
    FCount: Integer;
  public
    property Name: string read FName write FName;
    property Reverce: Boolean read FReverce write FReverce;
    property StartInd: Integer read FStartInd write FStartInd;
    property Count: Integer read FCount write FCount;

    constructor Create(); overload;
    constructor Create(src: TBus); overload;
    constructor Create(Name: string); overload;

    function Save(): TJSONObject; override;
    procedure Load(data: TJSONObject); override;
  end;

  TBusList = class(System.Generics.Collections.TObjectList<TBus>);

  TPinEnumerator = class(TObject)
  private
    FNet: string;
    FIndex: integer;
    FPin: TPinData;

    FGroup: TGroup;

    busi: Integer;
    buspini: Integer;
    pini: Integer;
    busstarti: Integer;
  public
    constructor Create(Grout: TGroup);
    function Next(): Boolean;

    property Net: string read FNet;
    property Index: Integer read FIndex;
    property Pin: TPinData read FPin;
  end;

  TPortEnumerator = class(TObject)
  private
    FNet: string;
    FHIndex: integer;
    FLIndex: integer;

    FGroup: TGroup;

    busi: Integer;
    pini: Integer;
    buspini: Integer;
  public
    constructor Create(Grout: TGroup);
    function Next(): Boolean;

    property Net: string read FNet;
    property HIndex: Integer read FHIndex;
    property LIndex: Integer read FLIndex;
  end;

  TPortEnumeratorAdapter = class(TObject)
  private
    FNet: string;
    FHIndex: integer;
    FLIndex: integer;
    FNotLast: Boolean;

    enumerator: TPortEnumerator;
  public
    constructor Create(Grout: TGroup);
    function Next(): Boolean;

    property NotLast: Boolean read FNotLast;
    property Net: string read FNet;
    property HIndex: Integer read FHIndex;
    property LIndex: Integer read FLIndex;
  end;

  TGroup = class(TJSONSerializeble)
  private
    FPins: TPinDataList;
    FPinDataPlug: TPinData;
    FVisible: Boolean;
    FFiltr: string;
    FBuses: TBusList;

    function GetFiltr: string;
    procedure SetFiltr(const Value: string);
    function GetName: string;
  public
    function GetPinEnumerator(): TPinEnumerator;
    function GetPortEnumerator(): TPortEnumeratorAdapter;

    function Accept(pin: TPinData): Boolean;

    procedure Add(pin: TPinData);
    procedure Clear();

    property PinDataPlug: TPinData read FPinDataPlug;
    property Name: string read GetName;
    property Filtr: string read GetFiltr write SetFiltr;
    property Visible: Boolean read FVisible write FVisible;

    property Buses: TBusList read FBuses;

    property Pins: TPinDataList read FPins;

    constructor Create();
    destructor Destroy(); override;

    function Save(): TJSONObject; override;
    procedure Load(data: TJSONObject); override;
  end;

  TGroupList = class(System.Generics.Collections.TObjectList<TGroup>);

  TReplaceSubStrData = class(TJSONSerializeble)
  public
    Condition: string;
    SubStr: string;
    Repl: string;

    function Save(): TJSONObject; override;
    procedure Load(data: TJSONObject); override;
  end;

  TReplaceSubStrDataList = class(System.Generics.Collections.TObjectList<TReplaceSubStrData>);

  TPinDocOnUpdate = procedure () of object;

  TPinDocOptions = class(TJSONSerializeble)
  private
    FReplaceSubStrDatas: TReplaceSubStrDataList;
    FPinNameFilters: TStringList;
    FFormatComment: TFormatCommentSet;
    FVisibleFiltered: Boolean;
  public
    function Save(): TJSONObject; override;
    procedure Load(data: TJSONObject); override;

    constructor Create();
    destructor Destroy(); override;

    property FormatComment: TFormatCommentSet read FFormatComment write FFormatComment;
    property VisibleFiltered: Boolean read FVisibleFiltered write FVisibleFiltered;
    property ReplaceSubStrDatas: TReplaceSubStrDataList read FReplaceSubStrDatas;
    property PinNameFilters: TStringList read FPinNameFilters;
  end;

  TPinDoc = class(TObject)
  private
    FOptions: TPinDocOptions;
    FGroups: TGroupList;
    FNoGroup: TGroup;
    FFilteredGroup: TGroup;
    FShema: TShema;
    FPinDocOnUpdate: TPinDocOnUpdate;
    FPinDocGroupsOnUpdate: TPinDocOnUpdate;
    FFormat: Integer;
    FPinDocFormatUpdate: TPinDocOnUpdate;
    FGroupUpdate: Boolean;
    FCurrentComp: TComp;
    FMezanins: TMezaninList;

    procedure GroupsNotify(sender: TObject; const  item: TGroup; action: System.Generics.Collections.TCollectionNotification);
    procedure SetFormat(const Value: Integer);
    procedure SetCurrentComp(const Value: TComp);
  protected
    procedure RstFormat();
  public
    property CurrentComp: TComp read FCurrentComp write SetCurrentComp;
    function GetFileComp(): string;
    function Select(cname: string): Boolean;

    property Format: Integer read FFormat write SetFormat;
    property Options: TPinDocOptions read FOptions;

    procedure AssignShema(src: TShema);
    procedure SwapShema(src: TShema);
    procedure SetMezanins(src: TMezaninList);
    procedure UpdateGroupsPins();

    function CompSelected: Boolean;

    property Shema: TShema read FShema;
    property Groups: TGroupList read FGroups;
    property NoGroup: TGroup read FNoGroup;
    property FilteredGroup: TGroup read FFilteredGroup;
    property Mezanins: TMezaninList read FMezanins;

    property PinDocOnUpdate: TPinDocOnUpdate read FPinDocOnUpdate write FPinDocOnUpdate;
    property PinDocGroupsOnUpdate: TPinDocOnUpdate read FPinDocGroupsOnUpdate write FPinDocGroupsOnUpdate;
    property PinDocFormatUpdate: TPinDocOnUpdate read FPinDocFormatUpdate write FPinDocFormatUpdate;

    constructor Create();
    destructor Destroy(); override;

    procedure Save(fn: string);
    procedure Load(fn: string; const LoadSch: Boolean = True);

    procedure AutoGroup();
    procedure CleanGroups();

    procedure BeginGroupUpdate();
    procedure EndGroupUpdate();

    function ValidMezanins(): Boolean;
  end;

  procedure CopyMezanins(src, dest: TMezaninList);
  procedure CopyBuses(src, dest: TBusList);

implementation

uses strutil, Dialogs, System.UITypes, UConstraintsFormatter, System.IOUtils, Math, System.Generics.Defaults;

procedure CopyMezanins(src, dest: TMezaninList);
var
  m: TMezanin;
begin
  for m in src do
    dest.Add(TMezanin.Create(m));
end;

procedure CopyBuses(src, dest: TBusList);
var
  m: TBus;
begin
  dest.Clear;
  for m in src do
    dest.Add(TBus.Create(m));
end;

{ TGroup }

function TGroup.Accept(pin: TPinData): Boolean;
begin
  result := reg_exp(pin.NetName, FFiltr);
end;

procedure TGroup.Add(pin: TPinData);
begin
  FPins.Add(pin);
end;

procedure TGroup.Clear;
begin
  FPins.Clear;
end;

constructor TGroup.Create;
begin
  FPinDataPlug := TPinData.Create(nil);
  FPinDataPlug.Group := self;
  FPins := TPinDataList.Create(True);
  FBuses := TBusList.Create(True);
end;

destructor TGroup.Destroy;
begin
  FBuses.Free;
  FPinDataPlug.Free;
  FPins.Free;
  inherited;
end;

function TGroup.GetFiltr: string;
begin
  Result := FFiltr;
end;

function TGroup.GetName: string;
begin
  Result := Filtr;
end;

function TGroup.GetPinEnumerator: TPinEnumerator;
begin
  Result := TPinEnumerator.Create(self);
end;

function TGroup.GetPortEnumerator: TPortEnumeratorAdapter;
begin
  Result := TPortEnumeratorAdapter.Create(self);
end;

procedure TGroup.Load(data: TJSONObject);
begin
  FFiltr := data.hReadStr('filtr', '');
  FVisible := data.hReadBool('visible', False);
  data.hRead<TBus>('Buses', FBuses);
end;

function TGroup.Save: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.hWrite('filtr', FFiltr);
  Result.hWrite('visible', FVisible);
  Result.hWrite<TBus>('Buses', FBuses);
end;

procedure TGroup.SetFiltr(const Value: string);
begin
  FFiltr := Value;
end;

{ TPinDoc }

procedure TPinDoc.AssignShema(src: TShema);
begin
  FMezanins.Clear();
  FShema.Assign(src);
  UpdateGroupsPins;
end;

procedure TPinDoc.AutoGroup;

  function GetGrp(net: string ; var grp: string): Boolean;
  var
    p: Integer;
  begin
    Result := False;
    p := Pos('_', net);
    if p > 0 then
    begin
      grp := Copy(net, 1, p) + '*';
      Result := True;
    end;
  end;

  function GetGrp2(net: string ; var grp: string): Boolean;
  var
    p: Integer;
  begin
    Result := False;
    p := Pos('_', net);
    if p > 0 then
    begin
      Inc(p);
      p := Pos('_', net, p);
      if p > 0 then
      begin
        grp := Copy(net, 1, p) + '*';
        Result := True;
      end;
    end;
  end;

  function IsBusGroups(rgx: string): Boolean;
  var
    pin: TPinData;
    cnt: Integer;
  begin
    Result := False;
    cnt := 0;
    for pin in FNoGroup.FPins do
      if reg_exp(pin.NetName, rgx) then
      begin
        Inc(cnt);
        if cnt > 1 then
        begin
          Result := True;
          exit;
        end;
      end;
  end;

  function IsBusGroups2(net: string; rgx: string): Boolean;
  var
    pin: TPinData;
    rgx2: string;
  begin
    Result := True;
    if  (not reg_exp(rgx, '\c\s#\s_\s')) and (GetGrp(net, rgx2) and (reg_exp(rgx2, '\c\s#\s_\*'))) then
      for pin in FNoGroup.FPins do
        if reg_exp(pin.NetName, rgx2) and (not reg_exp(pin.NetName, rgx)) and reg_exp(pin.NetName, '\c\s#\s_\s#*') then
        begin
          Result := False;
          exit;
        end;
  end;

  function GetGrpBus(net: string; var grp: string): Boolean;
  var
    s2: string;
    len: Integer;
    i: Integer;
    j: Integer;
  begin
    Result := False;
    len := Length(net);
    i := len;
    while i > 1 do
    begin
      if CharInSet(net[i], ['0'..'9']) then
      begin
        if i <> len then
          s2 := Copy(net, i + 1, len - i)
        else
          s2 := '';

        for j := i downto 1 do
          if not CharInSet(net[j], ['0'..'9']) then
          begin
            grp := Copy(net, 1, j) + '#' + s2;

            if IsBusGroups(grp) then
            begin
              Result := IsBusGroups2(net, grp);
              exit;
            end;
            break;
          end;

        i := j;
      end;

      Dec(i);
    end;
  end;

  procedure AddGrp(grp: string; isbus: Boolean = False);
  var
    g: TGroup;
    b: TBus;
  begin
    g := TGroup.Create;
    g.Visible := True;
    g.FFiltr := grp;
    if isbus then
    begin
      b := TBus.Create;
      b.Name := fltrstr(grp);
      g.Buses.Add(b);
    end;
    Groups.Add(g);
  end;

var
  i: Integer;
  maxi: Integer;
  c: Integer;
  grp: string;
  sgrp: string;
  pname: string;

  procedure SkipGrpdPins();
  var
    g: TGroup;
    f: Boolean;
  begin
    Inc(i);
    if Groups.Count = 0 then
      exit;
    f := True;
    while (i < maxi) and f do
    begin
      for g in Groups do
      begin
        f := g.Accept(FNoGroup.FPins[i]);
        if f then
        begin
          Inc(i);
          break;
        end;
      end;
    end;
  end;

begin
  if (FNoGroup.FPins.Count = 0) then
    exit();

  BeginGroupUpdate;

  maxi := FNoGroup.FPins.Count;
  i := 0;

  while (i < maxi) do
  begin
    pname := FNoGroup.FPins[i].NetName;


    if GetGrp(pname, grp) then
    begin
      c := 0;

      while i < maxi do
      begin
        pname := FNoGroup.FPins[i].NetName;

        if not reg_exp(pname, grp) then
          break
        else if GetGrpBus(pname, sgrp) then
          AddGrp(sgrp, True)
        else if GetGrp2(pname, sgrp) and IsBusGroups(sgrp) then
          AddGrp(sgrp, False)
        else
          Inc(c);

        SkipGrpdPins();
      end;

      if c > 0 then
        AddGrp(grp, False);
    end
    else if GetGrpBus(pname, grp) then
    begin
      AddGrp(grp, True);
      SkipGrpdPins();
    end
    else
      SkipGrpdPins();
  end;

  EndGroupUpdate;
end;

procedure TPinDoc.BeginGroupUpdate;
begin
  FGroupUpdate := True;
end;

procedure TPinDoc.CleanGroups;
begin
  BeginGroupUpdate();
  Groups.Clear();
  EndGroupUpdate();
end;

function TPinDoc.CompSelected: Boolean;
begin
  Result := Assigned(FCurrentComp);
end;

constructor TPinDoc.Create;
begin
  RstFormat;
  FOptions := TPinDocOptions.Create;

  FGroupUpdate := False;
  FPinDocOnUpdate := nil;
  FPinDocGroupsOnUpdate := nil;

  FNoGroup := TGroup.Create();
  FNoGroup.Filtr := 'No_grouped';
  FNoGroup.Visible := True;

  FFilteredGroup := TGroup.Create();
  FFilteredGroup.Filtr := 'Filtered';
  FFilteredGroup.Visible := True;

  FGroups := TGroupList.Create(True);
  FGroups.OnNotify := self.GroupsNotify;
  FShema := TShema.Create();

  FMezanins := TMezaninList.Create(True);
end;

destructor TPinDoc.Destroy;
begin
  FGroups.OnNotify := nil;
  FreeAndNil(FNoGroup);
  FreeAndNil(FGroups);
  FreeAndNil(FFilteredGroup);
  FreeAndNil(FShema);
  FreeAndNil(FMezanins);
  FreeAndNil(FOptions);
  inherited;
end;

procedure TPinDoc.EndGroupUpdate;
begin
  FGroupUpdate := False;
  if Assigned(FPinDocGroupsOnUpdate) then
    FPinDocGroupsOnUpdate();

  UpdateGroupsPins;
end;

function TPinDoc.GetFileComp: string;
begin
  Result := FShema.PrjName + '-';
  if Assigned(FCurrentComp) then
    Result := Result + FCurrentComp.Name;
end;

procedure TPinDoc.GroupsNotify(sender: TObject; const item: TGroup; action: System.Generics.Collections.TCollectionNotification);
begin
  if FGroupUpdate then
    exit;

  if Assigned(FPinDocGroupsOnUpdate) then
    FPinDocGroupsOnUpdate();

  UpdateGroupsPins;
end;

procedure TPinDoc.Load(fn: string; const LoadSch: Boolean);
  procedure SetFormatName(frmt: string);
  var
    i: Integer;
    f: TBaseFormat;
  begin
    RstFormat();
    if (frmt <> '') then
    begin
      i := 0;
      for f in FormatReestr do
      begin
        if f.GetFileExt = frmt then
        begin
          FFormat := i;
          break;
        end;
        Inc(i);
      end;
    end;
  end;
var
  json: TJSONObject;
  sfile: string;
  comp: string;
  m: TMezanin;
begin
  BeginGroupUpdate();

  try
    json := JSONFileRead(fn);

    FMezanins.Clear;

    if LoadSch then
    begin
      sfile := json.hReadStr('file');
      comp := json.hReadStr('comp');

      if not FileExists(sfile) then
      begin
        MessageDlg('���� "'+sfile+'" �� ������!', mtWarning, [mbOK], 0);
        exit;
      end;

      Shema.LoadFromFile(sfile);

      if not Select(comp) then
        MessageDlg('��������� "'+comp+'" �� ������!', mtWarning, [mbOK], 0);
    end;

    json.hRead<TPinDocOptions>('Options', FOptions);
    json.hRead<TGroup>('Groups', FGroups);
    json.hRead<TMezanin>('Mezanins', FMezanins);
    SetFormatName(json.hReadStr('Format'));

    for m in FMezanins do
      m.SetMainShema(FShema);

    json.Free;

  except
  end;

  EndGroupUpdate();

  if Assigned(FPinDocFormatUpdate) then
    FPinDocFormatUpdate();
end;

procedure TPinDoc.RstFormat;
begin
  FFormat := -1;
  if FormatReestr.Count > 0 then
    FFormat := 0;
end;

procedure TPinDoc.Save(fn: string);
  function GetFormatName: string;
  begin
    if FFormat >= 0 then
      Result := FormatReestr[FFormat].GetFileExt
    else
      Result := '';
  end;
var
  json: TJSONObject;
  sval: string;
begin
  try
    json := TJSONObject.Create;

    json.hWrite('file', Shema.FileName);

    if Assigned(FCurrentComp) then
      sval := FCurrentComp.Name
    else
      sval := '';
    json.hWrite('comp', sval);

    json.hWrite<TPinDocOptions>('Options', FOptions);
    json.hWrite<TGroup>('Groups', FGroups);
    json.hWrite<TMezanin>('Mezanins', FMezanins);
    json.hWrite('Format', GetFormatName);

    json.FileWrite(fn);
    json.Free;
  except
  end;
end;

function TPinDoc.Select(cname: string): Boolean;
var
  cmp: TComp;
begin
  for cmp in FShema do
    if cmp.Name = cname then
    begin
      CurrentComp := cmp;
      Result := True;
      exit;
    end;
  Result := False;
end;

procedure TPinDoc.SetCurrentComp(const Value: TComp);
begin
  FCurrentComp := Value;
  UpdateGroupsPins;
end;

procedure TPinDoc.SetFormat(const Value: Integer);
begin
  FFormat := Value;
  if Assigned(FPinDocFormatUpdate) then
    FPinDocFormatUpdate();
end;

procedure TPinDoc.SetMezanins(src: TMezaninList);
begin
  FMezanins := src;
  UpdateGroupsPins;
end;

procedure TPinDoc.SwapShema(src: TShema);
begin
  FMezanins.Clear();
  FShema.Swap(src);
end;

function CmpPinData(const p1: TPinData; const p2: TPinData): Integer;
begin
  Result := StringSortCompare(p1.NetName, p2.NetName);
end;

procedure TPinDoc.UpdateGroupsPins;
  function ApplyReplaseData(Net: string): string;
  var
    r: TReplaceSubStrData;
    c: string;
  begin
    Result := fltrstr(TranslitRus2Lat(Net));
    for r in FOptions.ReplaceSubStrDatas do
    begin
      c := r.Condition;
      if (c = '') or (reg_exp(Result, c)) then
        Result := StringReplace(Net, r.SubStr, r.Repl, [rfReplaceAll, rfIgnoreCase]);
    end;
    Result := fltrstr(TranslitRus2Lat(Result));
  end;

  function FiltrByPinName(pin: TPinData): Boolean;
  var
    frgx: string;
    s: string;
  begin
    Result := false;

    for frgx in FOptions.PinNameFilters do
    begin
      s := Trim(frgx);
      if length(s) = 0 then continue;
      if s[1] = '!' then
      begin
        Delete(s, 1, 1);
        if reg_exp(pin.Name, s) then
          Exit();
      end
      else if reg_exp(pin.Name, s) then
      begin
        Result := True;
        Exit();
      end;
    end;
  end;

  procedure MezaninConnection(pin: TPinData);
  var
    net_pin: TPin;
    mez: TMezanin;
    mcon: TMezaninConnection;
    main_pin: TPin;
    mez_pin: TPin;
  begin
    if Assigned(pin.FPin.Net) then
      for net_pin in pin.FPin.Net.Pins do
      begin
        for mez in Mezanins do
        begin
          for mcon in mez.Connections do
          begin
            if Assigned(mcon.CompMainBoard) and Assigned(mcon.CompMezaninBoard) then
              for main_pin in mcon.CompMainBoard.Pins do
                if main_pin.NetName = net_pin.NetName then
                begin
                  for mez_pin in mcon.CompMezaninBoard.Pins do
                  begin
                    if main_pin.Designator = mez_pin.Designator then
                    begin
                      pin.MezaninData.Add(TMezaninData.Create(mez_pin, mcon));

                      // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.................aa.....

                      if (mez_pin.NetName <> '') then
                        pin.NetName := ApplyReplaseData(mcon.Owner.NetPrefix + mez_pin.NetName);

                      //exit;
                    end;
                  end;
                end;
          end;
        end;
      end;
  end;

var
  g: TGroup;
  gi: TGroup;
  pin: TPin;
  pind: TPinData;
begin
  if FGroupUpdate then
    exit;

  FNoGroup.Pins.Clear;
  FFilteredGroup.Pins.Clear;
  for g in FGroups do
    g.Pins.Clear;

  if FCurrentComp <> nil then
    for pin in FCurrentComp.Pins do
    begin
      if (pin.NetName.IsEmpty) then
        continue;

      pind := TPinData.Create(pin);
      pind.NetName := ApplyReplaseData(pin.NetName);


      g := FFilteredGroup;
      if (not pind.NetName.IsEmpty) and FiltrByPinName(pind) then
      begin
        MezaninConnection(pind);

        g := FNoGroup;

        for gi in FGroups do
          if gi.Accept(pind) then
          begin
            g := gi;
            break;
          end;
      end;

      pind.Group := g;
      g.Add(pind);
    end;

  for gi in FGroups do
    gi.Pins.Sort(TComparer<TPinData>.Construct(CmpPinData));

  FNoGroup.Pins.Sort(TComparer<TPinData>.Construct(CmpPinData));
  FFilteredGroup.Pins.Sort(TComparer<TPinData>.Construct(CmpPinData));

  if Assigned(PinDocOnUpdate) then
    PinDocOnUpdate();
end;

function TPinDoc.ValidMezanins: Boolean;
var
  m: TMezanin;
  c: TMezaninConnection;
begin
  result := False;
  for m in Mezanins do
  begin
    if not m.Valid then
      exit;
    for c in m.Connections do
      if not c.Valid then
        exit;
  end;
  result := True;
end;

{ TReplaseSubStrData }

procedure TReplaceSubStrData.Load(data: TJSONObject);
begin
  if Assigned(data) then
  begin
    Condition := data.hReadStr('Condition');
    SubStr := data.hReadStr('SubStr');
    Repl := data.hReadStr('Repl');
  end else begin
    Condition := '';
    SubStr := '';
    Repl := '';
  end;
end;

function TReplaceSubStrData.Save: TJSONObject;
begin
  Result := TJSONObject.Create;

  Result.hWrite('Condition', Condition);
  Result.hWrite('SubStr', SubStr);
  Result.hWrite('Repl', Repl);
end;

{ TPinData }

constructor TPinData.Create(vPin: TPin);
begin
  FPin := vPin;
  FMezaninData := TMezaninDataList.Create(True);
end;

destructor TPinData.Destroy;
begin
  FMezaninData.Free;
  inherited;
end;

function TPinData.GetDesignator: string;
begin
  result := FPin.Designator;
end;

function TPinData.GetName: string;
begin
  result := FPin.Pin;
end;

function TPinData.GetNetNameOriginal: string;
begin
  result := FPin.Net.Name;
end;

function TPinData.GetPart: string;
begin
  result := FPin.Part;
end;

function TPinData.GetSheet: string;
begin
  result := FPin.Sheet;
end;

{ TMezanin }

constructor TMezanin.Create;
begin
  FConnections := TMezaninConnectionList.Create();
  FShema := TShema.Create();
  FValid := False;
  FFileName := '';
end;

procedure TMezanin.AssignShema(src: TShema);
begin
  FShema.Assign(src);
  UpdateShema();
end;

constructor TMezanin.Create(src: TMezanin);
var
  c: TMezaninConnection;
  n: TMezaninConnection;
begin
  FShema := TShema.Create();
  FShema.Assign(src.Shema);

  FFileName := src.FFileName;
  FNetPrefix := src.FNetPrefix;
  FValid := src.FValid;

  FConnections := TMezaninConnectionList.Create();
  for c in src.Connections do
  begin
    n := TMezaninConnection.Create();
    n.Owner := self;
    n.CompMainBoardName := c.CompMainBoardName;
    n.CompMezaninBoardName := c.CompMezaninBoardName;

    n.CompMainBoard := c.CompMainBoard;

    if FValid then
      n.CompMezaninBoard := FShema.GetComp(c.CompMezaninBoardName)
    else
      n.CompMezaninBoard := nil;

    FConnections.Add(n);
  end;
end;

destructor TMezanin.Destroy;
begin
  FConnections.Free;
  FShema.Free;
  inherited;
end;

procedure TMezanin.Load(data: TJSONObject);
var
  c: TMezaninConnection;
begin
  OpenShema(data.hReadStr('Shema'));
  FNetPrefix := data.hReadStr('Prefix');
  data.hRead<TMezaninConnection>('Connections', FConnections);

  for c in Connections do
  begin
    c.Owner := self;
    if FValid then
      c.CompMezaninBoard := FShema.GetComp(c.CompMezaninBoardName)
    else
      c.CompMezaninBoard := nil;
  end;
end;

procedure TMezanin.OpenShema(fname: string);
begin
  FValid := False;
  FFileName := fname;

  if FileExists(fname) then
  begin
    FShema.LoadFromFile(fname);
    UpdateShema();
  end;
end;

function TMezanin.Save: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.hWrite('Shema', FFileName);
  Result.hWrite('Prefix', FNetPrefix);
  Result.hWrite<TMezaninConnection>('Connections', FConnections);
end;

procedure TMezanin.SetMainShema(shm: TShema);
var
  c: TMezaninConnection;
begin
  for c in Connections do
    c.CompMainBoard := shm.GetComp(c.CompMainBoardName);
end;

procedure TMezanin.SwapShema(src: TShema);
begin
  FShema.Swap(src);
  UpdateShema();
end;

procedure TMezanin.UpdateShema();
var
  c: TMezaninConnection;
begin
  FValid := not FShema.FileName.IsEmpty;
  FFileName := FShema.FileName;

  if FValid then
    for c in Connections do
      c.CompMezaninBoard := FShema.GetComp(c.CompMezaninBoardName)
  else
    for c in Connections do
      c.CompMezaninBoard := nil;
end;

{ TMezaninData }

constructor TMezaninData.Create(xpin: TPin; mcon: TMezaninConnection);
begin
  FMezaninConnection := mcon;
  FMezaninPin := xpin;
end;

{ TMezaninConnection }

procedure TMezaninConnection.Assign(src: TMezaninConnection);
begin
  Owner := src.Owner;
  CompMainBoardName := src.CompMainBoardName;
  CompMainBoard := src.CompMainBoard;
  CompMezaninBoardName := src.CompMezaninBoardName;
  CompMezaninBoard := src.CompMezaninBoard;
end;

constructor TMezaninConnection.Create;
begin
  Owner := nil;
  CompMainBoard := nil;
  CompMezaninBoard := nil;
end;

function TMezaninConnection.GetValid: Boolean;
begin
  result := Assigned(CompMainBoard) and Assigned(CompMezaninBoard);
end;

procedure TMezaninConnection.Load(data: TJSONObject);
begin
  CompMainBoardName := data.hReadStr('MainBoard');
  CompMezaninBoardName := data.hReadStr('MezaninBoard');
end;

function TMezaninConnection.Save: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.AddPair('MainBoard', CompMainBoardName);
  Result.AddPair('MezaninBoard', CompMezaninBoardName);
end;

{ TBus }
constructor TBus.Create(src: TBus);
begin
  FName := src.FName;
  FStartInd := src.FStartInd;
  FReverce := src.FReverce;
  FCount := src.FCount;
end;

constructor TBus.Create;
begin
  FName := '';
  FStartInd := -1;
  FReverce := False;
  FCount := -1;
end;

constructor TBus.Create(Name: string);
begin
  FName := Name;
  FStartInd := -1;
  FReverce := False;
  FCount := -1;
end;

procedure TBus.Load(data: TJSONObject);
begin
    FName := data.hReadStr('Name');
    FStartInd := data.hReadInt('StartInd');
    FReverce := data.hReadBool('Reverce');
    FCount := data.hReadInt('Count');
end;

function TBus.Save: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.hWrite('Name', FName);
  Result.hWrite('StartInd', FStartInd);
  Result.hWrite('Reverce', FReverce);
  Result.hWrite('Count', FCount);
end;

{ TPinDocOptions }

constructor TPinDocOptions.Create;
begin
  FReplaceSubStrDatas := TReplaceSubStrDataList.Create(True);
  FPinNameFilters := TStringList.Create;
  FFormatComment := [];
  FVisibleFiltered := False;
end;

destructor TPinDocOptions.Destroy;
begin
  FReplaceSubStrDatas.Free;
  FPinNameFilters.Free;
  inherited;
end;


procedure TPinDocOptions.Load(data: TJSONObject);
begin
  FReplaceSubStrDatas.Clear;
  FPinNameFilters.Clear;
  FFormatComment := [];
  FVisibleFiltered := False;

  if Assigned(data) then
  begin
    data.hRead<TReplaceSubStrData>('ReplaceSubStr', FReplaceSubStrDatas);
    data.hRead('PinNameFilters', FPinNameFilters);

    FFormatComment := [];
    if data.hReadBool('fcNet') then
      FFormatComment := FFormatComment + [fcNet];
    if data.hReadBool('fcSheet') then
      FFormatComment := FFormatComment + [fcSheet];
    if data.hReadBool('fcComponent') then
      FFormatComment := FFormatComment + [fcComponent];
    if data.hReadBool('fcPinName') then
      FFormatComment := FFormatComment + [fcPinName];
    if data.hReadBool('fcGroup') then
      FFormatComment := FFormatComment + [fcGroup];

    FVisibleFiltered := data.hReadBool('VisibleFiltered');
  end else
  begin
    FPinNameFilters.Add('!VCC*_IO');
    FPinNameFilters.Add('IO*');
    FPinNameFilters.Add('*_IO_*');
    FPinNameFilters.Add('*IO');
    FPinNameFilters.Add('*IO,*');
    FPinNameFilters.Add('*, IO*');
    FPinNameFilters.Add('*I/O*');
    FPinNameFilters.Add('MGTH*');
    FPinNameFilters.Add('MGTREFCLK*');
  end;
end;

function TPinDocOptions.Save: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.hWrite<TReplaceSubStrData>('ReplaceSubStr', FReplaceSubStrDatas);
  Result.hWrite('PinNameFilters', FPinNameFilters);

  Result.hWrite('fcNet', fcNet in FFormatComment);
  Result.hWrite('fcSheet', fcSheet in FFormatComment);
  Result.hWrite('fcComponent', fcComponent in FFormatComment);
  Result.hWrite('fcPinName',fcPinName in FFormatComment);
  Result.hWrite('fcGroup', fcGroup in FFormatComment);

  Result.hWrite('VisibleFiltered', FVisibleFiltered);
end;

{ TPinEnumerator }

constructor TPinEnumerator.Create(Grout: TGroup);
begin
  FGroup := Grout;
  buspini := 0;
  busi := 0;
  pini := 0;
  busstarti := 0;
end;

function TPinEnumerator.Next: Boolean;
var
  x: Integer;
begin
  if pini >= FGroup.Pins.Count then
    exit(False);

  FPin := FGroup.Pins[pini];

  while busi < FGroup.Buses.Count do
  begin
    if (buspini < FGroup.Buses[busi].Count) or (FGroup.Buses[busi].Count < 0) then
    begin
      if FGroup.Buses[busi].Name.IsEmpty then
      begin
        FNet := FGroup.Pins[pini].NetName;
        FIndex := -1;
      end else begin
        if (FGroup.Buses[busi].Count = 1) and (FGroup.Buses[busi].StartInd <> 0) then
          x := -1
        else if (FGroup.Buses[busi].StartInd = -1) then
          x := 0
        else
          x := FGroup.Buses[busi].StartInd;

        if FGroup.Buses[busi].Reverce then
        begin
          if FGroup.Buses[busi].Count < 0 then
            FIndex := x + (FGroup.Pins.Count - 1 - buspini - busstarti)
          else
            FIndex := x + (FGroup.Buses[busi].Count - 1 - buspini);
        end else
          FIndex := buspini + x;

        FNet := FGroup.Buses[busi].Name;
      end;

      Inc(buspini);
      Inc(pini);
      exit(True);
    end;
    busstarti := pini;
    Inc(busi);
    buspini := 0;
  end;

  //
  FIndex := -1;
  FNet := FGroup.Pins[pini].NetName;

  Inc(pini);
  exit(True);
end;

{ TPortEnumerator }

constructor TPortEnumerator.Create(Grout: TGroup);
begin
  FGroup := Grout;
  busi := 0;
  pini := 0;
  buspini := 0;
end;

function TPortEnumerator.Next: Boolean;
var
  d: Integer;
begin
  if pini >= FGroup.Pins.Count then
    exit(False);

  while (busi < FGroup.Buses.Count) and (FGroup.Buses[busi].Name.IsEmpty) do
  begin
    if (buspini < FGroup.Buses[busi].Count)  then
    begin
      FHIndex := -1;
      FLIndex := -1;
      FNet := FGroup.Pins[pini].NetName;

      Inc(pini);
      Inc(buspini);
      exit(True);
    end else begin
      Inc(busi);
      buspini := 0;
    end;
  end;

  if busi < FGroup.Buses.Count then
  begin

    FNet := FGroup.Buses[busi].Name;
    if (FGroup.Buses[busi].Count = 1) and (FGroup.Buses[busi].StartInd < 0) then
    begin
      FLIndex := -1;
      FHIndex := -1;

      Inc(pini);
      Inc(busi);
      exit(True);
    end;

    FLIndex := max(0, FGroup.Buses[busi].StartInd);

    if FGroup.Buses[busi].Count >= 0 then
    begin
      d := min(FGroup.Buses[busi].Count, FGroup.Pins.Count - pini);
      FHIndex := FLIndex + d - 1;
      Inc(pini, d);
    end else begin
      FHIndex := FLIndex + FGroup.Pins.Count - pini - 1;
      if FHIndex < 0 then
      begin
        Result := False;
        exit;
      end;

      pini := FGroup.Pins.Count;
    end;

    Inc(busi);
    exit(True);
  end;

  //
  FHIndex := -1;
  FLIndex := -1;

  FNet := FGroup.Pins[pini].NetName;
  Inc(pini);
  exit(True);
end;

{ TPortEnumeratorAdapter }

constructor TPortEnumeratorAdapter.Create(Grout: TGroup);
begin
    enumerator := TPortEnumerator.Create(Grout);
    FNotLast := enumerator.Next();
end;

function TPortEnumeratorAdapter.Next: Boolean;
begin
  Result := FNotLast;
  if FNotLast then
  begin
    FNet := enumerator.Net;
    FHIndex := enumerator.HIndex;
    FLIndex := enumerator.LIndex;
    FNotLast := enumerator.Next();
  end;
end;

end.
