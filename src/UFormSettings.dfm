object FormSettings: TFormSettings
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
  ClientHeight = 426
  ClientWidth = 590
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 247
    Height = 385
    Margins.Left = 5
    Margins.Top = 5
    Margins.Right = 5
    Margins.Bottom = 10
    Align = alLeft
    Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1085#1072#1079#1074#1072#1085#1080#1102' '#1087#1080#1085#1072
    TabOrder = 0
    object mmFiltrList: TMemo
      Left = 14
      Top = 24
      Width = 225
      Height = 321
      Lines.Strings = (
        'mmWhiteList')
      ParentShowHint = False
      ScrollBars = ssVertical
      ShowHint = True
      TabOrder = 0
    end
    object cbVisibleFiltered: TCheckBox
      Left = 14
      Top = 354
      Width = 185
      Height = 17
      Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1086#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1085#1085#1099#1077
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 385
    Width = 590
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      590
      41)
    object Button1: TButton
      Left = 506
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object Button2: TButton
      Left = 425
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 1
    end
    object btnSetAsDefault: TButton
      Left = 14
      Top = 8
      Width = 225
      Height = 25
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1082#1072#1082' '#1085#1072#1089#1090#1088#1086#1081#1082#1080' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      TabOrder = 2
      OnClick = btnSetAsDefaultClick
    end
  end
  object Panel2: TPanel
    Left = 247
    Top = 0
    Width = 343
    Height = 385
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object GroupBox2: TGroupBox
      Left = 6
      Top = 0
      Width = 331
      Height = 146
      Align = alCustom
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1080
      TabOrder = 0
      object cbCommentGroups: TCheckBox
        Left = 24
        Top = 24
        Width = 97
        Height = 17
        Caption = #1053#1072#1079#1074#1072#1085#1080#1103' '#1075#1088#1091#1087#1087
        TabOrder = 0
      end
      object cbCommentNet: TCheckBox
        Left = 24
        Top = 47
        Width = 97
        Height = 17
        Caption = #1053#1072#1079#1074#1072#1085#1080#1103' '#1094#1077#1087#1077#1081
        TabOrder = 1
      end
      object cbCommentSheet: TCheckBox
        Left = 24
        Top = 70
        Width = 97
        Height = 17
        Caption = #1051#1080#1089#1090#1099' '#1089#1093#1077#1084#1099
        TabOrder = 2
      end
      object cbCommentComponent: TCheckBox
        Left = 24
        Top = 93
        Width = 123
        Height = 17
        Caption = #1050#1086#1084#1087#1086#1085#1077#1085#1090' '#1085#1072' '#1089#1093#1077#1084#1077
        TabOrder = 3
      end
      object cbCommentPin: TCheckBox
        Left = 24
        Top = 116
        Width = 97
        Height = 17
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1087#1080#1085#1086#1074
        TabOrder = 4
      end
    end
    object GroupBox3: TGroupBox
      Left = 8
      Top = 148
      Width = 329
      Height = 237
      Margins.Top = 10
      Caption = #1047#1072#1084#1077#1085#1099' ('#1076#1086' '#1075#1088#1091#1087#1087#1080#1088#1086#1074#1082#1080')'
      Padding.Left = 3
      Padding.Top = 5
      Padding.Right = 3
      Padding.Bottom = 3
      TabOrder = 1
      object sgReplace: TStringGrid
        Left = 5
        Top = 42
        Width = 319
        Height = 190
        Align = alClient
        ColCount = 3
        DefaultColWidth = 100
        DefaultRowHeight = 20
        DrawingStyle = gdsClassic
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goEditing, goTabs]
        PopupMenu = pmReplace
        TabOrder = 0
        OnContextPopup = sgReplaceContextPopup
        ColWidths = (
          100
          100
          100)
        RowHeights = (
          20
          20)
      end
      object TBXToolbar1: TTBXToolbar
        Left = 5
        Top = 20
        Width = 319
        Height = 22
        Align = alTop
        Caption = 'TBXToolbar1'
        Images = dmCommonData.Images16
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        object TBXItem3: TTBXItem
          Action = actReplaceAdd
          DisplayMode = nbdmTextOnlyInMenus
        end
        object TBXItem2: TTBXItem
          Action = actReplaceDel
          DisplayMode = nbdmTextOnlyInMenus
        end
        object TBXSeparatorItem1: TTBXSeparatorItem
          Blank = True
        end
        object TBXItem1: TTBXItem
          Action = actClean
          DisplayMode = nbdmTextOnlyInMenus
        end
      end
    end
  end
  object alReplace: TActionList
    Images = dmCommonData.Images16
    OnUpdate = alReplaceUpdate
    Left = 345
    Top = 258
    object actReplaceAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 16
      OnExecute = actReplaceAddExecute
    end
    object actReplaceDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 17
      OnExecute = actReplaceDelExecute
    end
    object actClean: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1105
      ImageIndex = 18
      OnExecute = actCleanExecute
    end
  end
  object pmReplace: TPopupMenu
    Images = dmCommonData.Images16
    Left = 281
    Top = 256
    object N1: TMenuItem
      Action = actReplaceAdd
    end
    object N2: TMenuItem
      Action = actReplaceDel
    end
  end
end
