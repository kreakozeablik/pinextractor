unit VirtualTreeViewEditors;

interface

uses VirtualTrees, Vcl.StdCtrls, System.Types, Messages, System.Classes, Vcl.Controls, CoolEditor;

type
  THackWinControl = class(TWinControl)
  public
    property OnKeyDown;
  end;

  TVTBaseCellEditor<T: TWinControl> = class(TInterfacedObject, IVTEditLink)
   private
     FEdit: T;
     FTree: TVirtualStringTree; // ������ �� ������, ��������� ��������������
     FNode: PVirtualNode;       // ������������� ����
     FColumn: Integer;          // ��� �������, � ������� ��� ����������
   protected
     procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); virtual;
     function GetText(): string; virtual;
     procedure SetText(v: string); virtual;
     procedure XBeginEdit(); virtual;
   public
     constructor Create(Sender: TBaseVirtualTree);
     destructor Destroy; override;

     property Editor: T read FEdit;

     function BeginEdit: Boolean; stdcall;
     function CancelEdit: Boolean; stdcall;
     function EndEdit: Boolean; stdcall;
     function GetBounds: TRect; stdcall;
     function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; stdcall;
     procedure ProcessMessage(var Message: TMessage); stdcall;
     procedure SetBounds(R: TRect); stdcall;
   end;

  TVTComboCellEditor = class(TVTBaseCellEditor<TComboBox>)
   protected
     procedure EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
     function GetText(): string; override;
     procedure SetText(v: string); override;
     procedure XBeginEdit(); override;
  end;

  TVTNumberCellEditor = class(TVTBaseCellEditor<TCoolEditor>)
   protected
     function GetText(): string; override;
     procedure SetText(v: string); override;
  end;

implementation

uses System.SysUtils, Windows;

function TVTBaseCellEditor<T>.BeginEdit: Boolean;
begin
     Result := True;
     FEdit.Show;
     FEdit.SetFocus;
     XBeginEdit();
end;

function TVTBaseCellEditor<T>.CancelEdit: Boolean;
begin
     Result := True;
     FEdit.Hide;
end;

constructor TVTBaseCellEditor<T>.Create(Sender: TBaseVirtualTree);
begin
  FEdit := T.Create(Sender);
  FEdit.Parent := Sender;
  FEdit.Visible := False;
  THackWinControl(FEdit).OnKeyDown := EditKeyDown;
end;

destructor TVTBaseCellEditor<T>.Destroy;
begin
     FreeAndNil(FEdit);
     inherited;
end;

procedure TVTBaseCellEditor<T>.EditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var CanContinue: Boolean;
begin
  case Key of
    VK_ESCAPE: // ������ Escape
      begin
        FTree.CancelEditNode;
        Key := 0;
      end;
    VK_RETURN: // ������ Enter
      begin
        FTree.EndEditNode;
        Key := 0;
      end;
  end;
end;

function TVTBaseCellEditor<T>.EndEdit: Boolean;
begin
     FTree.Text[FNode, FColumn] := GetText;
     FEdit.Hide;
     FTree.SetFocus;
     Result := true;
end;

function TVTBaseCellEditor<T>.GetBounds: TRect;
begin
     Result := FEdit.BoundsRect;
end;

function TVTBaseCellEditor<T>.GetText: string;
begin
  Result := '';
end;

function TVTBaseCellEditor<T>.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;
begin
     Result := True;
     FTree := Tree as TVirtualStringTree;
     FNode := Node;
     FColumn := Column;
     SetText(FTree.Text[FNode, FColumn]);
end;

procedure TVTBaseCellEditor<T>.ProcessMessage(var Message: TMessage);
begin
     FEdit.WindowProc(Message);
end;

procedure TVTBaseCellEditor<T>.SetBounds(R: TRect);
var Dummy: Integer;
begin
     FTree.Header.Columns.GetColumnBounds(FColumn, Dummy, R.Right);
     FEdit.BoundsRect := R;
end;

procedure TVTBaseCellEditor<T>.SetText(v: string);
begin

end;

procedure TVTBaseCellEditor<T>.XBeginEdit;
begin

end;

{ TVTComboCellEditor }

procedure TVTComboCellEditor.EditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_UP, VK_DOWN:
      if (Shift = []) and not Editor.DroppedDown then
      begin
        PostMessage(FTree.Handle, WM_KEYDOWN, Key, 0);
        Key := 0;
      end;
  end;
end;

function TVTComboCellEditor.GetText: string;
begin
  Result := Editor.Text;
end;

procedure TVTComboCellEditor.SetText(v: string);
var
  i: Integer;
begin
  i := Editor.Items.IndexOf(v);
  if i < 0 then
    Editor.Text := v
  else
    Editor.ItemIndex := i;
end;

procedure TVTComboCellEditor.XBeginEdit;
begin
  Editor.DroppedDown:=True;
end;

{ TVTNumberCellEditor }

function TVTNumberCellEditor.GetText: string;
begin
  Result := Editor.Text;
end;

procedure TVTNumberCellEditor.SetText(v: string);
begin
  Editor.Text := v;
end;

end.
