program PinExtractor;



{$R *.dres}

uses
  Forms,
  UMainForm in 'UMainForm.pas' {MainForm},
  strutil in 'strutil.pas',
  UFormWait in 'UFormWait.pas' {FormWait},
  USelectCompForm in 'USelectCompForm.pas' {SelectCompForm},
  Shema in 'Shema.pas',
  AltiumComponent in 'AltiumFileReader\AltiumComponent.pas',
  AltiumProject in 'AltiumFileReader\AltiumProject.pas',
  AltiumRecordEnum in 'AltiumFileReader\AltiumRecordEnum.pas',
  AltiumSheet in 'AltiumFileReader\AltiumSheet.pas',
  AppConfig in 'AppConfig.pas',
  PinDoc in 'PinDoc.pas',
  UDlgGroupForm in 'UDlgGroupForm.pas' {DlgGroupForm},
  SynHinhlightUCF in 'SynHinhlightUCF.pas',
  UFormSettings in 'UFormSettings.pas' {FormSettings},
  UMoveList in 'MoveList\UMoveList.pas',
  UConstraintsFormatter in 'UConstraintsFormatter.pas',
  UFrameSelectComp in 'UFrameSelectComp.pas' {FrameSelectComp: TFrame},
  UFormMezaninEdt in 'UFormMezaninEdt.pas' {FormMezaninEdt},
  JSONHelpers in 'JSONHelpers.pas',
  UFormMezaninConnection in 'UFormMezaninConnection.pas' {FormMezaninConnection},
  UdmCommonData in 'UdmCommonData.pas' {dmCommonData: TDataModule},
  VirtualTreeViewEditors in 'VirtualTreeViewEditors.pas',
  CoolEditor in 'CoolEditor.pas',
  UAboutForm in 'UAboutForm.pas' {AboutForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  LoadConfig;
  Application.CreateForm(TdmCommonData, dmCommonData);
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
  SaveConfig;
end.
