unit USelectCompForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.ComCtrls, Shema, PinDoc, UFrameSelectComp;

type
  TSelectCompForm = class(TForm)
    Panel1: TPanel;
    btnCancel: TButton;
    btnOk: TButton;
    Panel2: TPanel;
    Label2: TLabel;
    Button1: TButton;
    edtFile: TComboBox;
    fSelectComp: TFrameSelectComp;
    procedure FormCreate(Sender: TObject);
    procedure OnOpenFile(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edtFileClick(Sender: TObject);
  private
    FShema: TShema;
    FDoc: TPinDoc;
    fname: string;
    procedure SelectCurrent();
    procedure LoadFile(fn: string);
    procedure ShowOpenDlg();
    procedure OnSelectComp(Comp: TComp);
    procedure OnFilteredComps();
  public

    property Doc: TPinDoc read FDoc;
    procedure Init(vdoc: TPinDoc; fn: string = ''; cname: string = '');
  end;

implementation

uses UFormWait, AppConfig, strutil, UdmCommonData;

{$R *.dfm}

const
  CONFIG_FILES_LST = 'files_lst';
  CONFIG_FILTR_LST = 'filtr_lst';

procedure TSelectCompForm.Init(vdoc: TPinDoc; fn: string = ''; cname: string = '');
begin
  FDoc := vdoc;
  FShema.Assign(vdoc.Shema);

  if fn <> '' then
  begin
    AddComboLines(edtFile.Items, fn);
    edtFile.ItemIndex := 0;
    LoadFile(fn);
  end
  else if FShema.FileName <> '' then
    begin
      AddComboLines(edtFile.Items, FShema.FileName);
      edtFile.ItemIndex := 0;

      fSelectComp.SetCompList(FShema);
    end;

  if not cname.IsEmpty then
    fSelectComp.SelectComp(cname)
  else if Assigned(vdoc.CurrentComp) then
    fSelectComp.SelectComp(vdoc.CurrentComp.Name)
end;

procedure TSelectCompForm.LoadFile(fn: string);
begin
  edtFile.Text := fn;

  fname := fn;
  RunAsync(procedure()begin FShema.LoadFromFile(fname); end);
  fSelectComp.SetCompList(FShema);
  if Visible then
    fSelectComp.sgSelComp.SetFocus;
end;

procedure TSelectCompForm.OnFilteredComps;
begin
  btnOk.Enabled := fSelectComp.Selected;
end;

procedure TSelectCompForm.OnOpenFile(Sender: TObject);
begin
  ShowOpenDlg;
end;

procedure TSelectCompForm.OnSelectComp(Comp: TComp);
begin
  SelectCurrent();
end;

procedure TSelectCompForm.SelectCurrent;
var
  fn: string;
begin
  if fSelectComp.Selected then
  begin
    FDoc.CurrentComp := Nil;
    FDoc.SwapShema(FShema);
    FDoc.CurrentComp := fSelectComp.GetComp();

    edtFile.Items.Delete(edtFile.Items.Count-1);
    Config.WriteStrings(CONFIG_FILES_LST, edtFile.Items);

    fSelectComp.StoreFiltr;
    Config.WriteStrings(CONFIG_FILTR_LST, fSelectComp.FiltrList);

    fn := ExtractFileDir(FDoc.Shema.FileName) + '\' + FDoc.GetFileComp() + '.pin';
    if FileExists(fn) then
      FDoc.Load(fn, False);

    ModalResult := mrOk;
  end
  else
    MessageBeep(MB_ICONWARNING);
end;

procedure TSelectCompForm.ShowOpenDlg;
begin
  if dmCommonData.OpenDialog.Execute then
  begin
    AddComboLines(edtFile.Items, dmCommonData.OpenDialog.FileName);
    edtFile.ItemIndex := 0;
    LoadFile(dmCommonData.OpenDialog.FileName);
  end
  else if edtFile.Items.Count = 1 then
    edtFile.ItemIndex := -1
  else
    edtFile.ItemIndex := 0;
end;

procedure TSelectCompForm.btnOkClick(Sender: TObject);
begin
  SelectCurrent();
end;

procedure TSelectCompForm.edtFileClick(Sender: TObject);
begin
  if edtFile.ItemIndex = (edtFile.Items.Count - 1) then
  begin
    ShowOpenDlg();
  end
  else if edtFile.ItemIndex > -1 then
  begin
    AddComboLines(edtFile.Items, edtFile.Text);
    edtFile.ItemIndex := 0;
    LoadFile(edtFile.Text);
  end;
end;

procedure TSelectCompForm.FormCreate(Sender: TObject);
begin
  FShema := TShema.Create();

  fSelectComp.CompPrefix := 'DD';
  fSelectComp.Init(OnSelectComp, OnFilteredComps);

  Config.ReadStrings(CONFIG_FILTR_LST, fSelectComp.FiltrList);
  fSelectComp.RstFiltr;

  Config.ReadStrings(CONFIG_FILES_LST, edtFile.Items);
  edtFile.Items.Add('<- ������� ->');
end;

procedure TSelectCompForm.FormDestroy(Sender: TObject);
begin
  FShema.Free;
end;

end.
