unit CoolEditor;

interface

uses
  Windows, Messages, Classes, Controls, StdCtrls, Clipbrd, SysUtils, Forms,
  Graphics, Math; // MihUtilities,


type
  TCoolEditor = class;

  TCEBase = (cebText, cebUnsigned, cebSigned, cebHexadecimal, cebBinary);

  TCEOnAfterChange = procedure (sender: TCoolEditor) of object;

  TCoolEditor = class(TEdit)
  private
    fBase: TCEBase;
    fBitCount: Integer;
    FChangeTime: Integer;
    FAfterChange: TCEOnAfterChange;

    FChanged: Boolean;

    procedure SetBase(const Value: TCEBase);
    function GetAsInteger: Integer;
    procedure SetAsInteger(const Value: Integer);
    procedure SetBitCount(const Value: Integer);
    function GetAsCardinal: Cardinal;
    procedure SetAsCardinal(const Value: Cardinal);

  protected
    procedure SetMaxLength();

    procedure KeyPress(var Key: Char); override;

    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;

    procedure WMPaste(var Message: TWMPaste); message WM_PASTE;
    procedure WMTimer(var Message: TWMTimer); message WM_TIMER;

    procedure SetChangeTimer;
  public
    constructor Create(AOwner:TComponent); override;

    property AsCardinal: Cardinal read GetAsCardinal write SetAsCardinal;
    property AsInteger: Integer read GetAsInteger write SetAsInteger;

    property PasswordChar;
    property MaxLength;
    property NumbersOnly;
  published
    property Base: TCEBase read fBase write SetBase default cebText;
    property BitCount: Integer read fBitCount write SetBitCount default 32;
    property Alignment default taRightJustify;
    property ChangeTime: Integer read FChangeTime write FChangeTime default -1;  // ms
    property OnAfterChange: TCEOnAfterChange read FAfterChange write FAfterChange;
  end;

implementation

{ TCoolEditor }

procedure TCoolEditor.SetChangeTimer;
begin
  FChanged := true;
  if((ChangeTime > -1) and HandleAllocated)then
    SetTimer(Handle, 0, ChangeTime, nil);
end;

constructor TCoolEditor.Create(AOwner: TComponent);
begin
  inherited;
  FChanged := false;
  fBase := cebText;
  fBitCount := 0;
  Alignment := taRightJustify;
  BitCount := 32;
  ChangeTime := -1;
end;

function TCoolEditor.GetAsCardinal: Cardinal;
var
  tmperr: Integer;
begin
    if(Length(Text) > 0)then
      case fBase of
      cebUnsigned, cebSigned: {result := Cardinal(StrToInt(Text));} val(Text, result, tmperr);
      //cebHexadecimal: result := Cardinal(HexStrToInt(Text));
      //cebBinary: result := Cardinal(BinStrToInt(Text, 0));
      else raise Exception.Create('Невозможно получить числовое значение!');
      end
    else
      result := 0;
end;

function TCoolEditor.GetAsInteger: Integer;
begin
    if(Length(Text) > 0)then
      case fBase of
      cebUnsigned, cebSigned: result := StrToInt(Text);
      //cebHexadecimal: result := HexStrToInt(Text);
      //cebBinary: result := BinStrToInt(Text, fBitCount);
      else raise Exception.Create('Невозможно получить числовое значение!');
      end
    else
      result := 0;
end;

procedure TCoolEditor.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;

  if((Key in [VK_END, VK_HOME, VK_LEFT, VK_UP, VK_RIGHT, VK_DOWN]) and FChanged and (ChangeTime <> -1)) then
    KillTimer(Handle, 0);
end;

procedure TCoolEditor.KeyPress(var Key: Char);
begin
  if (Key > #31)then
    case fBase of

    cebUnsigned: begin
      if(not CharInSet(Key, ['0'..'9']))then
        Key := #7;
    end;

    cebSigned: begin
      if(not CharInSet(Key, ['0'..'9']))then
      if(not((Key = '-') and (SelStart = 0) and ((Length(Text) = 0) or(Text[1] <> '-'))))then
        Key := #7;
    end;

    cebHexadecimal: begin
      if(not CharInSet(Key, ['0'..'9', 'A'..'F']))then begin
        if(CharInSet(Key, ['a'..'f']))then
          Key := Char(Byte(Key) - 32)
        else
          Key := #7;
      end;
    end;

    cebBinary: begin
      if(not CharInSet(Key, ['0'..'1']))then
        Key := #7;
    end;

    end;

  if(Key <> #7)then
    SetChangeTimer;

  inherited KeyPress(Key);
end;

procedure TCoolEditor.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if((Key in [VK_END, VK_HOME, VK_LEFT, VK_UP, VK_RIGHT, VK_DOWN]) and FChanged) then
    SetChangeTimer;
end;

procedure TCoolEditor.SetAsCardinal(const Value: Cardinal);
begin
    case fBase of
    cebSigned:  Text := Format('%d', [Value]);
    //cebUnsigned: Text := Format('%u', [Value]);
    cebHexadecimal: Text := Format('%X', [Value]);
    //cebBinary: Text := IntToBinStr(Integer(Value));
    else Text := Format('%u', [Value]);
    end;
end;

procedure TCoolEditor.SetAsInteger(const Value: Integer);
begin
    case fBase of
    //cebSigned:  Text := Format('%d', [Value]);
    cebUnsigned: Text := Format('%u', [Value]);
    cebHexadecimal: Text := Format('%X', [Value]);
    //cebBinary: Text := IntToBinStr(Value);
    else Text := Format('%d', [Value]);
    end;
end;

procedure TCoolEditor.SetBase(const Value: TCEBase);
var
  tmp: Cardinal;
begin
  if(fBase <> cebText)then
    tmp := AsCardinal
  else
    tmp := 0;
  fBase := Value;
  SetMaxLength;
  AsCardinal := tmp;
end;

procedure TCoolEditor.SetBitCount(const Value: Integer);
var
  tmp: Cardinal;
begin
  if(fBase <> cebText)then
    tmp := AsCardinal
  else
    tmp := 0;
  fBitCount := Value;
  SetMaxLength;
  AsCardinal := tmp;
end;

procedure TCoolEditor.SetMaxLength;
var
    i: Integer;
begin
    case fBase of
    cebHexadecimal:
        begin
            i := BitCount div 4;
            if((BitCount and $00000003) <> 0)then
                Inc(i);
            MaxLength := i;
        end;
    cebBinary: MaxLength := fBitCount;
    else MaxLength := 0;
    end;
end;

procedure TCoolEditor.WMPaste(var Message: TWMPaste);
var
  SGlobalHnd : THandle;

  Ptr        : PAnsiChar;
  //Ptr        : PChar;

  Size,i     : Longint;
  s          : AnsiString;
begin
  s:='';
  if OpenClipboard(Handle) then begin
    try
      if Clipboard.HasFormat(CF_TEXT) then begin
        SGlobalHnd:=GetClipboardData(CF_TEXT);
        if SGlobalHnd<>0 then begin
          Size:=GlobalSize(SGlobalHnd);
          Ptr:=GlobalLock(SGlobalHnd);
          if Ptr<>nil then begin
            i:=0;


            case fBase of

            cebText: begin
              while (i<size) do begin
                s := s + Ptr[i];
                inc(i);
              end;
            end;

            cebSigned: begin
              if ((0<size) and (Ptr[0] = '-') and (SelStart = 0))then begin
                s := s + Ptr[0];
                inc(i);
              end;
              while (i<size) and CharInSet(Ptr[i], ['0'..'9']) do begin
                s := s + Ptr[i];
                inc(i);
              end;
            end;

            cebUnsigned:
              while (i<size) and CharInSet(Ptr[i], ['0'..'9']) do begin
                s := s + Ptr[i];
                inc(i);
              end;

            cebHexadecimal:
              while ((i<size) and CharInSet(Ptr[i], ['0'..'9', 'A'..'F', 'a'..'f'])) do begin
                if(CharInSet(Ptr[i], ['a'..'f']))then
                  s := s + AnsiChar(Byte(Ptr[i]) - 32)
                else
                  s := s + Ptr[i];

                inc(i);
              end;

            cebBinary:
              while (i<size) and CharInSet(Ptr[i], ['0'..'1']) do begin
                s := s + Ptr[i];
                inc(i);
              end;

            end;

          end;
          GlobalUnlock(SGlobalHnd);
        end;
      end;

      SetChangeTimer;
    finally
      CloseClipboard;
    end;
  end;

  SelText:=string(s);
end;

procedure TCoolEditor.WMTimer(var Message: TWMTimer);
begin
  if(FChanged and Assigned(FAfterChange))then
    FAfterChange(self);

  FChanged := false;
end;

end.
