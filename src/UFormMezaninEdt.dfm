object FormMezaninEdt: TFormMezaninEdt
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1052#1077#1079#1072#1085#1080#1085#1099
  ClientHeight = 358
  ClientWidth = 794
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TBXToolbar1: TTBXToolbar
    Left = 0
    Top = 0
    Width = 794
    Height = 22
    Align = alTop
    Caption = 'TBXToolbar1'
    Images = dmCommonData.Images16
    TabOrder = 0
    object TBXItem1: TTBXItem
      Action = actNewMezanin
    end
    object TBXItem3: TTBXItem
      Action = actNewConnection
    end
    object TBXSeparatorItem1: TTBXSeparatorItem
    end
    object TBXItem2: TTBXItem
      Action = actDelete
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 319
    Width = 794
    Height = 39
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      794
      39)
    object btnCancel: TButton
      Left = 709
      Top = 6
      Width = 78
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object Button2: TButton
      Left = 628
      Top = 6
      Width = 78
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #1055#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
      Default = True
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object VST: TVirtualStringTree
    Left = 0
    Top = 22
    Width = 794
    Height = 297
    Align = alClient
    Colors.BorderColor = 15987699
    Colors.DisabledColor = clGray
    Colors.DropMarkColor = 15385233
    Colors.DropTargetColor = 15385233
    Colors.DropTargetBorderColor = 15385233
    Colors.FocusedSelectionColor = 15385233
    Colors.FocusedSelectionBorderColor = 15385233
    Colors.GridLineColor = 15987699
    Colors.HeaderHotColor = clBlack
    Colors.HotColor = clBlack
    Colors.SelectionRectangleBlendColor = 15385233
    Colors.SelectionRectangleBorderColor = 15385233
    Colors.SelectionTextColor = clBlack
    Colors.TreeLineColor = 9471874
    Colors.UnfocusedColor = clGray
    Colors.UnfocusedSelectionColor = 13421772
    Colors.UnfocusedSelectionBorderColor = 13421772
    Header.AutoSizeIndex = 0
    Header.Options = [hoAutoResize, hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible]
    HintMode = hmTooltip
    ParentShowHint = False
    PopupMenu = PopupMenu1
    ShowHint = True
    TabOrder = 2
    TreeOptions.MiscOptions = [toAcceptOLEDrop, toEditable, toFullRepaintOnResize, toInitOnSave, toWheelPanning, toEditOnClick, toEditOnDblClick]
    TreeOptions.SelectionOptions = [toExtendedFocus, toFullRowSelect, toRightClickSelect]
    OnEditing = VSTEditing
    OnGetText = VSTGetText
    OnPaintText = VSTPaintText
    OnNewText = VSTNewText
    OnNodeDblClick = VSTNodeDblClick
    Columns = <
      item
        Options = [coAllowClick, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coSmartResize, coAllowFocus, coStyleColor]
        Position = 0
        Text = #1057#1093#1077#1084#1072
        Width = 640
      end
      item
        Options = [coAllowClick, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coEditable, coStyleColor]
        Position = 1
        Text = #1055#1088#1077#1092#1080#1082#1089
        Width = 150
      end>
  end
  object ActionList1: TActionList
    Images = dmCommonData.Images16
    OnUpdate = ActionList1Update
    Left = 136
    Top = 88
    object actNewMezanin: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1084#1077#1079#1072#1085#1080#1085'...'
      ImageIndex = 21
      OnExecute = actNewMezaninExecute
    end
    object actNewConnection: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077'...'
      ImageIndex = 29
      OnExecute = actNewConnectionExecute
    end
    object actDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 17
      OnExecute = actDeleteExecute
    end
    object actEdit: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100'...'
      ImageIndex = 6
      OnExecute = actEditExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Images = dmCommonData.Images16
    Left = 136
    Top = 144
    object N4: TMenuItem
      Action = actEdit
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = actNewMezanin
    end
    object N2: TMenuItem
      Action = actNewConnection
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = actDelete
    end
  end
end
