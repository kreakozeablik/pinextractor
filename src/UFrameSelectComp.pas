unit UFrameSelectComp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.StdCtrls,
  Vcl.ExtCtrls, Shema;

const
  WM_COMCHANGE = WM_USER + 777;

type
  TOnSelectComp = procedure (Comp: TComp) of object;
  TOnFilteredComps = procedure () of object;

  TFrameSelectComp = class(TFrame)
    Panel2: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    edtFiltr: TComboBox;
    sgSelComp: TStringGrid;
    cbViewAll: TCheckBox;
    procedure FrameResize(Sender: TObject);
    procedure edtFiltrChange(Sender: TObject);
    procedure edtFiltrKeyPress(Sender: TObject; var Key: Char);
    procedure sgSelCompKeyPress(Sender: TObject; var Key: Char);
    procedure sgSelCompDblClick(Sender: TObject);
    procedure sgSelCompSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    FOldSelectRow: Integer;
    FRowCnt: Integer;
    FCompList: TShema;
    FOnSelectComp: TOnSelectComp;
    FOnFilteredComps: TOnFilteredComps;
    FOnSelectClick: TOnSelectComp;
    FCompPrefix: string;
    procedure SetLastColWidth;
    procedure FiltrComp();
    procedure SelectCurrent();
    function GetFiltrList: TStrings;

    procedure WMComChange(var Message: TMessage); message WM_COMCHANGE;
  public
    property CompPrefix: string read FCompPrefix write FCompPrefix;
    property FiltrList: TStrings read GetFiltrList;
    procedure RstFiltr();
    procedure StoreFiltr();
    function Selected: Boolean;
    function GetComp(): TComp;
    procedure SelectComp(cname: string);
    procedure SetCompList(CompList: TShema);
    procedure Init(OnSelectComp: TOnSelectComp = nil; OnFilteredComps: TOnFilteredComps = nil; OnSelectClick: TOnSelectComp = nil);
  end;

implementation

uses Math, strutil, StrUtils;

const
  COL_WIDTH_NAME = 80;
  COL_WIDTH_DESIGNATOR = 250;
  MIN_COL_WIDTH_DESCRIPTION = 250;

{$R *.dfm}

procedure TFrameSelectComp.edtFiltrChange(Sender: TObject);
begin
  FiltrComp();
end;

procedure TFrameSelectComp.edtFiltrKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) then
  begin
    SelectCurrent;
    Key := #0;
  end;
end;

procedure TFrameSelectComp.FiltrComp;
var
  c: TComp;
  uccf: string;
  v: Boolean;
  viewAll: Boolean;
begin
  viewAll := cbViewAll.Checked;

  v := Visible;
  if v then
    SendMessage(sgSelComp.Handle, WM_SETREDRAW, 0, 0);

  uccf := UpperCase(edtFiltr.Text);

  sgSelComp.RowCount := 1;
  sgSelComp.Cells[0, 0] := '';
  sgSelComp.Cells[1, 0] := '';
  sgSelComp.Cells[2, 0] := '';
  sgSelComp.Objects[0, 0] := nil;

  FRowCnt := 0;

  if Assigned(FCompList) then
    for c in FCompList do
    begin
      if(((uccf = '') or (pos(uccf, UpperCase(c.Name)) > 0) or (pos(uccf, UpperCase(c.Designator)) > 0)) and
       (viewAll or (FCompPrefix = '') or StartsStr(FCompPrefix, UpperCase(c.Name))))then
      begin
        if FRowCnt > 0 then
          sgSelComp.RowCount := sgSelComp.RowCount + 1;
        sgSelComp.Cells[0, FRowCnt] := c.Name;
        sgSelComp.Cells[1, FRowCnt] := c.Designator;
        sgSelComp.Cells[2, FRowCnt] := c.Description;
        sgSelComp.Objects[0, FRowCnt] := c;
        Inc(FRowCnt);
      end;
    end;

  SetLastColWidth;

  if v then
  begin
    SendMessage(sgSelComp.Handle, WM_SETREDRAW, 1, 0);
    sgSelComp.Invalidate();
  end;

  if Assigned(FOnFilteredComps) then
    FOnFilteredComps();

  PostMessage(Handle, WM_COMCHANGE, 0, 0);
end;

procedure TFrameSelectComp.FrameResize(Sender: TObject);
begin
  SetLastColWidth;
end;

function TFrameSelectComp.GetComp: TComp;
begin
  Result := nil;
  if FRowCnt > 0 then
    Result := (sgSelComp.Objects[0, sgSelComp.Row] as TComp);
end;

function TFrameSelectComp.GetFiltrList: TStrings;
begin
  Result := edtFiltr.Items;
end;

procedure TFrameSelectComp.Init(OnSelectComp: TOnSelectComp; OnFilteredComps: TOnFilteredComps; OnSelectClick: TOnSelectComp);
begin
  FOldSelectRow := -1;
  FOnSelectComp := OnSelectComp;
  FOnSelectClick := OnSelectClick;
  FOnFilteredComps := OnFilteredComps;

  sgSelComp.ColWidths[0] := COL_WIDTH_NAME;
  sgSelComp.ColWidths[1] := COL_WIDTH_DESIGNATOR;
  SetLastColWidth;
end;

procedure TFrameSelectComp.RstFiltr;
begin
  if edtFiltr.Items.Count > 0 then
    edtFiltr.ItemIndex := 0
end;

procedure TFrameSelectComp.SelectComp(cname: string);
var
  i: Integer;
  ucname: string;
begin
  ucname := UpperCAse(cname);
  for i := 0 to sgSelComp.RowCount - 1 do
  begin
    if UpperCase(sgSelComp.Cells[0, i]) = ucname then
    begin
      sgSelComp.Row := i;
      break;
    end;
  end;
end;

procedure TFrameSelectComp.SelectCurrent;
var
  c: TComp;
begin
  if Assigned(FOnSelectComp) then
  begin
    c := GetComp();
    if Assigned(c) then
      FOnSelectComp(c);
  end;
end;

function TFrameSelectComp.Selected: Boolean;
begin
  Result := FRowCnt > 0;
end;

procedure TFrameSelectComp.SetCompList(CompList: TShema);
begin
  FCompList := CompList;
  FiltrComp();
end;

procedure TFrameSelectComp.SetLastColWidth;
begin
  sgSelComp.ColWidths[2] := Max(MIN_COL_WIDTH_DESCRIPTION,
    sgSelComp.ClientWidth - COL_WIDTH_NAME - COL_WIDTH_DESIGNATOR);
end;

procedure TFrameSelectComp.sgSelCompDblClick(Sender: TObject);
begin
  SelectCurrent();
end;

procedure TFrameSelectComp.sgSelCompKeyPress(Sender: TObject; var Key: Char);
  procedure FiltrBack();
  var
    l: Integer;
    s: string;
  begin
    s := edtFiltr.Text;
    l := Length(s);
    if (l > 0) then
    begin
      delete(s, l, 1);
      edtFiltr.Text := s;
      FiltrComp();
    end;
  end;
begin
  case Key of
    #13: SelectCurrent();
    'A'..'Z', '_', 'a'..'z', '0'..'9':
    begin
      edtFiltr.Text := edtFiltr.Text + Key;
      FiltrComp();
    end;
    #8: FiltrBack();
  end;
end;

procedure TFrameSelectComp.sgSelCompSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> FOldSelectRow then
  begin
    PostMessage(Handle, WM_COMCHANGE, 0, 0);
    FOldSelectRow := ARow;
  end;
end;

procedure TFrameSelectComp.StoreFiltr;
begin
  AddComboLines(edtFiltr.Items, edtFiltr.Text);
end;

procedure TFrameSelectComp.WMComChange(var Message: TMessage);
begin
  if Assigned(FOnSelectClick) then
    FOnSelectClick(GetComp());
end;

end.
