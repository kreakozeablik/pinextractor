object AboutForm: TAboutForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
  ClientHeight = 307
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 266
    Width = 457
    Height = 41
    Align = alBottom
    TabOrder = 0
    object Button1: TButton
      Left = 368
      Top = 8
      Width = 75
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 457
    Height = 266
    Align = alClient
    Lines.Strings = (
      ''
      '                       -=  PinExtractor v1.0.0 (15.01.2021) =-'
      ''
      '                '#169' 2021 kreakozeablik (kreakozeablik@yandex.ru)'
      ''
      
        '    '#1055#1088#1086#1075#1088#1072#1084#1084#1072' '#1087#1088#1077#1076#1085#1072#1079#1085#1072#1095#1077#1085#1072' '#1076#1083#1103' '#1087#1086#1083#1091#1095#1077#1085#1080#1103' '#1092#1072#1081#1083#1086#1074' '#1087#1088#1080#1074#1103#1079#1082#1080' '#1094#1077#1087#1077#1081' ' +
        #1082' '#1087#1080#1085#1072#1084
      'FPGA '#1080#1079' '#1089#1093#1077#1084' '#1074' '#1092#1086#1088#1084#1072#1090#1077' Altium Designer.'
      ''
      #1048#1089#1093#1086#1076#1085#1099#1081' '#1082#1086#1076': https://bitbucket.org/kreakozeablik/pinextractor'
      ''
      #1048#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077' '#1082#1086#1084#1087#1086#1085#1077#1085#1090#1099':'
      '************************'
      ''
      '- SynEdit'
      '  (https://github.com/SynEdit/SynEdit)'
      '- Toolbar2000'
      '  (https://github.com/plashenkov/Toolbar2000)'
      '- TBX'
      '  (https://github.com/plashenkov/TBX)'
      '- Virtual-TreeView'
      '  (https://github.com/JAM-Software/Virtual-TreeView)'
      ''
      #1048#1082#1086#1085#1082#1080':'
      '*******'
      ''
      '- IconArchive'
      '  (https://iconarchive.com/)'
      ''
      #1054#1090#1076#1077#1083#1100#1085#1086#1077' '#1089#1087#1072#1089#1080#1073#1086':'
      '******************'
      ''
      '- Martin Panter (vadmium)'
      '  (https://github.com/vadmium/python-altium)'
      '- '#1040#1083#1077#1082#1089#1072#1085#1076#1088' '#1041#1072#1075#1077#1083#1100' (Rouse_)'
      
        '  (http://alexander-bagel.blogspot.com/2015/03/compound-file.htm' +
        'l)'
      ''
      #1048#1089#1090#1086#1088#1080#1103':'
      '********'
      ''
      '* v1.0.0 '#1086#1090' 15.01.2021: '#1055#1077#1088#1074#1099#1081' '#1088#1077#1083#1080#1079'.')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
end
