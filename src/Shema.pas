unit Shema;

interface

uses System.Generics.Collections, Classes;

type
  TNet = class;
  TPin = class;
  TComp = class;
  TShema = class;

  TPin = class(TObject)
  private
    FNet: TNet;
    FComp: TComp;

    function GetNetName: string;
  public
    Pin: string;
    Designator: string;
    Sheet: string;
    Part: string;

    destructor Destroy(); override;

    procedure Connect(net: TNet);

    property Net: TNet read FNet;
    property Comp: TComp read FComp;
    property NetName: string read GetNetName;
  end;

  TPinList = class(System.Generics.Collections.TObjectList<TPin>);

  TNet = class(TObject)
  private
    FPins: TPinList;
    FOriginal: string;
  public
    constructor Create(OriginalName: string); overload;
    constructor Create(src: TNet); overload;
    destructor Destroy(); override;

    property Pins: TPinList read FPins;
    property Name: string read FOriginal;
  end;

  TNetList = class(System.Generics.Collections.TObjectList<TNet>);

  TComp = class(TObject)
  private
    FName: string;
    FLibRef: string;
    FDesignator: string;
    FDescription: string;
    FPins: TPinList;
    FShema: TShema;
  public
    constructor Create(vShema: TShema; vName: string; vLibRef: string; vDesignator: string; vDescription: string); overload;
    constructor Create(vShema: TShema; src: TComp); overload;
    destructor Destroy(); override;

    property Shema: TShema read FShema;
    property Name: string read FName;
    property LibRef: string read FLibRef;
    property Designator: string read FDesignator;
    property Description: string read FDescription;
    property Pins: TPinList read FPins;
  end;
  TCompList = class(System.Generics.Collections.TObjectList<TComp>);

  TShema = class(TObject)
  private
    FFileName: string;
    FComps: TCompList;
    FNets: TNetList;
    function GetPrjName: string;
  protected
    function GetNet(name: string): TNet;
  public
    constructor Create();
    destructor Destroy(); override;

    function GetEnumerator: TEnumerator<TComp>; inline;

    procedure Assign(src: TShema);
    procedure Swap(src: TShema);
    procedure LoadFromFile(fn: string);
    procedure Clear;

    property FileName: string read FFileName;
    property Comps: TCompList read FComps;
    property Nets: TNetList read FNets;
    property PrjName: string read GetPrjName;

    function GetComp(const name: string): TComp;
  end;

implementation

uses AltiumProject, AltiumComponent, System.Generics.Defaults, strutil,
  System.IOUtils, System.SysUtils;

type
TPinNameComparer = class(TComparer<TComp>)
public
    function Compare(const Left, Right: TComp): Integer; override;
end;

function TPinNameComparer.Compare(const Left, Right: TComp): Integer;
begin
  result := StringSortCompare(Left.Name, Right.Name);
end;

{ TCompList }

procedure TShema.Assign(src: TShema);
var
  comp: TComp;
begin
  if (self <> src) then
  begin
    FFileName := src.FFileName;
    Clear();

    for comp in src.Comps do
      FComps.Add(TComp.Create(self, comp));
  end;
end;

procedure TShema.Clear;
begin
  FComps.Clear;
  FNets.Clear;
end;

constructor TShema.Create;
begin
  FComps := TCompList.Create(True);
  FNets := TNetList.Create(True);
end;

destructor TShema.Destroy;
begin
  FComps.Free;
  FNets.Free;
  inherited;
end;

function TShema.GetComp(const name: string): TComp;
var
  c: TComp;
begin
  for c in FComps do
    if c.Name = name then
    begin
      result := c;
      exit;
    end;
  result := nil;
end;

function TShema.GetEnumerator: TEnumerator<TComp>;
begin
  result := FComps.GetEnumerator();
end;

function TShema.GetNet(name: string): TNet;
begin
  for result in FNets do
    if result.FOriginal = name then
      exit;

  result := TNet.Create(name);
  FNets.Add(result);
end;

function TShema.GetPrjName: string;
begin
  result := TPath.GetFileNameWithoutExtension(FFileName);
end;

procedure TShema.LoadFromFile(fn: string);
var
  AltiumPrj: TAltiumProject;
  AltComp: TAltiumComponent;
  comp: TComp;
  pin: TAltiumPin;
  npin: TPin;
  net: TNet;
begin
  FFileName := fn;

  Clear;
  AltiumPrj := TAltiumProject.Create(fn);

  for AltComp in AltiumPrj.Components do
  begin
    comp := TComp.Create(self, AltComp.Name, AltComp.LibRef, AltComp.PartNum, AltComp.Description);
    comp.Pins.Capacity := AltComp.Count;

    for pin in AltComp do
    begin
      npin := TPin.Create;
      npin.FComp := comp;
      npin.Pin := pin.Name;
      npin.Designator := pin.Designator;
      npin.Sheet := pin.OwnerPart.OwnerSheet.Name;
      npin.Part := pin.OwnerPart.PartName;
      comp.Pins.Add(npin);

      net := GetNet(pin.NetName);
      if Assigned(net) then
        npin.Connect(net);
    end;

    FComps.Add(comp);
  end;

  AltiumPrj.Free;

  FComps.Sort(TPinNameComparer.Create());
end;

procedure TShema.Swap(src: TShema);
var
  tmpn: TNetList;
  tmpc: TCompList;
  tmps: string;
  comp: TComp;
begin
  if (self <> src) then
  begin
    tmps := src.FFileName;
    src.FFileName := FFileName;
    FFileName := tmps;

    tmpn := src.FNets;
    src.FNets := FNets;
    FNets := tmpn;

    tmpc := src.FComps;
    src.FComps := FComps;
    FComps := tmpc;

    for comp in src.Comps do
      comp.FShema := src;

    for comp in Comps do
      comp.FShema := self;
  end;
end;

{ TComp }

constructor TComp.Create(vShema: TShema; src: TComp);
var
  srcpin: TPin;
  pin: TPin;
begin
  FShema := vShema;
  FName := src.FName;
  FLibRef := src.FLibRef;
  FDesignator := src.FDesignator;
  FDescription := src.FDescription;

  FPins := TPinList.Create(True);

  for srcpin in src.FPins do
  begin
    pin := TPin.Create();

    pin.FComp := self;
    pin.Pin := srcpin.Pin;
    pin.Designator := srcpin.Designator;
    pin.Sheet := srcpin.Sheet;
    pin.Part := srcpin.Part;

    pin.Connect(vShema.GetNet(srcpin.NetName));

    FPins.Add(pin);
  end;
end;

constructor TComp.Create(vShema: TShema; vName, vLibRef, vDesignator, vDescription: string);
begin
  FShema := vShema;
  FName := vName;
  FLibRef := vLibRef;
  FDesignator := vDesignator;
  FDescription := vDescription;

  FPins := TPinList.Create(True);
end;

destructor TComp.Destroy;
begin
  FPins.Free;
  inherited;
end;

{ TPin }

procedure TPin.Connect(net: TNet);
begin
  FNet := net;
  FNet.FPins.Add(self);
end;

destructor TPin.Destroy;
begin
  if Assigned(FNet) then
    FNet.FPins.Remove(self);
  inherited;
end;

function TPin.GetNetName: string;
begin
  if Assigned(FNet) then
    result := FNet.Name
  else
    result := '';
end;

{ TNet }

constructor TNet.Create(OriginalName: string);
begin
  FPins := TPinList.Create(False);
  FOriginal := OriginalName;
end;

constructor TNet.Create(src: TNet);
begin
  FPins := TPinList.Create(False);
  FOriginal := src.FOriginal;
end;

destructor TNet.Destroy;
var
  p: TPin;
begin
  for p in Pins do
    p.FNet := Nil;
  FPins.Free;
  inherited;
end;

end.
