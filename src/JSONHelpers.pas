unit JSONHelpers;

// ����������� ������ Delphi
{$I ..\External\SynEdit\Source\SynEdit.inc}

interface

uses System.JSON, System.Generics.Collections, Classes;

type
  TJSONSerializeble = class(TObject)
  public
    procedure Load(data: TJSONObject); virtual; abstract;
    function Save(): TJSONObject; virtual; abstract;
  end;

  TJSONObjHelper = class helper for TJSONObject
    procedure FileWrite(const fn: string);

    procedure SetPair(const pname: string; const Val: TJSONValue);

    procedure hWrite(const pname: string; const sl: TStrings); overload;
    procedure hWrite(const pname: string; value: TJSONObject); overload;
    procedure hWrite(const pname: string; const  value: Boolean); overload;
    procedure hWrite(const pname: string; const value: Integer); overload;
    procedure hWrite(const pname: string; const value: string); overload;
    procedure hWrite<T: constructor, TJSONSerializeble>(const pname: string; obj: T); overload;
    procedure hWrite<T: constructor, TJSONSerializeble>(const pname: string; lst: System.Generics.Collections.TObjectList<T>); overload;

    function hReadBool(const pname: string; const  default: Boolean = False): Boolean;
    function hReadInt(const pname: string; const default: Integer = 0): Integer;
    function hReadStr(const pname: string; const default: string = ''): string;
    procedure hRead(const pname: string; sl: TStrings); overload;
    procedure hRead<T: constructor, TJSONSerializeble>(const pname: string; obj: T); overload;
    procedure hRead<T: constructor, TJSONSerializeble>(const pname: string; lst: System.Generics.Collections.TObjectList<T>); overload;
  end;

function JSONFileRead(const fn: string): TJSONObject;

implementation

function JSONFileRead(const fn: string): TJSONObject;
var
  sl: TStringList;
begin
    sl := TStringList.Create;
    try
      sl.LoadFromFile(fn);
      Result := (TJSONObject.ParseJSONValue(sl.Text) as TJSONObject);
    finally
      sl.Free;
    end;
end;

{ TJSONObjHelper }

procedure TJSONObjHelper.hRead(const pname: string; sl: TStrings);
var
  s: string;
  a: TJSONArray;
  jo: TJSONValue;
begin
  sl.Clear;
  a := (GetValue(pname) as TJSONArray);
  if Assigned(a) then
    for jo in a do
    begin
      s := jo.Value;
      sl.Add(s);
    end;
end;

procedure TJSONObjHelper.FileWrite(const fn: string);
var
  sl: TStringList;
begin
  sl := TStringList.Create;

  {$IFDEF SYN_DELPHI_10_3_UP}
    sl.Text := Format();
  {$ELSE}
    sl.Text := ToJSON();
  {$ENDIF}

  try
    sl.SaveToFile(fn);
  finally
    sl.Free;
  end;
end;

function TJSONObjHelper.hReadBool(const pname: string; const default: Boolean): Boolean;
var
  v: TJSONValue;
begin
  v := GetValue(pname);
  if Assigned(v) then
    result := (v as TJSONBool).AsBoolean
  else
    result := default;
end;

function TJSONObjHelper.hReadInt(const pname: string; const default: Integer): Integer;
var
  v: TJSONValue;
begin
  v := GetValue(pname);
  if Assigned(v) then
    result := (v as TJSONNumber).AsInt
  else
    result := default;
end;

procedure TJSONObjHelper.hRead<T>(const pname: string; obj: T);
begin
  obj.Load(GetValue(pname) as TJSONObject);
end;

procedure TJSONObjHelper.hRead<T>(const pname: string; lst: System.Generics.Collections.TObjectList<T>);
var
  a: TJSONArray;
  g: T;
  jo: TJSONValue;
begin
  lst.Clear;

  a := (GetValue(pname) as TJSONArray);
  if Assigned(a) then
    for jo in a do
    begin
      g := T.Create();
      g.Load(jo as TJSONObject);
      lst.Add(g);
    end;
end;

function TJSONObjHelper.hReadStr(const pname: string; const default: string): string;
var
  v: TJSONValue;
begin
  v := GetValue(pname);
  if Assigned(v) then
    result := (v as TJSONString).Value
  else
    result := default;
end;

procedure TJSONObjHelper.hWrite(const pname: string; const value: Boolean);
begin
  SetPair(pname, TJSONBool.Create(value));
end;

procedure TJSONObjHelper.hWrite(const pname: string; const value: Integer);
begin
  SetPair(pname, TJSONNumber.Create(value));
end;

procedure TJSONObjHelper.hWrite(const pname, value: string);
begin
  SetPair(pname, TJSONString.Create(value));
end;

procedure TJSONObjHelper.hWrite<T>(const pname: string; obj: T);
begin
  SetPair(pname, obj.Save());
end;

procedure TJSONObjHelper.hWrite<T>(const pname: string; lst: System.Generics.Collections.TObjectList<T>);
var
  a: TJSONArray;
  v: T;
  jo: TJSONValue;
begin
  a := TJSONArray.Create();
  for v in lst do
    a.AddElement(v.Save());
  SetPair(pname, a);
end;

procedure TJSONObjHelper.SetPair(const pname: string; const Val: TJSONValue);
var
  p: TJSONPair;
begin
  p := Get(pname);
  if Assigned(p) then
    p.JsonValue := Val
  else
    AddPair(pname, Val);
end;

procedure TJSONObjHelper.hWrite(const pname: string; value: TJSONObject);
begin
  SetPair(pname, value);
end;

procedure TJSONObjHelper.hWrite(const pname: string; const sl: TStrings);
var
  a: TJSONArray;
  s: String;
begin
  a := TJSONArray.Create;
  for s in sl do
    a.Add(s);
  SetPair(pname, a);
end;

end.
