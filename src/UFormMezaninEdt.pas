unit UFormMezaninEdt;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, TB2Dock, TB2Toolbar, TBX,
  Vcl.StdCtrls, VirtualTrees, PinDoc, System.Actions, Vcl.ActnList, Vcl.Menus,
  TB2Item, Shema;

type
  TFormMezaninEdt = class(TForm)
    TBXToolbar1: TTBXToolbar;
    Panel2: TPanel;
    btnCancel: TButton;
    Button2: TButton;
    VST: TVirtualStringTree;
    ActionList1: TActionList;
    PopupMenu1: TPopupMenu;
    actNewMezanin: TAction;
    TBXItem1: TTBXItem;
    actDelete: TAction;
    TBXItem2: TTBXItem;
    actNewConnection: TAction;
    TBXItem3: TTBXItem;
    TBXSeparatorItem1: TTBXSeparatorItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    actEdit: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure actNewMezaninExecute(Sender: TObject);
    procedure VSTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure actDeleteExecute(Sender: TObject);
    procedure VSTEditing(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; var Allowed: Boolean);
    procedure VSTNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; NewText: string);
    procedure VSTPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType);
    procedure VSTNodeDblClick(Sender: TBaseVirtualTree;
      const HitInfo: THitInfo);
    procedure actNewConnectionExecute(Sender: TObject);
    procedure actEditExecute(Sender: TObject);
  private
    FMezanins: TMezaninList;
    FDoc: TPinDoc;

    function SchNameExists(sn: string): Boolean;

    procedure AddConnection(Node: PVirtualNode);
    procedure EditNode();

    function ValidateMain(c: TComp): Boolean;
  public
    property Mezanins: TMezaninList read FMezanins;

    procedure Init(Doc: TPinDoc);
    function AddMezanin(shemaName: string): TMezanin;

  end;

var
  FormMezaninEdt: TFormMezaninEdt;

implementation

uses System.UITypes, UFormMezaninConnection, UFormWait, UdmCommonData;

{$R *.dfm}

{ TMezaninConnectionValidator }
type
  TMezaninConnectionValidator = class
  private
    FMezanin: TMezanin;
  public
    constructor Create(Mezanin: TMezanin);
    function ValidateMezanin(c: TComp): Boolean;
  end;

constructor TMezaninConnectionValidator.Create(Mezanin: TMezanin);
begin
  FMezanin := Mezanin;
end;

function TMezaninConnectionValidator.ValidateMezanin(c: TComp): Boolean;
var
  mc: TMezaninConnection;
begin
  result := False;
  for mc in FMezanin.Connections do
    if mc.CompMezaninBoard = c then
      exit;
  result := True;
end;

{ TFormMezaninEdt }

procedure TFormMezaninEdt.actDeleteExecute(Sender: TObject);
var
  Node: PVirtualNode;
begin
  Node := VST.GetFirstSelected();
  if Assigned(Node) then
    if (Node.Parent = VST.RootNode) then
    begin
      if (MessageDlg('������� �������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
      begin
        FMezanins.Delete(Node.Index);
        VST.DeleteNode(Node);
      end;
    end else begin
      if (MessageDlg('������� �����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
      begin
        FMezanins[Node.Parent.Index].Connections.Delete(Node.Index);
        VST.DeleteNode(Node);
      end;
    end;
end;

procedure TFormMezaninEdt.actEditExecute(Sender: TObject);
begin
  EditNode;
end;

procedure TFormMezaninEdt.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
  f: Boolean;
begin
  f := VST.SelectedCount <> 0;

  actDelete.Enabled := f;
  actNewConnection.Enabled := f;
  actEdit.Enabled := f;
end;

procedure TFormMezaninEdt.actNewConnectionExecute(Sender: TObject);
var
  Node: PVirtualNode;
begin
  Node := VST.GetFirstSelected();
  if Assigned(Node) then
  begin
    if (Node.Parent <> VST.RootNode) then
      Node := Node.Parent;

    AddConnection(Node);
  end;
end;

procedure TFormMezaninEdt.actNewMezaninExecute(Sender: TObject);
begin
  while True do
  begin
    if dmCommonData.OpenDialog.Execute() then
    begin
      if not SchNameExists(dmCommonData.OpenDialog.FileName) then
      begin
        AddMezanin(dmCommonData.OpenDialog.FileName);
        VST.Invalidate;
        break;
      end else
        MessageDlg('������� ��� ��������!'#13'������� ������.', mtWarning, [mbOK], 0);
    end else
      break;
  end;
end;

procedure TFormMezaninEdt.AddConnection(Node: PVirtualNode);
var
  fmc: TFormMezaninConnection;
  newcon: TMezaninConnection;
  mcv: TMezaninConnectionValidator;
begin
    fmc := TFormMezaninConnection.Create(self);
    try
      fmc.OnValidateMain := ValidateMain;
      mcv := TMezaninConnectionValidator.Create(FMezanins[Node.Index]);
      try
        fmc.OnValidateMezanin := mcv.ValidateMezanin;
        fmc.Init(FDoc.Shema, FMezanins[Node.Index].Shema);
        if(fmc.ShowModal = mrOk) then
        begin
          newcon := TMezaninConnection.Create;
          newcon.Assign(fmc.Connection);
          newcon.Owner := FMezanins[Node.Index];
          FMezanins[Node.Index].Connections.Add(newcon);

          VST.AddChild(Node);
          VST.Expanded[Node] := True;
        end;
      finally
        mcv.Free;
      end;
    finally
      fmc.Free;
    end;
end;

function TFormMezaninEdt.AddMezanin(shemaName: string): TMezanin;
var
  Node: PVirtualNode;
  res: TMezanin;
begin
  res := TMezanin.Create;
  RunAsync(procedure()begin res.OpenShema(shemaName); end);
  FMezanins.Add(res);
  Node := VST.AddChild(VST.RootNode);
  AddConnection(Node);                        // TODO: ��������� !!!!
  result := res;
end;

procedure TFormMezaninEdt.Button2Click(Sender: TObject);
var
  t: TMezaninList;
begin
  t := FDoc.Mezanins;
  FDoc.SetMezanins(FMezanins);
  FMezanins := t;
  ModalResult := mrOk;
end;

procedure TFormMezaninEdt.EditNode;
var
  Node: PVirtualNode;

  procedure EditMezanin();
  var
    newSch: TShema;
  begin
    while True do
      begin
        if dmCommonData.OpenDialog.Execute() then
        begin
          if dmCommonData.OpenDialog.FileName = FMezanins[Node.Index].FileName then
            break;

          if not SchNameExists(dmCommonData.OpenDialog.FileName) then
          begin
            newSch := TShema.Create();
            try
              RunAsync(procedure()begin newSch.LoadFromFile(dmCommonData.OpenDialog.FileName); end);
              FMezanins[Node.Index].SwapShema(newSch);

              VST.Invalidate;
            finally
              newSch.Free;
            end;

            break;
          end else
            MessageDlg('������� ��� ��������!'#13'������� ������.', mtWarning, [mbOK], 0);
        end else
          break;
      end;
  end;

  procedure EditConnection();
  var
    fmc: TFormMezaninConnection;
    con: TMezaninConnection;
    mcv: TMezaninConnectionValidator;
  begin
    con := FMezanins[Node.Parent.Index].Connections[Node.Index];

    fmc := TFormMezaninConnection.Create(self);
    try
      mcv := TMezaninConnectionValidator.Create(FMezanins[Node.Parent.Index]);
      try
        fmc.OnValidateMain := ValidateMain;
        fmc.OnValidateMezanin := mcv.ValidateMezanin;
        fmc.Init(FDoc.Shema, FMezanins[Node.Parent.Index].Shema, con);

        if(fmc.ShowModal = mrOk) then
          con.Assign(fmc.Connection);

      finally
        mcv.Free;
      end;
    finally
      fmc.Free;
    end;
  end;

begin
  Node := VST.GetFirstSelected();

  if (Node.Parent = VST.RootNode) then
    EditMezanin()
  else
    EditConnection();
end;

procedure TFormMezaninEdt.FormCreate(Sender: TObject);
begin
  FMezanins := TMezaninList.Create(True);
end;

procedure TFormMezaninEdt.FormDestroy(Sender: TObject);
begin
  FMezanins.Free;
end;

procedure TFormMezaninEdt.Init(Doc: TPinDoc);
var
  m: TMezanin;
  c: TMezaninConnection;
  mnode: PVirtualNode;
begin
  FDoc := Doc;
  CopyMezanins(Doc.Mezanins, FMezanins);

  for m in FMezanins do
  begin
    mnode := VST.AddChild(VST.RootNode);
    for c in m.Connections do
      VST.AddChild(mnode);
    VST.Expanded[mnode] := True;
  end;
end;

function TFormMezaninEdt.ValidateMain(c: TComp): Boolean;
var
  m: TMezanin;
  mc: TMezaninConnection;
begin
  result := False;
  for m in FMezanins do
    for mc in m.Connections do
      if mc.CompMainBoard = c then
        exit;

  result := True;
end;

procedure TFormMezaninEdt.VSTEditing(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := (Node.Parent = Sender.RootNode) and (Column = 1);
end;

procedure TFormMezaninEdt.VSTGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: string);

  function  FormatConnection(c: TMezaninConnection): string;
  begin
    result := c.CompMainBoardName + ' <-> ' + c.CompMezaninBoardName;
  end;
begin
  if (Node.Parent = Sender.RootNode) then
    case Column of
    0: CellText := FMezanins[Node.Index].FileName;
    1: CellText := FMezanins[Node.Index].NetPrefix;
    end
    else begin
    case Column of
    0: CellText := FormatConnection(FMezanins[Node.Parent.Index].Connections[Node.Index]);
    1: CellText := '';
    end
  end;
end;

procedure TFormMezaninEdt.VSTNewText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; NewText: string);
begin
  if ((Node.Parent = Sender.RootNode) and (Column = 1)) then
    FMezanins[Node.Index].NetPrefix := NewText;
end;

function TFormMezaninEdt.SchNameExists(sn: string): Boolean;
var
  m: TMezanin;
begin
  for m in FMezanins do
    if (m.Shema.FileName = sn) then
    begin
      result := True;
      exit;
    end;
  result := False;
end;

procedure TFormMezaninEdt.VSTNodeDblClick(Sender: TBaseVirtualTree; const HitInfo: THitInfo);
begin
  if HitInfo.HitColumn = 0 then
    EditNode;
end;

procedure TFormMezaninEdt.VSTPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
var
  Valid: Boolean;
begin
  if (Node.Parent = Sender.RootNode) then
    Valid := FMezanins[Node.Index].Valid
  else
    Valid := FMezanins[Node.Parent.Index].Connections[Node.Index].Valid;

  if Valid then
    TargetCanvas.Font.Color := clWindowText
  else
    TargetCanvas.Font.Color := clRed;
end;

end.
