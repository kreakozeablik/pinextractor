unit AppConfig;

interface

uses Classes, JSON;

const
  CONFIG_RECENT_PRJ_LST = 'recent_projects_lst';
  CONFIG_OPTIONS = 'DocOptions';

type

  TConfigObj = class(TObject)
    FJsonObj: TJSONObject;
    FChanged: Boolean;
  public
    constructor Create();
    destructor Destroy(); override;

    procedure Save(const fname: string);
    procedure Load(const fname: string);

    procedure ReadStrings(pname: string; sl: TStrings);
    function ReadString(pname: string; default: string = ''): string;
    function ReadBool(pname: string; default: Boolean = False): Boolean;
    function ReadInteger(pname: string; default: Integer = 0): Integer;
    function GetJSONObj(pname: string): TJSONValue;

    procedure WriteStrings(pname: string; sl: TStrings);
    procedure WriteString(pname: string; value: string);
    procedure WriteBool(pname: string; value: Boolean);
    procedure WriteInteger(pname: string; value: Integer);
    procedure WriteJSONObj(pname: string; value: TJSONValue);
  end;

var
  Config: TConfigObj;

procedure SaveConfig();
procedure LoadConfig();

implementation

uses IniFiles, SysUtils, JSONHelpers;

const
  INI_FILE_NAME = '.\pinextractor.json';

procedure SaveConfig();
begin
  Config.Save(INI_FILE_NAME);
end;

procedure LoadConfig();
begin
  Config.Load(INI_FILE_NAME);
end;

{ TConfigObj }

constructor TConfigObj.Create;
begin
  FJsonObj := nil;
end;

destructor TConfigObj.Destroy;
begin
  FreeAndNil(FJsonObj);
  inherited;
end;

function TConfigObj.GetJSONObj(pname: string): TJSONValue;
begin
  result := FJsonObj.GetValue(pname);
end;

procedure TConfigObj.Load(const fname: string);
begin
  FChanged := False;

  if FileExists(fname) then
  begin
    FJsonObj := JSONFileRead(fname);
  end else
    FJsonObj := TJSONObject.Create;
end;

function TConfigObj.ReadBool(pname: string; default: Boolean): Boolean;
begin
  result := FJsonObj.hReadBool(pname, default)
end;

function TConfigObj.ReadInteger(pname: string; default: Integer): Integer;
begin
  result := FJsonObj.hReadInt(pname, default)
end;

function TConfigObj.ReadString(pname, default: string): string;
begin
  result := FJsonObj.hReadStr(pname, default)
end;

procedure TConfigObj.ReadStrings(pname: string; sl: TStrings);
begin
  FJsonObj.hRead(pname, sl);
end;

procedure TConfigObj.Save(const fname: string);
begin
  if(FChanged) then
  begin
    FJsonObj.FileWrite(fname);
    FChanged := False;
  end;
end;

procedure TConfigObj.WriteBool(pname: string; value: Boolean);
begin
  FChanged := True;
  FJsonObj.hWrite(pname, value);
end;

procedure TConfigObj.WriteInteger(pname: string; value: Integer);
begin
  FChanged := True;
  FJsonObj.hWrite(pname, value);
end;

procedure TConfigObj.WriteJSONObj(pname: string; value: TJSONValue);
var
  p: TJSONPair;
begin
  p := FJsonObj.Get(pname);
  if Assigned(p) then
    p.JsonValue := value
  else
    FJsonObj.AddPair(pname, value);

  FChanged := True;
end;

procedure TConfigObj.WriteString(pname, value: string);
begin
  FChanged := True;
  FJsonObj.hWrite(pname, value);
end;

procedure TConfigObj.WriteStrings(pname: string; sl: TStrings);
begin
  FChanged := True;
  FJsonObj.hWrite(pname, sl);
end;

initialization
  Config := TConfigObj.Create;

finalization
  Config.Free;
end.
