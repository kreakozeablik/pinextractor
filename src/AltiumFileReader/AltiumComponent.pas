unit AltiumComponent;

interface

uses System.Generics.Collections, System.Types, AltiumRecordEnum;

type
  TAltiumSheetBase = class(TObject)
  private
    FName: string;
  public
    constructor Create(sname: string);

    property Name: string read FName;
  end;

  TAltiumPin = class;
  TAltiumComponentPart = class;

  TAltiumPinList = class(System.Generics.Collections.TObjectList<TAltiumPin>);
  TAltiumComponentPartList = class(System.Generics.Collections.TObjectList<TAltiumComponentPart>);

  TAltiumComponentPart = class
    FCompName: string;
    FIndex: Integer;
    FLibRef: string;
    FCurrentPartID: Integer;
    FPins: TAltiumPinList;
    FPartNum: string;
    FDescription: string;
    FPartCnt: Integer;
    FOwner: TAltiumSheetBase;
  private
    function GetPartName: string;
  public
    constructor Create(owner: TAltiumSheetBase; ind: Integer; rec: TAltiumRecordEnum);
    destructor Destroy(); override;

    property CompName: string read FCompName write FCompName;
    property Index: Integer read FIndex;
    property LibRef: string read FLibref;
    property CurrentPartID: Integer read FCurrentPartID;
    property Pins: TAltiumPinList read FPins;
    property PartNum: string read FPartNum write FPartNum;
    property PartName: string read GetPartName;
    property Description: string read FDescription;
    property OwnerSheet: TAltiumSheetBase read FOwner;
  end;

  TAltiumPin = class
  private
    FConPoint: TPoint;
    FName: string;
    FDesignator: string;
    FNetName: string;
    FOwner: TAltiumComponentPart;
  public
    constructor Create(owner: TAltiumComponentPart; rec: TAltiumRecordEnum);
    property ConPoint: TPoint read FConPoint;
    property Name: string read FName;
    property Designator: string read FDesignator;
    property NetName: string read FNetName write FNetName;
    property OwnerPart: TAltiumComponentPart read FOwner;
  end;

    TAltiumWire = class(TObject)
  protected
    FPoints: array of TPoint;
  public
    function Connected(p: TPoint): Boolean; overload;
    function Connected(w: TAltiumWire): Boolean; overload;
    constructor Create(rec: TAltiumRecordEnum);
  end;

  TAltiumNet = class(System.Generics.Collections.TObjectList<TAltiumWire>)
  private
    FName: string;
  public
    property Name: string read FName write FName;
    function Connected(p: TPoint): Boolean; overload;
    function Connected(w: TAltiumWire): Boolean; overload;
  end;

  TAltiumNetList = class(System.Generics.Collections.TObjectList<TAltiumNet>);

  TAltiumNetLabel = class(TObject)
  private
    FName: string;
    FPoint: TPoint;
  public
    property Name: string read FName;
    property Point: TPoint read FPoint;
    constructor Create(name: string; ipoint: TPoint);
  end;

  TLabelList = class(System.Generics.Collections.TObjectList<TAltiumNetLabel>);

  TAltiumComponent = class;

  TAltiumPinIterator = class(TObject)
    FComponent: TAltiumComponent;
    FPartInd: Integer;
    FPinInd: Integer;
    function NextPart(): Boolean;
  private
    function GetCurrent: TAltiumPin;
  public
    constructor Create(comp: TAltiumComponent);

    function MoveNext: Boolean;
    property Current: TAltiumPin read GetCurrent;
  end;

  TAltiumComponent = class(TObject)
  private
    FParts: TAltiumComponentPartList;
    function GetDescription: string;
    function GetLibref: string;
    function GetName: string;
    function GetPartNum: string;
    function GetCount: Integer;
  public
    property Parts: TAltiumComponentPartList read FParts;

    constructor Create();
    destructor Destroy(); override;

    property Name: string read GetName;
    property PartNum: string read GetPartNum;
    property LibRef: string read GetLibref;
    property Description: string read GetDescription;

    function GetEnumerator: TAltiumPinIterator;
    property Count: Integer read GetCount;
  end;

  TAltiumComponentList = class(System.Generics.Collections.TObjectList<TAltiumComponent>);

implementation

uses System.SysUtils;

function PointOnLine(var p, l1, l2: TPoint): Boolean;
var
  ax, ay, bx, by: Integer;
begin
  if(l1.X = l2.X)then
    result := (l1.X = p.X)and
              ((p.Y >= l1.Y) and (p.Y <= l2.Y) or
               (p.Y >= l2.Y) and (p.Y <= l1.Y))

  else if(l1.Y = l2.Y)then
    result := (l1.Y = p.Y)and
              ((p.X >= l1.X) and (p.X <= l2.X) or
               (p.X >= l2.X) and (p.X <= l1.X))
  else
   begin
    ax := p.X - l1.X;
    ay := p.Y - l1.Y;
    bx := l2.X - l1.X;
    by := l2.Y - l1.Y;

    result := ((ax*by - bx*ay) = 0) and
              ((p.X >= l1.X) and (p.X <= l2.X) or
               (p.X >= l2.X) and (p.X <= l1.X));
  end;
end;

{ TAltiumComponentPart }

constructor TAltiumComponentPart.Create(owner: TAltiumSheetBase; ind: Integer; rec: TAltiumRecordEnum);
begin
  FOwner := owner;
  FPins := TAltiumPinList.Create(False);
  FIndex := ind;
  FCurrentPartID := rec.PropI['CURRENTPARTID'];
  FLibRef := rec.PropS['LIBREFERENCE'];
  FDescription := rec.PropS['COMPONENTDESCRIPTION'];
  FPartCnt := rec.PropI['PARTCOUNT'];
end;

destructor TAltiumComponentPart.Destroy;
begin
  FreeAndNil(FPins);
  inherited;
end;

function TAltiumComponentPart.GetPartName: string;
var
  i: Integer;
  s: string;
begin
  Result := FCompName;
  if FPartCnt > 2 then
  begin
    i := FCurrentPartID - 1;
    s := Chr(Ord('A') + (i mod 26)) + s;
    i := i div 26;

    while i > 0 do
    begin
      s := Chr(Ord('@') + (i mod 25)) + s;
      i := i div 25;
    end;
    Result := Result + s;
  end;
end;

{ TAltiumPin }

constructor TAltiumPin.Create(owner: TAltiumComponentPart; rec: TAltiumRecordEnum);
var
  d: Integer;
begin
  FOwner := owner;
  FConPoint.X := rec.PropI['LOCATION.X'] * 100000 + rec.PropI['LOCATION.X_FRAC'];
  FConPoint.Y := rec.PropI['LOCATION.Y'] * 100000 + rec.PropI['LOCATION.Y_FRAC'];
  d := rec.PropI['PINLENGTH'] * 100000 + rec.PropI['PINLENGTH_FRAC'];
  case (rec.PropI['PINCONGLOMERATE'] and $3) of
  0: FConPoint.X := FConPoint.X + d;
  1: FConPoint.Y := FConPoint.Y + d;
  2: FConPoint.X := FConPoint.X - d;
  3: FConPoint.Y := FConPoint.Y - d;
  end;
  FDesignator := rec.PropS['DESIGNATOR'];
  FName := rec.PropS['NAME'];
end;

{ TAltiumNetLabel }

constructor TAltiumNetLabel.Create(name: string; ipoint: TPoint);
begin
  FName := name;
  FPoint := ipoint;
end;

{ TAltiumWire }

function TAltiumWire.Connected(p: TPoint): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 1 to High(FPoints) do
    if(PointOnLine(p, FPoints[i-1], FPoints[i]))then
    begin
      Result := True;
      exit;
    end;
end;

function TAltiumWire.Connected(w: TAltiumWire): Boolean;
begin
  result := Connected(w.FPoints[0]) or
            Connected(w.FPoints[High(w.FPoints)]) or
            w.Connected(FPoints[0]) or
            w.Connected(FPoints[High(FPoints)]);
end;

constructor TAltiumWire.Create(rec: TAltiumRecordEnum);
var
  i: Integer;
  locationcount: Integer;
begin
  locationcount := rec.PropI['LOCATIONCOUNT'];
  SetLength(FPoints, locationcount);
  for i := 1 to locationcount do
  begin
    FPoints[i-1].X := rec.PropI['X'+IntToStr(i)] * 100000 + rec.PropI['X'+IntToStr(i)+'_FRAC'];
    FPoints[i-1].Y := rec.PropI['Y'+IntToStr(i)] * 100000 + rec.PropI['Y'+IntToStr(i)+'_FRAC'];
  end;
end;

{ TAltiumNet }

function TAltiumNet.Connected(p: TPoint): Boolean;
var
  wire: TAltiumWire;
begin
  Result := False;
  for wire in self do
    if(wire.Connected(p))then
    begin
      Result := True;
      exit;
    end;
end;

function TAltiumNet.Connected(w: TAltiumWire): Boolean;
var
  wire: TAltiumWire;
begin
  Result := False;
  for wire in self do
    if(wire.Connected(w))then
    begin
      Result := True;
      exit;
    end;
end;

{ TAltiumComponent }

constructor TAltiumComponent.Create;
begin
  FParts := TAltiumComponentPartList.Create(False);
end;

destructor TAltiumComponent.Destroy;
begin
  FParts.Free;
  inherited;
end;

function TAltiumComponent.GetCount: Integer;
var
  p: TAltiumComponentPart;
begin
  Result := 0;

  for p in FParts do
    Result := Result + p.Pins.Count;
end;

function TAltiumComponent.GetDescription: string;
begin
  Result := FParts[0].Description;
end;

function TAltiumComponent.GetEnumerator: TAltiumPinIterator;
begin
  Result := TAltiumPinIterator.Create(self);
end;

function TAltiumComponent.GetLibref: string;
begin
  Result := FParts[0].Libref;
end;

function TAltiumComponent.GetName: string;
begin
  Result := FParts[0].CompName;
end;

function TAltiumComponent.GetPartNum: string;
begin
  Result := FParts[0].PartNum;
end;

{ TAltiumSheetBase }

constructor TAltiumSheetBase.Create(sname: string);
begin
  FName := sname;
end;

{ TAltiumPinIterator }

constructor TAltiumPinIterator.Create(comp: TAltiumComponent);
begin
  FComponent := comp;
  FPartInd := -1;
  FPinInd := -1;
end;

function TAltiumPinIterator.GetCurrent: TAltiumPin;
begin
  Result := FComponent.Parts[FPartInd].Pins[FPinInd];
end;

function TAltiumPinIterator.MoveNext: Boolean;
begin
  if FPartInd < 0 then
  begin
    Result := NextPart;
  end else begin
    Inc(FPinInd);
    Result := FComponent.Parts[FPartInd].Pins.Count > FPinInd;
    if not Result then
      Result := NextPart;
  end;
end;

function TAltiumPinIterator.NextPart: Boolean;
begin
  Result := false;
  FPinInd := 0;
  repeat
    Inc(FPartInd);
    if FComponent.Parts.Count <= FPartInd then
      exit;
    Result := FComponent.Parts[FPartInd].Pins.Count > 0;
  until Result;
end;

end.
