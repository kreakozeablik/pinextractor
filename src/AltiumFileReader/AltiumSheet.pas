unit AltiumSheet;

interface

uses AltiumRecordEnum, AltiumComponent, System.Types;

type
  TAltiumSheet = class(TAltiumSheetBase)
  private
    FParts: TAltiumComponentPartList;

    procedure SetPartName(rec: TAltiumRecordEnum);
    procedure SetPartNum(rec: TAltiumRecordEnum);
    procedure ReadPin(rec: TAltiumRecordEnum);
  public
    property Parts: TAltiumComponentPartList read FParts;

    constructor Create(sname: string);
    destructor Destroy; override;
  end;

implementation

uses System.IOUtils, System.SysUtils;

{ TAltiumSheet }

constructor TAltiumSheet.Create(sname: string);
var
  rec: TAltiumRecordEnum;
  rind: Integer;
  NetList: TAltiumNetList;
  LabelList: TLabelList;

  procedure AddWire(rec: TAltiumRecordEnum);
  var
    wire: TAltiumWire;
    net: TAltiumNet;
    net1: TAltiumNet;
    i: Integer;
    j: Integer;
    len: Integer;
  begin
    wire := TAltiumWire.Create(rec);

    // find net
    for i := 0 to NetList.Count-1 do
    begin
      net := NetList[i];
      if(net.Connected(wire))then
      begin
        net.Add(wire);

        // combine nets
        len := NetList.Count;
        j := i + 1;
        while j < len do
        begin
          net1 := NetList[j];
          if(net1.Connected(wire))then
          begin
            net.AddRange(net1);

            net1.OwnsObjects := false;
            NetList.Delete(j);

            len := NetList.Count;
          end else
            Inc(j);
        end;

        exit;
      end;
    end;

    // new net
    net := TAltiumNet.Create(True);
    net.Add(wire);
    NetList.Add(net);
  end;

  procedure AddLabel(rec: TAltiumRecordEnum);
  var
    p: TPoint;
    lbl: string;
  begin
    lbl := rec.PropS['TEXT'];
    p.X := rec.PropI['LOCATION.X'] * 100000 + rec.PropI['LOCATION.X_FRAC'];
    p.Y := rec.PropI['LOCATION.Y'] * 100000 + rec.PropI['LOCATION.Y_FRAC'];
    LabelList.Add(TAltiumNetLabel.Create(lbl, p));
  end;

  procedure AssignNetName();
  var
    net: TAltiumNet;
    lbl: TAltiumNetLabel;
    prt: TAltiumComponentPart;
    pin: TAltiumPin;
    nname: string;
    tmp_name: string;
    flg: Boolean;
  begin
    for net in NetList do
    begin
      flg := False;
      for lbl in LabelList do
        if net.Connected(lbl.Point) then
        begin
          net.Name := lbl.Name;
          flg := True;
        end;

      if not flg then
      begin
        nname := #255;

        for prt in FParts do
          for pin in prt.Pins do
            if net.Connected(pin.ConPoint) then
            begin
              tmp_name := prt.CompName + '_' + pin.Designator;
              if tmp_name < nname then
                nname := tmp_name;
            end;

        net.Name := 'Net' + nname;
      end;
    end;

    for prt in FParts do
      for pin in prt.Pins do
        for net in NetList do
          if net.Connected(pin.ConPoint) then
            pin.NetName := net.Name;
  end;

begin
  inherited Create(TPath.GetFileNameWithoutExtension(sname));

  NetList := TAltiumNetList.Create(True);
  LabelList := TLabelList.Create(True);
  FParts := TAltiumComponentPartList.Create(True);

  rind := 0;
  for rec in TAltiumRecordEnum.Create(sname) do
  begin
    case rec.PropI['RECORD'] of
      1: FParts.Add(TAltiumComponentPart.Create(self, rind, rec));
      2: ReadPin(rec);
      17: AddLabel(rec);
      25: AddLabel(rec);
      27: AddWire(rec);
      34: SetPartName(rec);
      41: SetPartNum(rec);
    end;

    Inc(rind);
  end;

  AssignNetName;

  NetList.Free;
  LabelList.Free;
end;

destructor TAltiumSheet.Destroy;
begin
  FParts.Free;
  inherited;
end;

procedure TAltiumSheet.ReadPin(rec: TAltiumRecordEnum);
var
  p: TAltiumComponentPart;
  owner: Integer;
  ownerpartid: Integer;
begin
  owner := rec.PropI['OWNERINDEX'];
  ownerpartid := rec.PropI['OWNERPARTID'];

  for p in FParts do
    if((p.Index = owner) and (p.CurrentPartID = ownerpartid))then
    begin
      p.Pins.Add(TAltiumPin.Create(p, rec));
      break;
    end;
end;

procedure TAltiumSheet.SetPartName(rec: TAltiumRecordEnum);
var
  p: TAltiumComponentPart;
  owner: Integer;
begin
  if (rec.PropS['NAME'] <> 'Designator') then
    exit;

  owner := rec.PropI['OWNERINDEX'];

  for p in FParts do
    if(p.Index = owner)then
    begin
      p.CompName := rec.PropS['TEXT'];
      break;
    end;
end;

procedure TAltiumSheet.SetPartNum(rec: TAltiumRecordEnum);
var
  p: TAltiumComponentPart;
  owner: Integer;
begin
  if (rec.PropS['NAME'] <> 'Part Number') then
    exit;

  owner := rec.PropI['OWNERINDEX'];

  for p in FParts do
    if(p.Index = owner)then
    begin
      p.PartNum := rec.PropS['TEXT'];
      break;
    end;
end;

end.
