unit AltiumProject;

interface

uses AltiumSheet, AltiumComponent, Classes, System.Generics.Collections;

type
  TAltiumSheetList = class(TObjectList<TAltiumSheet>);

  TAltiumProject = class(TObject)
  protected
    FSheets: TAltiumSheetList;
    FComponents: TAltiumComponentList;
    FFileName: string;
    procedure LoadFromFile(fn: string);
  public
    property FileName: string read FFileName;
    property Sheets: TAltiumSheetList read FSheets;
    property Components: TAltiumComponentList read FComponents;
    constructor Create(fn: string);
    destructor Destroy; override;
  end;

implementation

uses System.IniFiles, System.SysUtils;

constructor TAltiumProject.Create(fn: string);
begin
  FSheets := TAltiumSheetList.Create(True);
  FComponents := TAltiumComponentList.Create(True);
  LoadFromFile(fn);
end;

destructor TAltiumProject.Destroy;
begin
  FComponents.Free;
  FSheets.Free;
  inherited;
end;

procedure TAltiumProject.LoadFromFile(fn: string);
  procedure OpenSchDoc(schname: string);
  var
    af: TAltiumSheet;
    part: TAltiumComponentPart;
    cmp: TAltiumComponent;
    flg: Boolean;
  begin
      af := TAltiumSheet.Create(schname);
      FSheets.Add(af);

      for part in af.Parts do
      begin
        flg := False;
        for cmp in FComponents do
          if cmp.Name = part.CompName then
          begin
            cmp.Parts.Add(part);
            flg := True;
            break;
          end;

        if not flg then
        begin
          cmp := TAltiumComponent.Create;
          cmp.Parts.Add(part);
          FComponents.Add(cmp);
        end;
      end;
  end;
var
  pfile: TIniFile;
  i: Integer;
  sect: string;
  fname: string;
  dir: string;
  fext: string;
begin
  FFileName := fn;

  fext := UpperCase(ExtractFileExt(fn));
  if(fext = '.PRJPCB')then
  begin
    FSheets.Clear;

    dir := ExtractFilePath(fn)+'\';

    if(not FileExists(fn))then
      raise Exception.Create('File '''+fn+''' not found!''');

    pfile := TIniFile.Create(fn);

    i := 1;
    sect := 'Document1';
    while(pfile.SectionExists(sect))do
    begin
      fname := pfile.ReadString(sect, 'DocumentPath', '');

      if(UpperCase(ExtractFileExt(fname)) = '.SCHDOC')then
        OpenSchDoc(dir + fname);

      Inc(i);
      sect := 'Document' + IntToStr(i);
    end;
  end
  else if(fext = '.SCHDOC')then
    OpenSchDoc(fn);
end;

end.
