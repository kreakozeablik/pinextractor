unit AltiumRecordEnum;

interface

uses WinApi.ActiveX, Classes, Vcl.AxCtrls;

type
  TAltiumRecordEnum = class(TObject)
  private
    plst: TStringList;
    FOleStream: TOleStream;
    AFile: IStream;
    Root: IStorage;
    function GetPropI(ind: string): Integer;
    function GetPropS(ind: string): string;
    function GetCurrent: TAltiumRecordEnum;
    function GetText: string;
  public
    property PropS[ind: string]: string read GetPropS;
    property PropI[ind: string]: Integer read GetPropI;

    property Text: string read GetText;

    constructor Create(fn: string);
    destructor Destroy; override;

    function GetEnumerator: TAltiumRecordEnum;
    function MoveNext: Boolean;
    property Current: TAltiumRecordEnum read GetCurrent;
  end;

implementation

uses System.SysUtils, AnsiStrings;

{ TAltiumRecord }

constructor TAltiumRecordEnum.Create(fn: string);
var
  BuffFileName: WideString;
  len: Int32;
begin
  plst := TStringList.Create();

  BuffFileName := fn;

  if not Succeeded(StgOpenStorage(@BuffFileName[1], nil, STGM_READ or STGM_SHARE_DENY_WRITE, nil, 0, Root)) then
    RaiseLastOSError;

  if not Succeeded(Root.OpenStream('FileHeader', nil, STGM_READ or STGM_SHARE_EXCLUSIVE, 0, AFile)) then
    RaiseLastOSError;

  FOleStream := TOleStream.Create(AFile);

  //Ignore header
  if (FOleStream.Read(len, 4) = 4) then
    FOleStream.Seek(len, soFromCurrent);
end;

destructor TAltiumRecordEnum.Destroy;
begin
  plst.Free;
  FOleStream.Free;
  inherited;
end;

function TAltiumRecordEnum.GetCurrent: TAltiumRecordEnum;
begin
  Result := self;
end;

function TAltiumRecordEnum.GetEnumerator: TAltiumRecordEnum;
begin
  Result := self;
end;

function TAltiumRecordEnum.GetPropI(ind: string): Integer;
var
  s: string;
begin
  s := plst.Values[ind];
  if(s = '')then
    result := 0
  else
    result := StrToInt(s);
end;

function TAltiumRecordEnum.GetPropS(ind: string): string;
begin
  result := plst.Values[ind];
end;

function TAltiumRecordEnum.GetText: string;
var
  k: string;
begin
  Result := '';
  for k in plst do
  begin
    if Result <> '' then
      Result := Result + '|';
    Result := Result + k;
  end;
end;

function TAltiumRecordEnum.MoveNext: Boolean;
var
  len: Int32;
  rlen: Integer;
  ts: AnsiString;
  pos: PAnsiChar;
  name: AnsiString;
  value: AnsiString;
begin
  plst.Clear;

  Result := false;
  if (FOleStream.Read(len, 4) < 4) then
    exit;

  SetLength(ts, len);
  rlen := FOleStream.Read(Pointer(ts)^, len);

  if(rlen <> len)then
    raise Exception.Create('Read record error.');

  pos := @ts[1];
  while(pos^ <> #0) do
  begin
  while((pos^ = '|') and (pos^ <> #0)) do
    Inc(pos);

    name := '';
    while((pos^ <> '=') and (pos^ <> #0)) do
    begin
      name := name + pos^;
      Inc(pos);
    end;

    Inc(pos);
    value := '';
    while((pos^ <> '|') and (pos^ <> #0)) do
    begin
      value := value + pos^;
      Inc(pos);
    end;

    plst.AddPair(string(name), string(value));
  end;

  Result := true;
end;

end.
