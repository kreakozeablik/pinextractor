program read_rec;

uses
  Vcl.Forms,
  System.IOUtils,
  Classes,
  SysUtils,
  AltiumRecordEnum in '..\AltiumRecordEnum.pas';

{$R *.res}

var
  sl: TStringList;
  nfn: string;
  r: TAltiumRecordEnum;
  ind: Integer;
begin
  Application.Initialize;
  If(ParamCount > 0)then
  begin
    sl := TStringList.Create;

    ind := 0;
    for r in TAltiumRecordEnum.Create(ParamStr(1)) do
    begin
      if(r.PropI['RECORD'] in [1,2,17,25,27, 34])then
        sl.Add(IntToStr(ind) + ') ' + r.Text);
      Inc(ind);
    end;

    nfn := ExtractFilePath(ParamStr(1))+'\'+TPath.GetFileNameWithoutExtension(ParamStr(1))+'_rec.txt';
    sl.SaveToFile(nfn);
    sl.Free;
  end;
end.
