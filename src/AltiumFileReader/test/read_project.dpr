program read_project;

{$APPTYPE CONSOLE}
uses
  Vcl.Forms,
  Classes,
  System.IOUtils,
  SysUtils,
  AltiumComponent in '..\AltiumComponent.pas',
  AltiumProject in '..\AltiumProject.pas',
  AltiumRecordEnum in '..\AltiumRecordEnum.pas',
  AltiumSheet in '..\AltiumSheet.pas';

{$R *.res}

var
  sl: TStringList;
  nfn: string;

  prj: TAltiumProject;
  cmp: TAltiumComponent;
  pin: TAltiumPin;
begin
  Application.Initialize;
  If(ParamCount > 0)then
  begin
    sl := TStringList.Create;

    prj := TAltiumProject.Create(ParamStr(1));

    for cmp in prj.Components do
    begin
      sl.Add(cmp.Name + ' : '+ cmp.LibRef + ' : '+ cmp.PartNum + ' : ' + cmp.Description);
      for pin in cmp do
        sl.Add(' - ' +
                pin.OwnerPart.OwnerSheet.Name + ' : ' +
                pin.OwnerPart.PartName + ' : '+
                pin.Name + ' : ' +
                pin.Designator + ' : ' +
                pin.NetName);
    end;

    nfn := ExtractFilePath(ParamStr(1))+'\'+TPath.GetFileNameWithoutExtension(ParamStr(1))+'_prj.txt';
    sl.SaveToFile(nfn);
    sl.Free;
    prj.Free;
  end;
end.
