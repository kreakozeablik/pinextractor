program read_parts;

uses
  Vcl.Forms,
  Classes,
  System.IOUtils,
  SysUtils,
  AltiumSheet in '..\AltiumSheet.pas',
  AltiumRecordEnum in '..\AltiumRecordEnum.pas',
  AltiumComponent in '..\AltiumComponent.pas';

{$R *.res}

var
  sl: TStringList;
  nfn: string;

  sheet: TAltiumSheet;
  part: TAltiumComponentPart;
  pin: TAltiumPin;
begin
  Application.Initialize;
  If(ParamCount > 0)then
  begin
    sl := TStringList.Create;

    sheet := TAltiumSheet.Create(ParamStr(1));

    for part in sheet.Parts do
    begin
      sl.Add(part.Name + ' : '+ part.PartNum + ' : ' + part.PartName + ' : ' + part.Description);
      for pin in part.Pins do
        sl.Add(' - '+pin.Name + ' : '+ pin.Designator + ' : '+ pin.NetName);
    end;

    nfn := ExtractFilePath(ParamStr(1))+'\'+TPath.GetFileNameWithoutExtension(ParamStr(1))+'_parts.txt';
    sl.SaveToFile(nfn);
    sl.Free;
  end;
end.
