unit UFormWait;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls;

type
  TAssyncProc = reference to procedure();

  TReadThread = class(TThread)
  public
    wfHandle: THANDLE;
    fn: TAssyncProc;
    e: Exception;
  protected
    procedure Execute; override;
  end;

  TFormWait = class(TForm)
    Panel1: TPanel;
    procedure FormShow(Sender: TObject);
  public
    t: TReadThread;
  end;

procedure RunAsync(fn: TAssyncProc); // RunAsync(procedure() begin ...statment...; end);

implementation

{$R *.dfm}

procedure RunAsync(fn: TAssyncProc);
var
  wf: TFormWait;
  t: TReadThread;
  e: Exception;
begin
  wf := TFormWait.Create(Application.MainForm);
  t := TReadThread.Create(true);
  t.fn := fn;
  t.e := nil;
  wf.t := t;
  wf.ShowModal;  // � ��������� ���� ���� ���� ���������! :)
  e := t.e;
  wf.Free;
  t.Free;
  if Assigned(e) then
    raise e;
end;

{ TReadThread }

procedure TReadThread.Execute;
begin
  try
    fn();
  except
    on ee: Exception do
      e := ee;
  end;
  PostMessage(wfHandle, WM_CLOSE, 0, 0);
end;

procedure TFormWait.FormShow(Sender: TObject);
begin
  t.wfHandle := Handle;
  t.Start;
end;

end.
