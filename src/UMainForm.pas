unit UMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, ImgList,
  Menus, Generics.Collections, System.ImageList,
  Vcl.Grids, Vcl.ToolWin, System.Actions, Vcl.ActnList, SynMemo,
  SynEditSearch, SynHinhlightUCF, SynEdit, SynEditMiscClasses,
  Vcl.StdActns, Vcl.Buttons, PinDoc, SynEditHighlighter, TB2ExtItems,
  TBXExtItems, TBX, TB2Item, TB2Dock, TB2Toolbar, TBXLists, UMoveList,
  UdmCommonData, UAboutForm;

type
  TMainForm = class(TForm)
    Splitter1: TSplitter;
    Panel3: TPanel;
    ActionList: TActionList;
    actOpenSch: TAction;
    SynMemo1: TSynMemo;
    SynEditSearch: TSynEditSearch;
    cbSeachWholeWord: TCheckBox;
    ActionFindForward: TAction;
    ActionFindBackward: TAction;
    edtFind: TComboBox;
    alGroups: TActionList;
    ActionGroupAdd: TAction;
    ActionGroupRemove: TAction;
    pmGroups: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pmMemo: TPopupMenu;
    Copy1: TMenuItem;
    FileSaveAs: TFileSaveAs;
    N3: TMenuItem;
    miAddGrp1: TMenuItem;
    miAddGrp2: TMenuItem;
    miAddGrp3: TMenuItem;
    ActionGroupClean: TAction;
    pnlFiltered: TPanel;
    actMakeMeHappy: TAction;
    actSetings: TAction;
    Panel2: TPanel;
    TBXToolbar1: TTBXToolbar;
    smRecentFiles: TTBXSubmenuItem;
    TBXItem1: TTBXItem;
    TBXItem2: TTBXItem;
    TBXItem3: TTBXItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBSeparatorItem4: TTBSeparatorItem;
    TBXToolbar2: TTBXToolbar;
    TBXItem4: TTBXItem;
    TBXItem5: TTBXItem;
    TBSeparatorItem5: TTBSeparatorItem;
    TBXItem6: TTBXItem;
    TBXToolbar3: TTBXToolbar;
    TBXSeparatorItem1: TTBXSeparatorItem;
    TBXLabelItem2: TTBXLabelItem;
    TBXItem7: TTBXItem;
    TBXItem8: TTBXItem;
    TBXSeparatorItem2: TTBXSeparatorItem;
    TBControlItem1: TTBControlItem;
    TBControlItem2: TTBControlItem;
    Panel1: TPanel;
    smFormat: TTBXSubmenuItem;
    actHideGroup: TAction;
    actShowGroup: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    pnlAllCnt: TPanel;
    actEditMezanins: TAction;
    TBXItem9: TTBXItem;
    N6: TMenuItem;
    actGroupEdit: TAction;
    N7: TMenuItem;
    actMemoEditGroup: TAction;
    N8: TMenuItem;
    N9: TMenuItem;
    actGroupMoveTo: TAction;
    N10: TMenuItem;
    pnlNoGrouped: TPanel;
    EditCopy2: TEditCopy;
    EditSelectAll2: TEditSelectAll;
    SelectAll1: TMenuItem;
    actAbout: TAction;
    TBXItem10: TTBXItem;
    TBSeparatorItem1: TTBSeparatorItem;
    procedure actOpenSchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ActionFindForwardExecute(Sender: TObject);
    procedure ActionFindBackwardExecute(Sender: TObject);
    procedure edtFindKeyPress(Sender: TObject; var Key: Char);
    procedure sgGroupsDblClick(Sender: TObject);
    procedure ActionGroupAddExecute(Sender: TObject);
    procedure ActionGroupRemoveExecute(Sender: TObject);
    procedure lbGroupsData(Control: TWinControl; Index: Integer;
      var Data: string);
    procedure lbGroupsDataObject(Control: TWinControl; Index: Integer;
      var DataObject: TObject);
    procedure lbGroupsContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure lbGroupsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure tbSelectFormatClick(Sender: TObject);
    procedure FileSaveAsBeforeExecute(Sender: TObject);
    procedure FileSaveAsAccept(Sender: TObject);
    procedure FileSaveAsSaveDialogCanClose(Sender: TObject;
      var CanClose: Boolean);
    procedure FileSaveAsUpdate(Sender: TObject);
    procedure pmMemoPopup(Sender: TObject);
    procedure miAddGrp1Click(Sender: TObject);
    procedure miAddGrp2Click(Sender: TObject);
    procedure miAddGrp3Click(Sender: TObject);
    procedure SynMemo1TokenHint(Sender: TObject; Coords: TBufferCoord;
      const Token: string; TokenType: Integer; Attri: TSynHighlighterAttributes;
      var HintText: string);
    procedure ActionGroupCleanExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actMakeMeHappyExecute(Sender: TObject);
    procedure actSetingsExecute(Sender: TObject);
    procedure smRecentFilesPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure actHideGroupExecute(Sender: TObject);
    procedure Panel2Resize(Sender: TObject);
    procedure alGroupsUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure actEditMezaninsExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure actGroupEditExecute(Sender: TObject);
    procedure actMemoEditGroupExecute(Sender: TObject);
    procedure actGroupMoveToExecute(Sender: TObject);
    procedure actShowGroupExecute(Sender: TObject);
    procedure pnlNoGroupedClick(Sender: TObject);
    procedure pnlFilteredClick(Sender: TObject);
    procedure actAboutExecute(Sender: TObject);
  private
    lbGroups: TMoveList;

    FStartup: Boolean;
    FDoc: TPinDoc;

    UpdGroups: Boolean;

    FPosAggGrp: Integer;
    FStrAggGrp1: string;
    FStrAggGrp2: string;
    FStrAggGrp3: string;

    FRecentPrj: TStringList;
    recent2open: Integer;

    procedure UpdateActionGroups();

    procedure lbGroupsMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure lbGroupsOnMove(Src, Dst: Integer);
    procedure NOpenRecentClick(Sender: TObject);
    procedure NFormatClick(Sender: TObject);

    procedure FocusGroup(g: TGroup);

    function DoOpenSch(fname: string; cmp: string): Boolean;
    procedure DoSearch(back: Boolean);
    procedure OnSelectComp();

    procedure AddGroup(fltr: string = ''; ind: Integer = -1; bus: Boolean = False);
    procedure RemoveGroup(i: Integer);
    procedure UpdateGroup(i: Integer = -1);

    procedure PinDocOnUpdate();
    procedure OnGroupsUpdate();
    procedure OnFormatUpdate();

    procedure UpdateFormatMenu();
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses System.IOUtils, USelectCompForm, System.UITypes, SynEditTypes, AppConfig,
  UDlgGroupForm, System.Types, strutil, UFormSettings, JSON, UConstraintsFormatter,
  UFormWait, UFormMezaninEdt;

{$R *.dfm}

procedure TMainForm.actAboutExecute(Sender: TObject);
begin
 with TAboutForm.Create(self) do
 begin
   ShowModal();
   Free;
 end;
end;

procedure TMainForm.actEditMezaninsExecute(Sender: TObject);
var
  frm: TFormMezaninEdt;
begin
  frm := TFormMezaninEdt.Create(self);
  frm.Init(FDoc);
  frm.ShowModal();
  frm.Free;
end;

procedure TMainForm.actGroupEditExecute(Sender: TObject);
begin
  //
end;

procedure TMainForm.actGroupMoveToExecute(Sender: TObject);
begin
  if lbGroups.ItemIndex >= 0 then
    FocusGroup(FDoc.Groups[lbGroups.ItemIndex]);
end;

procedure TMainForm.actHideGroupExecute(Sender: TObject);
begin
  if lbGroups.ItemIndex >= 0 then
  begin
    FDoc.Groups[lbGroups.ItemIndex].Visible := False;
    lbGroups.Invalidate();
    PinDocOnUpdate;
  end;
end;

procedure TMainForm.ActionFindBackwardExecute(Sender: TObject);
begin
  DoSearch(True);
end;

procedure TMainForm.ActionFindForwardExecute(Sender: TObject);
begin
  DoSearch(False);
end;

procedure TMainForm.ActionGroupAddExecute(Sender: TObject);
begin
  AddGroup('');
end;

procedure TMainForm.ActionGroupCleanExecute(Sender: TObject);
begin
  if (MessageDlg('������� ������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    FDoc.CleanGroups();
  end;
end;

procedure TMainForm.ActionGroupRemoveExecute(Sender: TObject);
begin
  if lbGroups.ItemIndex >= 0 then
    RemoveGroup(lbGroups.ItemIndex)
  else
    Beep();
end;

procedure TMainForm.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  actEditMezanins.Enabled := FDoc.CompSelected;
end;

procedure TMainForm.actMakeMeHappyExecute(Sender: TObject);
begin
  if Assigned(FDoc.CurrentComp) then
    FDoc.AutoGroup()
  else if DoOpenSch('', '') then
    FDoc.AutoGroup();
end;

procedure TMainForm.actMemoEditGroupExecute(Sender: TObject);
var
  line: Integer;
  pd: TPinData;
begin
  line := SynMemo1.CaretY - 1;
  if (line < SynMemo1.Lines.Count) then
  begin
    pd := (SynMemo1.Lines.Objects[line] as TPinData);
    if Assigned(pd) and Assigned(pd.Group) and (pd.Group <> FDoc.NoGroup) and (pd.Group <> FDoc.FilteredGroup) then
    begin
    with TDlgGroupForm.Create(self) do
    begin
      if Edit(pd.Group) then
        UpdateGroup();
      Free;
    end;
    end;
  end;
end;

procedure TMainForm.actOpenSchExecute(Sender: TObject);
begin
  DoOpenSch('', '');
end;

procedure TMainForm.actSetingsExecute(Sender: TObject);
begin
  FormSettingsDlgExecute(FDoc);
end;

procedure TMainForm.actShowGroupExecute(Sender: TObject);
begin
  if lbGroups.ItemIndex >= 0 then
  begin
    FDoc.Groups[lbGroups.ItemIndex].Visible := True;
    lbGroups.Invalidate();
    PinDocOnUpdate;
  end;
end;

procedure TMainForm.AddGroup(fltr: string = ''; ind: Integer = -1; bus: Boolean = False);
var
  g: TGroup;
begin
  g := TGroup.Create();
  g.Visible := True;
  g.Filtr := fltr;

  if bus then
    g.Buses.Add(TBus.Create(fltrstr(fltr)));

  with TDlgGroupForm.Create(self) do
  begin
    if Edit(g) then
    begin
      if ind >= 0 then
        FDoc.Groups.Insert(ind, g)
      else
        FDoc.Groups.Add(g);
      UpdateGroup(0);
    end else
      g.Free;
    Free;
  end;
end;

procedure TMainForm.alGroupsUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  UpdateActionGroups();
end;

function TMainForm.DoOpenSch(fname: string; cmp: string): Boolean;
var
  frm: TSelectCompForm;
begin
  Result := False;
  frm := TSelectCompForm.Create(self);
  frm.Init(FDoc, fname, cmp);
  if frm.ShowModal = mrOk then
  begin
    OnSelectComp;
    Result := True;
  end;
  frm.Free;
end;

procedure TMainForm.DoSearch(back: Boolean);
var
  Options: TSynSearchOptions;
begin
  if edtFind.Text = '' then
  begin
    edtFind.SetFocus;
    MessageBeep(MB_ICONASTERISK);
    exit;
  end;

  Options := [];
  if back then
    Include(Options, ssoBackwards);
  if cbSeachWholeWord.Checked then
    Include(Options, ssoWholeWord);

  if SynMemo1.SearchReplace(edtFind.Text, '', Options) = 0 then
  begin
    MessageBeep(MB_ICONASTERISK);
    if ssoBackwards in Options then
      SynMemo1.BlockEnd := SynMemo1.BlockBegin
    else
      SynMemo1.BlockBegin := SynMemo1.BlockEnd;
    SynMemo1.CaretXY := SynMemo1.BlockBegin;
  end else
  begin
    AddComboLines(edtFind.Items, edtFind.Text);
    edtFind.ItemIndex := 0;
  end;
end;

procedure TMainForm.edtFindKeyPress(Sender: TObject; var Key: Char);
var
  KState : TKeyboardState;
begin
  if Key = #$D then
  begin
    GetKeyboardState(KState);
    DoSearch((KState[vk_Shift] and 128) <> 0);
    Key := #0;
  end;
end;

procedure TMainForm.FileSaveAsAccept(Sender: TObject);
var
  fn: string;
begin
  fn := FileSaveAs.Dialog.FileName;
  if fn <> '' then
    SynMemo1.Lines.SaveToFile(fn);
end;

procedure TMainForm.FileSaveAsBeforeExecute(Sender: TObject);
var
  ext: string;
begin
  if FDoc.Format < 0 then
    exit;

  ext := FormatReestr[FDoc.Format].GetFileExt;
  FileSaveAs.Dialog.Filter := FormatReestr[FDoc.Format].GetName + ' (*.' + ext + ')|*.' + ext + '|All files(*.*)|*.*';
  FileSaveAs.Dialog.FilterIndex := 0;
  FileSaveAs.Dialog.DefaultExt := '.' + ext;


  if Assigned(FDoc.CurrentComp) then
    FileSaveAs.Dialog.FileName := TPath.GetFileNameWithoutExtension(FDoc.Shema.FileName) +
              '-'+
              FDoc.CurrentComp.Name +
              FileSaveAs.Dialog.DefaultExt;
end;

procedure TMainForm.FileSaveAsSaveDialogCanClose(Sender: TObject;
  var CanClose: Boolean);
var
  fn: string;
begin
  fn := FileSaveAs.Dialog.FileName;
  CanClose := not FileExists(fn) or
    (MessageDlg('���� '''+ fn +''' ��� ����������, ������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes);
end;

procedure TMainForm.FileSaveAsUpdate(Sender: TObject);
begin
  FileSaveAs.Enabled := SynMemo1.Lines.Count > 0;
end;

procedure TMainForm.FocusGroup(g: TGroup);
var
  i: Integer;
  pd: TPinData;
  bc: TBufferCoord;

begin
  for i := 0 to SynMemo1.Lines.Count - 1 do
  begin
    pd := SynMemo1.Lines.Objects[i] as TPinData;
    if Assigned(pd) and (pd.Group = g) then
    begin
      if (i < SynMemo1.TopLine) or (i > SynMemo1.TopLine + SynMemo1.LinesInWindow) then
        SynMemo1.TopLine := i - (SynMemo1.LinesInWindow div 2);

      bc.Char := 1;
      bc.Line := i+1;
      SynMemo1.SelStart := SynMemo1.RowColToCharIndex(bc);
      bc.Line := i+2;
      SynMemo1.SelEnd := SynMemo1.RowColToCharIndex(bc);

      break;
    end;
  end;
end;

procedure TMainForm.FormActivate(Sender: TObject);
var
  cmp: string;
begin
  if(FStartup) then
  begin
    FStartup := False;

    if(ParamCount > 0) then
    begin
      cmp := '';
      if ParamCount > 1 then
      begin
        if FileExists(ParamStr(1)) then
        begin
          FDoc.Shema.LoadFromFile(ParamStr(1));
          if FDoc.Select(UpperCase(ParamStr(2))) then
            OnSelectComp()
          else
          begin
            MessageDlg('��������� "' + ParamStr(2) + '" �� ������!', mtWarning, [mbOK], 0);
            DoOpenSch('', '')
          end;
        end
        else
          MessageDlg('���� "' + ParamStr(1) + '" �� ������!', mtWarning, [mbOK], 0);
      end
      else
      begin
        if FileExists(ParamStr(1)) then
          DoOpenSch(ParamStr(1), cmp)
        else
          MessageDlg('���� "' + ParamStr(1) + '" �� ������!', mtWarning, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  s: string;
  df: string;
begin
  if Assigned(FDoc.CurrentComp) { and (FDoc.Groups.Count > 0) } then
  begin
    s := FDoc.GetFileComp();

    FDoc.Save('.\' + s + '.pin');

    df := AddComboLines(FRecentPrj, s);

    if (df <> '') then
    begin
      s := '.\' + df + '.pin';
      if (FileExists(s)) then
        DeleteFile(s);
    end;

    Config.WriteStrings(CONFIG_RECENT_PRJ_LST, FRecentPrj);
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);

  procedure CreateFormatMenu();
  var
    f: TBaseFormat;
    mi: TTBXItem;
    i: Integer;
    imgind: Integer;
    icon: string;
    c: TIcon;
  begin
    i := 0;
    for f in FormatReestr do
    begin
      imgind := -1;
      icon := f.GetIconResurceName;
      if not icon.IsEmpty then
      begin
        c := TIcon.Create;
        c.LoadFromResourceName(hInstance, icon);
        imgind := dmCommonData.ImagesFormat.AddIcon(c);
      end;


      mi := TTBXItem.Create(smFormat);
      mi.InheritOptions := False;
      mi.DisplayMode := nbdmImageAndText;
      mi.Caption := f.GetName;
      mi.Tag := i;
      mi.Images := dmCommonData.ImagesFormat;
      mi.OnClick := NFormatClick;
      mi.ImageIndex := imgind;
      mi.RadioItem := True;
      mi.GroupIndex := 1;
      smFormat.Add(mi);

      Inc(i);
    end;
  end;

begin
///////////////
  lbGroups := TMoveList.Create(Self);
  lbGroups.Parent := Panel2;
  lbGroups.Style := lbVirtualOwnerDraw;
  lbGroups.Align := alClient;
  lbGroups.ItemHeight := 20;
  lbGroups.PopupMenu := pmGroups;
  lbGroups.TabOrder := 1;
  lbGroups.OnContextPopup := lbGroupsContextPopup;
  lbGroups.OnData := lbGroupsData;
  lbGroups.OnDataObject := lbGroupsDataObject;
  lbGroups.OnDblClick := sgGroupsDblClick;
  lbGroups.OnDrawItem := lbGroupsDrawItem;
  lbGroups.OnMove := lbGroupsOnMove;
  lbGroups.OnMouseUp := lbGroupsMouseUp;

///////////////

  TBXSetTheme('OfficeXP');

  CreateFormatMenu();

  UpdGroups := False;
  FStartup := True;

  FDoc := TPinDoc.Create();
  FDoc.Options.Load(Config.GetJSONObj(CONFIG_OPTIONS) as TJSONObject);

  FDoc.PinDocGroupsOnUpdate := OnGroupsUpdate;
  FDoc.PinDocOnUpdate := PinDocOnUpdate;
  FDoc.PinDocFormatUpdate := OnFormatUpdate;

  FRecentPrj := TStringList.Create;
  Config.ReadStrings(CONFIG_RECENT_PRJ_LST, FRecentPrj);

  UpdateFormatMenu();
end;

procedure TMainForm.FormDestroy(Sender: TObject);
var
  h: TSynCustomHighlighter;
begin
  h := SynMemo1.Highlighter;
  if h <> nil then
  begin
    SynMemo1.Highlighter := nil;
    h.Free();
  end;

  FDoc.PinDocGroupsOnUpdate := nil;
  FDoc.PinDocOnUpdate := nil;
  FDoc.PinDocFormatUpdate := nil;
  FreeAndNil(FDoc);
  FreeAndNil(FRecentPrj);
end;

procedure TMainForm.lbGroupsContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
  lbGroups.ItemIndex := lbGroups.ItemAtPos(MousePos, True);
end;

procedure TMainForm.lbGroupsData(Control: TWinControl; Index: Integer;
  var Data: string);
begin
  if (Index < FDoc.Groups.Count) and (Index >= 0) then
    Data := FDoc.Groups[Index].Name
  else
    Data := '';
end;

procedure TMainForm.lbGroupsDataObject(Control: TWinControl; Index: Integer;
  var DataObject: TObject);
begin
  if (Index < FDoc.Groups.Count) and (Index >= 0) then
    DataObject := FDoc.Groups[Index]
  else
    DataObject := nil;
end;

procedure TMainForm.lbGroupsDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  Flags: Longint;
  Canvas: TCanvas;
  Str: string;
  TxtRec: TRect;
  CntWidth: Integer;
begin
  if not Assigned(FDoc) then
    exit;

  Canvas := lbGroups.Canvas;
  Canvas.FillRect(Rect);
  if (Index < FDoc.Groups.Count) and (Index >= 0) then
  begin
    if FDoc.Groups[Index].Visible then
    begin
      if FDoc.Groups[Index].Buses.Count > 0 then
        Canvas.Font.Style := [fsBold]
      else
        Canvas.Font.Style := [];

      Canvas.Font.Color := clBlack
    end else begin
      Canvas.Font.Style := [fsItalic];
      Canvas.Font.Color := clGray;
    end;

    CntWidth := Canvas.TextWidth('0000');

    TxtRec := Rect;
    Inc(TxtRec.Left, 2);
    Dec(TxtRec.Right, CntWidth + 4);
    Str := FDoc.Groups[Index].Name;
    Flags := lbGroups.DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);
    DrawText(Canvas.Handle, Str, Length(Str), TxtRec, Flags);

    if FDoc.Groups[Index].Pins.Count > 0 then
    begin
      TxtRec.Left := TxtRec.Right + 2;
      TxtRec.Right := Rect.Right - 2;
      Str := IntToStr(FDoc.Groups[Index].Pins.Count);
      Flags := lbGroups.DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX or DT_LEFT);
      DrawText(Canvas.Handle, Str, Length(Str), TxtRec, Flags);
    end;
  end;
end;

procedure TMainForm.lbGroupsMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  g: TGroup;
begin
  if (ssCtrl in Shift) and (lbGroups.ItemIndex >= 0) then
  begin
    g := FDoc.Groups[lbGroups.ItemIndex];
    FocusGroup(g);
  end;
end;

procedure TMainForm.miAddGrp1Click(Sender: TObject);
begin
  AddGroup(FStrAggGrp1, FPosAggGrp);
end;

procedure TMainForm.miAddGrp2Click(Sender: TObject);
begin
  AddGroup(FStrAggGrp2, FPosAggGrp);
end;

procedure TMainForm.miAddGrp3Click(Sender: TObject);
begin
  AddGroup(FStrAggGrp3, FPosAggGrp, True);
end;

procedure TMainForm.NFormatClick(Sender: TObject);
begin
   FDoc.Format := (Sender as TComponent).Tag;
end;

procedure TMainForm.NOpenRecentClick(Sender: TObject);
var
  s: string;
  c: string;
begin
  recent2open := (Sender as TComponent).Tag;

  c := FRecentPrj[recent2open];
  s := '.\' + c + '.pin';
  if FileExists(s) then
  begin
    FDoc.PinDocGroupsOnUpdate := nil;
    FDoc.PinDocOnUpdate := nil;
    FDoc.PinDocFormatUpdate := nil;


    RunAsync(procedure()begin FDoc.Load(s); end);
    AddComboLines(FRecentPrj, c);

    FDoc.PinDocGroupsOnUpdate := OnGroupsUpdate;
    FDoc.PinDocOnUpdate := PinDocOnUpdate;
    FDoc.PinDocFormatUpdate := OnFormatUpdate;

    OnGroupsUpdate;
    UpdateFormatMenu;
    PinDocOnUpdate;

    if not FDoc.ValidMezanins() then
    begin
      MessageDlg('������ �������� ��������!', mtWarning, [mbOK], 0);
      actEditMezanins.Execute;
    end;
  end
  else
    MessageDlg('���� ������� �� ������!', mtError, [mbOK], 0);
end;

procedure TMainForm.sgGroupsDblClick(Sender: TObject);
begin
  if lbGroups.Items.Objects[lbGroups.ItemIndex] <> nil then
  begin
    with TDlgGroupForm.Create(self) do
    begin
      if Edit(lbGroups.Items.Objects[lbGroups.ItemIndex] as TGroup) then
        UpdateGroup(lbGroups.ItemIndex);
      Free;
    end;
  end;
end;

procedure TMainForm.smRecentFilesPopup(Sender: TTBCustomItem; FromLink: Boolean);
var
  s: string;
  mi: TTBXItem;
  i: Integer;
begin
  smRecentFiles.Clear;
  i := 0;
  for s in FRecentPrj do
  begin
    if FileExists('.\' + s + '.pin') then
    begin
      mi := TTBXItem.Create(smRecentFiles);
      mi.Caption := s;
      mi.Tag := i;
      mi.OnClick := NOpenRecentClick;
      smRecentFiles.Add(mi);
    end;
    Inc(i);
  end;
end;

procedure TMainForm.SynMemo1TokenHint(Sender: TObject; Coords: TBufferCoord;
  const Token: string; TokenType: Integer; Attri: TSynHighlighterAttributes;
  var HintText: string);

  function FormatPinHint(p: TPinData): string;
  begin
    Result := p.NetName + ' | ' +
      p.Designator + ' | ' +
      p.NetNameOriginal + ' | ' +
      p.Sheet + ' | ' +
      p.Part + ' | ' +
      p.Name
  end;

var
  pd: TPinData;
  p: TPinData;
  f: Boolean;
begin
  pd := (SynMemo1.Lines.Objects[Coords.Line - 1] as TPinData);
  if Assigned(pd) then
    with pd do
      if Assigned(pd.Pin) then
        HintText := FormatPinHint(pd)
      else if Assigned(Group) then
      begin
        HintText := '';
        f := False;
        for p in Group.Pins do
        begin
          if f then
            HintText := HintText + sLineBreak;
          f := True;
          HintText := HintText + FormatPinHint(p);
        end;
      end;
end;

procedure TMainForm.tbSelectFormatClick(Sender: TObject);
begin
  (Sender as TToolButton).CheckMenuDropdown;
end;

procedure TMainForm.OnFormatUpdate;
begin
  UpdateFormatMenu();
  PinDocOnUpdate();
end;

procedure TMainForm.OnGroupsUpdate;
var
  sp: Integer;
begin
  sp := lbGroups.TopIndex;
  lbGroups.Count := FDoc.Groups.Count;
  lbGroups.TopIndex := sp;
end;

procedure TMainForm.UpdateActionGroups();
var
  v: Boolean;
begin
  ActionGroupRemove.Enabled := lbGroups.ItemIndex >= 0;
  ActionGroupClean.Enabled := lbGroups.Items.Count > 0;

  v := (lbGroups.Items.Count > 0) and (lbGroups.ItemIndex >= 0);
  actGroupMoveTo.Enabled := v;
  actHideGroup.Enabled := v;
  actShowGroup.Enabled := v;
  actGroupEdit.Enabled := v;
  v := v and FDoc.Groups[lbGroups.ItemIndex].Visible;
  actHideGroup.Visible := v;
  actShowGroup.Visible := not v;
end;

procedure TMainForm.lbGroupsOnMove(Src, Dst: Integer);
begin
  FDoc.Groups.Move(Src, Dst);
  FDoc.UpdateGroupsPins;
  FocusGroup(FDoc.Groups[Dst]);
end;

procedure TMainForm.OnSelectComp;
begin
  if Assigned(FDoc.CurrentComp) then
    Caption := 'PinExtractor - ' +
              TPath.GetFileNameWithoutExtension(FDoc.Shema.FileName) +
              ' - '+
              FDoc.CurrentComp.Name;
end;

procedure TMainForm.Panel2Resize(Sender: TObject);
begin
  if Assigned(lbGroups) then
    lbGroups.Invalidate;
end;

procedure TMainForm.PinDocOnUpdate;
var
  row: Integer;
  frmttr: TBaseDocFormatter;
begin
  SynMemo1.Lines.BeginUpdate;

  row := SynMemo1.TopLine;
  SynMemo1.Lines.Clear;

  if FDoc.Format >= 0 then
  begin
    frmttr := FormatReestr[FDoc.Format].GetFormatter();
    frmttr.Format(FDoc, SynMemo1.Lines);
    frmttr.Free;
  end;

  SynMemo1.TopLine := row;
  if not FStartup then
    SynMemo1.SetFocus;

  if Assigned(FDoc.CurrentComp) then
  begin
    pnlNoGrouped.Caption := '�� �������������: ' + IntToStr(FDoc.NoGroup.Pins.Count);
    pnlFiltered.Caption := '�������������: ' + IntToStr(FDoc.FilteredGroup.Pins.Count);
    pnlAllCnt.Caption := '�����: ' + IntToStr(FDoc.CurrentComp.Pins.Count);
  end;

  SynMemo1.Lines.EndUpdate;
end;

procedure TMainForm.pmMemoPopup(Sender: TObject);
  function GetGrp2(net: string ; var grp: string): Boolean;
  var
    p: Integer;
  begin
    Result := False;
    p := Pos('_', net);
    if p > 0 then
    begin
      grp := Copy(net, 1, p) + '*';
      Result := True;
    end;
  end;

  function GetGrp3(net: string ; var grp: string): Boolean;
    function IsBusGroups(rgx: string): Boolean;
    var
      pin: TPinData;
      cnt: Integer;
      g: TGroup;
    begin
      Result := False;
      cnt := 0;
      for g in FDoc.Groups do
        for pin in g.Pins do
          if reg_exp(pin.NetName, rgx) then
          begin
            cnt := cnt + 1;
            if cnt > 1 then
            begin
              Result := True;
              exit;
            end;
          end;

      for pin in FDoc.NoGroup.Pins do
        if reg_exp(pin.NetName, rgx) then
        begin
          cnt := cnt + 1;
          if cnt > 1 then
          begin
            Result := True;
            exit;
          end;
        end;
    end;
  var
    s2: string;
    len: Integer;
    i: Integer;
    j: Integer;
  begin
    Result := False;
    len := Length(net);
    i := len;
    while i > 1 do
    begin
      if CharInSet(net[i], ['0'..'9']) then
      begin
        if i <> len then
          s2 := Copy(net, i + 1, len - i)
        else
          s2 := '';

        for j := i downto 1 do
          if not CharInSet(net[j], ['0'..'9']) then
          begin
            grp := Copy(net, 1, j) + '#' + s2;

            if IsBusGroups(grp) then
            begin
              Result := True;
              exit;
            end;
            break;
          end;

        i := j;
      end;

      Dec(i);
    end;
  end;

  function FindGroupInd(pin: TPinData): Integer;
  var
    g: TGroup;
    i: Integer;
  begin
    Result := -1;
    i := 0;
    for g in FDoc.Groups do
    begin
      if g.Accept(pin) then
      begin
        Result := i;
        exit;
      end;
      Inc(i);
    end;
  end;

  function MemoEditGroupEnabled(): Boolean;
  var
    line: Integer;
    pd: TPinData;
  begin
    result := False;
    line := SynMemo1.CaretY - 1;
    if (line < SynMemo1.Lines.Count) then
    begin
      pd := (SynMemo1.Lines.Objects[line] as TPinData);
      if Assigned(pd) and Assigned(pd.Group) then
      begin
        result := (pd.Group <> FDoc.NoGroup) and (pd.Group <> FDoc.FilteredGroup);
      end;
    end;
  end;

var
  line: Integer;
  pd: TPinData;
begin
  actMemoEditGroup.Enabled := MemoEditGroupEnabled();

  miAddGrp1.Visible := False;
  miAddGrp2.Visible := False;
  miAddGrp3.Visible := False;

  FPosAggGrp := -1;
  line := SynMemo1.CaretY - 1;
  if (line < SynMemo1.Lines.Count) then
  begin
    pd := (SynMemo1.Lines.Objects[line] as TPinData);
    if Assigned(pd) and Assigned(pd.Pin) then
      FPosAggGrp := FindGroupInd(pd);
  end;

  if SynMemo1.SelStart <> SynMemo1.SelEnd then
  begin
    FStrAggGrp1 := SynMemo1.SelText;
    if pos(' ', FStrAggGrp1) <= 0 then
    begin
      miAddGrp1.Caption := '������������ "' + FStrAggGrp1 + '"';
      miAddGrp1.Visible := True;
    end;
  end else begin
    if line < SynMemo1.Lines.Count  then
    begin
      pd := (SynMemo1.Lines.Objects[line] as TPinData);
      if Assigned(pd) then
      begin
        if Assigned(pd.Pin) then
        begin
          FStrAggGrp1 := pd.NetName;
          miAddGrp1.Caption := '������������ "' + FStrAggGrp1 + '"';
          miAddGrp1.Visible := True;

          if GetGrp2(pd.NetName, FStrAggGrp2) then
          begin
            miAddGrp2.Caption := '������������ "' + FStrAggGrp2 + '"';
            miAddGrp2.Visible := True;
          end;

          if GetGrp3(pd.NetName, FStrAggGrp3) then
          begin
            miAddGrp3.Caption := '������������ "' + FStrAggGrp3 + '"';
            miAddGrp3.Visible := True;
          end;
        end;
      end;
    end;
  end;
end;

procedure TMainForm.pnlFilteredClick(Sender: TObject);
begin
  FocusGroup(FDoc.FilteredGroup);
end;

procedure TMainForm.pnlNoGroupedClick(Sender: TObject);
begin
  FocusGroup(FDoc.NoGroup);
end;

procedure TMainForm.RemoveGroup(i: Integer);
begin
  FDoc.Groups.Delete(i);
end;

procedure TMainForm.UpdateFormatMenu;
var
  h: TSynCustomHighlighter;
  i: Integer;
  chkd: Boolean;
begin
  h := SynMemo1.Highlighter;
  if Assigned(h) then
  begin
    SynMemo1.Highlighter := nil;
    h.Free();
  end;

  if FDoc.Format >= 0 then
  begin
    smFormat.Caption := FormatReestr[FDoc.Format].GetName;

    SynMemo1.Highlighter := FormatReestr[FDoc.Format].GetHighlighter;
  end
  else
    smFormat.Caption := '������';

  for i := 0 to smFormat.Count - 1 do
  begin
    chkd := i = FDoc.Format;
    smFormat.Items[i].Checked := chkd;
    if chkd then
      smFormat.ImageIndex := smFormat.Items[i].ImageIndex;
  end;
end;

procedure TMainForm.UpdateGroup(i: Integer);
begin
  FDoc.UpdateGroupsPins;
  lbGroups.Invalidate;
end;

end.
